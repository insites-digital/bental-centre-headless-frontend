export const addToSearch = data => dispatch => {
  dispatch({
    type: "ADD_TO_SEARCH",
    payload: data
  });
};
