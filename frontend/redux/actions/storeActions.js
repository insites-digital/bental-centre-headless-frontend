import Config from "../../config";
import axios from "axios";

export const fetchStores = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/stores`).then(response => {
    dispatch({
      type: "FETCH_STORES",
      payload: response.data
    });
  });
};
