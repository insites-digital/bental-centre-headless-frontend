import Config from "../../config";
import axios from "axios";

export const fetchCampaigns = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/campaigns`).then(response => {
    dispatch({
      type: "FETCH_CAMPAIGNS",
      payload: response.data
    });
  });
};
