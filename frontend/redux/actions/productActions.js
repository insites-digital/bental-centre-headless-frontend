import Config from "../../config";
import axios from "axios";

export const fetchProducts = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/products`).then(response => {
    dispatch({
      type: "FETCH_PRODUCTS",
      payload: response.data
    });
  });
};
