import Config from "../../config";
import axios from "axios";

export const fetchFloors = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/floor`).then(response => {
    dispatch({
      type: "FETCH_FLOORS",
      payload: response.data
    });
  });
};
