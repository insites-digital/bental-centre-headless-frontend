import Config from "../../config";
import axios from "axios";

export const fetchAmenities = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/amenities`).then(response => {
    dispatch({
      type: "FETCH_AMENITIES",
      payload: response.data
    });
  });
};
