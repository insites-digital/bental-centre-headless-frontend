import Config from "../../config";
import axios from "axios";

export const fetchEvents = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/events`).then(response => {
    dispatch({
      type: "FETCH_EVENTS",
      payload: response.data
    });
  });
};

export const fetchEventCategories = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/event-category`).then(response => {
    dispatch({
      type: "FETCH_EVENT_CATEGORIES",
      payload: response.data
    });
  });
};
