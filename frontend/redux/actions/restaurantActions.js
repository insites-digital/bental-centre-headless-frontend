import Config from "../../config";
import axios from "axios";

export const fetchRestaurants = () => async dispatch => {
  await axios.get(`${Config.apiUrl}/wp/v2/restaurants`).then(response => {
    dispatch({
      type: "FETCH_RESTAURANTS",
      payload: response.data
    });
  });
};

export const fetchRestaurantCategories = () => async dispatch => {
  await axios
    .get(`${Config.apiUrl}/wp/v2/restaurant-category`)
    .then(response => {
      dispatch({
        type: "FETCH_RESTAURANT_CATEGORIES",
        payload: response.data
      });
    });
};
