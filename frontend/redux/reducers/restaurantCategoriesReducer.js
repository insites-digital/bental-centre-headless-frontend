export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_RESTAURANT_CATEGORIES":
      return [...state, action.payload];
    default:
      return state;
  }
};
