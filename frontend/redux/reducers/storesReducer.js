export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_STORES":
      return [...state, action.payload];
    default:
      return state;
  }
};
