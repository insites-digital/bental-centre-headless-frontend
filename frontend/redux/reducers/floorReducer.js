export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_FLOORS":
      return [...state, action.payload];
    default:
      return state;
  }
};
