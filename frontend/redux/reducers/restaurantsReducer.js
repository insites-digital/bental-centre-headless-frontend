export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_RESTAURANTS":
      return [...state, action.payload];
    default:
      return state;
  }
};
