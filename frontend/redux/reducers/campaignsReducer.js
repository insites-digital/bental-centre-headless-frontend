export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_CAMPAIGNS":
      return [...state, action.payload];
    default:
      return state;
  }
};
