export default (state = "", action) => {
  switch (action.type) {
    case "ADD_TO_SEARCH": {
      return action.payload;
    }

    default:
      return {};
  }
};
