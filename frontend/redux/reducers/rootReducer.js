import { combineReducers } from "redux";
import productReducer from "./productsReducer";
import amenitiesReducer from "./amenitiesReducer";
import eventsReducer from "./eventsReducer";
import storesReducer from "./storesReducer";
import campaignsReducer from "./campaignsReducer";
import eventCategoriesReducer from "./eventCategoriesReducer";
import restaurantsReducer from "./restaurantsReducer";
import restaurantCategoriesReducer from "./restaurantCategoriesReducer";
import floorReducer from "./floorReducer";
import searchReducer from "./searchReducer";

export default combineReducers({
  products: productReducer,
  amenities: amenitiesReducer,
  events: eventsReducer,
  eventCats: eventCategoriesReducer,
  stores: storesReducer,
  campaigns: campaignsReducer,
  restaurants: restaurantsReducer,
  restaurantCats: restaurantCategoriesReducer,
  floors: floorReducer,
  search: searchReducer
});
