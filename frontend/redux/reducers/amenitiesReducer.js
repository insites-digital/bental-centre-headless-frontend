export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_AMENITIES":
      return [...state, action.payload];
    default:
      return state;
  }
};
