export default (state = [], action) => {
  switch (action.type) {
    case "FETCH_EVENT_CATEGORIES":
      return [...state, action.payload];
    default:
      return state;
  }
};
