var DomParser = require("dom-parser");
let parser = new DomParser();

export const decodeString = (encodedStr) => {
  let dom = parser.parseFromString(
    "<!doctype html><body>" + encodedStr,
    "text/html"
  );
  return dom.getElementsByTagName("body")[0].textContent;
};
