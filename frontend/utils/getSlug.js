import Config from "../config";

export const getSlug = (url) => {
  if (!url) return url;
  if (url.indexOf(Config.hostName) < 0) {
    return url;
  }

  let formatUrl;
  if (url.includes(".co.uk")) {
    formatUrl = url.split(".co.uk")[1];
    return formatUrl === "/home/" ? "/" : formatUrl;
  }
  formatUrl = url.split(".com")[1];
  return formatUrl === "/home/" ? "/" : formatUrl;
};
