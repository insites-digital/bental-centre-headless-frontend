import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import HeroText from "../components/HeroText";
import TabbedStores from "../components/tabbedContent/TabbedStores";
import Error from "../components/Error";

const Stores = (props) => {
  if (!props || !props.stores) return <></>;
  //Error handling
  if (props.stores.length === 0)
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: [],
        }}
        pageClass={"page"}
        amenities={props.amenities}
        amenityCats={props.amenityCats}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        campaigns={props.campaigns}
      >
        <Error />
      </Layout>
    );

  const {
    page,
    floors,
    storeCats,
    stores,
    products,
    campaigns,
    restaurants,
    events,
    amenities,
    amenityCats,
    globalContent,
  } = props;

  let reorderedStoreCats = [];
  let sortingArr = props.page.acf.tab_order.categories;

  sortingArr.forEach((key) => {
    var found = false;
    [...storeCats].filter((item) => {
      if (!found && item.id == key) {
        reorderedStoreCats.push(item);
        found = true;
        return false;
      } else return true;
    });
  });

  const heroContent = page.acf.components[0].content;

  return (
    <Layout
      data={globalContent}
      metaData={{
        title:
          page.title.rendered.replace("&#8217;", "'").replace("&#038;", "&") +
          " | Bentall Centre | Kingston",
        meta: page.yoast_head,
      }}
      pageClass="search"
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      {heroContent && heroContent !== "" && (
        <HeroText
          data={{
            description: heroContent,
          }}
        />
      )}

      <TabbedStores
        stores={stores}
        storeCats={reorderedStoreCats}
        floors={floors}
      />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  const globalContent = await getOptions();
  const page = await getPageBySlug("stores");
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");
  const storeCats = await getContent("store-category");
  const floors = await getContent("floor");

  return {
    revalidate: 1,
    props: {
      page,
      floors,
      storeCats,
      stores: stores.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      amenities: amenities.filter((item) => item.status === "publish"),
      amenityCats,
      globalContent,
    },
  };
}

export default Stores;
