import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import ContactPage from "../components/templates/contactPage";
import Error from "../components/Error";

const ContactUs = (props) => {
  if (!props || !props.page) return <></>;

  const {
    page,
    products,
    stores,
    restaurants,
    campaigns,
    events,
    globalContent,
    amenities,
    amenityCats,
  } = props;

  if (!page) {
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: null,
        }}
        pageClass={"page"}
        amenities={amenities}
        amenityCats={amenityCats}
        products={products}
        restaurants={restaurants}
        stores={stores}
        events={events}
        campaigns={campaigns}
      >
        <Error />
      </Layout>
    );
  }

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Contact us | Bentall Centre | Kingston",
        meta: page.yoast_head,
      }}
      pageClass={"contact-us"}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <ContactPage data={page} stores={stores} />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  const globalContent = await getOptions();
  const page = await getPageBySlug("contact-us");
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    revalidate: 1,
    props: {
      page,
      globalContent,
      amenityCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export default ContactUs;
