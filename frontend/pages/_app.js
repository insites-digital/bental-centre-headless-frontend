import App, { Container } from "next/app";
import React from "react";
import Router from "next/router";
import { GTMPageView } from "../utils/gt";

class MyApp extends App {
  componentDidMount() {
    const handleRouteChange = (url) => GTMPageView(url);
    Router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      Router.events.off("routeChangeComplete", handleRouteChange);
    };
  }

  render() {
    const { Component, pageProps } = this.props;
    return <Component {...pageProps} />;
  }
}

export default MyApp;
