import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import HeroText from "../components/HeroText";
import TabbedCentres from "../components/tabbedContent/TabbedCentres";
import Error from "../components/Error";

const TheCentre = (props) => {
  if (!props) return <></>;
  //Error handling
  if (!props.globalContent)
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: null,
        }}
        pageClass={"page"}
        amenities={props.amenities}
        amenityCats={props.amenityCats}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        campaigns={props.campaigns}
      >
        <Error />
      </Layout>
    );

  const {
    page,
    products,
    campaigns,
    events,
    stores,
    restaurants,
    globalContent,
    amenities,
    amenityCats,
    query,
  } = props;

  return (
    <Layout
      data={globalContent}
      metaData={{
        title:
          page.title.rendered.replace("&#8217;", "'").replace("&#038;", "&") +
          " | Bentall Centre | Kingston",
        meta: page.yoast_head,
      }}
      pageClass={page.title.rendered}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <HeroText
        data={{
          breadcrumbs: [
            { name: "The Centre", link: "/the-centre/" },
            { name: "Centre Info", link: "" },
          ],
        }}
      />

      <TabbedCentres data={globalContent.centre_details} query={query} />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps(context) {
  const globalContent = await getOptions();
  const page = await getPageBySlug("the-centre");
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  let query = null;

  if (context.query && context.query.q) {
    query = context.query;
  }

  return {
    revalidate: 1,
    props: {
      page,
      products,
      campaigns: campaigns.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
      globalContent,
      amenities: amenities.filter((item) => item.status === "publish"),
      amenityCats,
      query,
    },
  };
}

export default TheCentre;
