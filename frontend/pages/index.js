import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import Hero from "../components/Hero";
import Marquee from "../components/Marquee";
import CampaignFeature from "../components/CampaignFeature";
import WhatsOn from "../components/WhatsOn";
import TeaserFeature from "../components/TeaserFeature";
import FeaturedProductsSlider from "../components/FeaturedProductsSlider";
import RestaurantSlider from "../components/RestaurantSlider";

const Index = (props) => {
  if (!props.pages || !props.page) return <></>;

  const { restaurants } = props;

  // Find any sticky restaurants
  let stickyRestaurants = restaurants.filter((restaurant) => {
    return (
      restaurant.hasOwnProperty("acf") &&
      restaurant.acf.hasOwnProperty("stick_post") &&
      restaurant.acf.stick_post === true
    );
  });

  const compare = (a, b) => {
    const bandA = a.acf.sticky_order;
    const bandB = b.acf.sticky_order;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  };

  stickyRestaurants.sort(compare);

  const filteredRestaurants = restaurants.filter(function (restaurant) {
    return !stickyRestaurants.find(function (sticky) {
      return restaurant.slug === sticky.slug;
    });
  });

  const orderedRestaraunts = [...stickyRestaurants, ...filteredRestaurants];

  const HomeComponents = () => {
    const components = props.page.acf.components.map((item, index) => {
      switch (item.acf_fc_layout) {
        case "hero":
          return (
            <div key={`hero=${index}`}>
              <Hero data={item.slides} />
              <Marquee data={item.marquee} />
            </div>
          );
        case "eventSlider":
          return (
            <WhatsOn
              key={`whatson=${index}`}
              data={item}
              events={props.events}
              eventCats={props.eventCats}
            />
          );
        case "dotd":
          return (
            <TeaserFeature
              key={`teaser=${index}`}
              data={props.globalContent.daily_deals}
              products={props.products}
            />
          );
        // case "cinemaSlider":
        //   return <CinemaSlider key={`cinema=${index}`} />;
        case "fullwidthTeaser":
          return <CampaignFeature key={`campaign=${index}`} data={item} />;
        case "restaurantSlider":
          return (
            <RestaurantSlider
              key={`restaurant=${index}`}
              data={
                orderedRestaraunts.length > 0 ? orderedRestaraunts : restaurants
              }
              componentData={item}
              restaurantCats={props.restaurantCats}
              floors={props.floors}
            />
          );
        case "productSlider":
          return (
            <FeaturedProductsSlider
              key={`products=${index}`}
              data={item}
              allProducts={props.products}
              stores={props.stores}
            />
          );
        default:
          break;
      }
    });
    return components;
  };

  return (
    <Layout
      pageClass="home"
      metaData={{
        title: "Home | Bentall Centre | Kingston",
        meta: props.page.yoast_head,
      }}
      data={props.globalContent}
      showServices={true}
      amenities={props.amenities}
      amenityCats={props.amenityCats}
      products={props.products}
      restaurants={props.restaurants}
      stores={props.stores}
      events={props.events}
      campaigns={props.campaigns}
    >
      <HomeComponents />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  const globalContentData = getOptions();
  const pageData = getPageBySlug("home");
  const pagesData = getContent("pages", "id,title,link,content");
  const productsData = getContent("products", "id,title,link,content");
  const campaignsData = getContent("campaigns", "id,title,link,content");
  const eventsData = getContent(
    "events",
    "id,title,link,content,event-category,acf"
  );
  const restaurantsData = getContent(
    "restaurants",
    "id,title,content,slug,link,acf"
  );
  const storesData = getContent("stores", "id,title,link,content");
  const floorsData = getContent("floor");
  const eventCatsData = getContent("event-category", "id,name,acf");
  const restaurantCatsData = getContent("restaurant-category", "id,name");
  const amenityCatsData = getContent("amenity-category", "id,name");
  const amenitiesData = getContent(
    "amenities",
    "title,content,acf,amenity-category,link"
  );

  const [
    globalContent,
    page,
    pages,
    products,
    campaigns,
    events,
    eventCats,
    restaurants,
    restaurantCats,
    stores,
    floors,
    amenities,
    amenityCats,
  ] = await Promise.allSettled([
    globalContentData,
    pageData,
    pagesData,
    productsData,
    campaignsData,
    eventsData,
    eventCatsData,
    restaurantsData,
    restaurantCatsData,
    storesData,
    floorsData,
    amenitiesData,
    amenityCatsData,
  ]);

  return {
    revalidate: 60,
    props: {
      page: page.value,
      pages: pages.value,
      globalContent: globalContent.value,
      amenityCats: amenityCats.value,
      eventCats: eventCats.value,
      restaurantCats: restaurantCats.value,
      floors: floors.value,
      amenities: amenities.value,
      stores: stores.value,
      events: events.value,
      products: products.value,
      campaigns: campaigns.value,
      restaurants: restaurants.value,
    },
  };
}

export default Index;
