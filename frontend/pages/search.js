import React from "react";
import { getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import SearchResults from "../components/templates/searchResults";

const Search = (props) => {
  const {
    amenities,
    amenityCats,
    restaurants,
    products,
    events,
    campaigns,
    stores,
    globalContent,
  } = props;

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Search results | Bentall Centre | Kingston",
        meta: null,
      }}
      pageClass="search"
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
    >
      <SearchResults
        events={events}
        restaurants={restaurants}
        amenities={amenities}
        stores={stores}
        products={products}
        campaigns={campaigns}
      />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
//export async function getStaticProps() {
export async function getServerSideProps() {
  const globalContent = await getOptions();
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    props: {
      amenityCats,
      globalContent,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export default Search;
