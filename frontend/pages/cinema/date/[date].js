import React from "react";
import { getContent, getOptions } from "../../../services/content";
import Layout from "../../../components/Layout";
import FilmGrid from "../../../components/films/FilmGrid";
import { getCinemaShowTimes } from "../../../services/helpers/getCinemaData";

const Films = (props) => {
  if (!props) return <></>;

  const { globalContent, amenities, amenityCats } = props;

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Curzon Cinema | Bentall Centre | Kingston",
      }}
      pageClass="cinema"
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
    >
      <div
        className="container services-page"
        style={{ marginBottom: "100px", marginTop: "40px" }}
      >
        <FilmGrid films={props?.cinemaShowTimes?.films || []} />
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ req, res, params }) {
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=10, stale-while-revalidate=59"
  );

  const cinema_id = 51305;
  const nowDateString = params.date;

  const globalContentData = getOptions();
  const amenityCatsData = getContent("amenity-category", "id,name");
  const amenitiesData = getContent(
    "amenities",
    "title,content,acf,amenity-category,link"
  );
  const cinemaShowTimesData =
    getCinemaShowTimes(cinema_id, nowDateString) || null;

  const [globalContent, amenities, amenityCats, cinemaShowTimes] =
    await Promise.allSettled([
      globalContentData,
      amenitiesData,
      amenityCatsData,
      cinemaShowTimesData,
    ]);

  return {
    props: {
      cinemaShowTimes: cinemaShowTimes.value || null,
      globalContent: globalContent.value,
      amenityCats: amenityCats.value,
      amenities: amenities.value.filter((item) => item.status === "publish"),
    },
  };
}

export default Films;
