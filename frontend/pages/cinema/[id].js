import React, { useState, useEffect } from "react";
import Link from "next/link";

import moment from "moment";
import ReactPlayer from "react-player/file";

import Layout from "../../components/Layout";

import { getContent, getOptions } from "../../services/content";
import {
  getFilmById,
  getPurchaseConfirmation,
  getCinemaShowTimesForAFilm,
} from "../../services/helpers/getCinemaData";

const Film = (props) => {
  const [purchaseUrl, setPurchaseUrl] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedTime, setSelectedTime] = useState(null);
  const [playVideo, setPlayVideo] = useState(false);

  const { film, filmDatesAndTime, amenities, amenityCats, globalContent } =
    props;

  // console.log(props);

  if (!film || !filmDatesAndTime) {
    return <></>;
  }

  const filmTrailer = film?.trailers?.high[0]["film_trailer"];
  const posterUrl = film.images["poster"][1].medium.film_image;

  useEffect(() => {
    if (selectedDate && selectedTime) {
      getPurchaseConfirmation(
        51305,
        film.film_id,
        selectedDate,
        selectedTime
      ).then((data) => {
        setPurchaseUrl(data.url);
      });
    }
  }, [selectedDate, selectedTime]);

  console.log(filmTrailer);

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: film.film_name + " at Curzon Cinema | Bentall Centre | Kingston",
        // meta: cinema.yoast_head,
      }}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
    >
      <section className="single-page film-details">
        <div className="container hero-tet">
          <nav className="breadcrumb">
            <Link href="/cinema">
              <a>Cinema</a>
            </Link>
          </nav>
        </div>
        <div className="container">
          <div className="">
            <div
              className="teaser-content__container"
              style={{ flexWrap: "wrap" }}
            >
              <div className="teaser-content teaser-content--half">
                {filmTrailer ? (
                  <div
                    onClick={() => setPlayVideo(!playVideo)}
                    className="film-details--trailer"
                  >
                    <ReactPlayer
                      light={posterUrl}
                      url={filmTrailer}
                      playing={playVideo}
                      controls={false}
                      height="100%"
                      width="100%"
                    />
                  </div>
                ) : (
                  <img
                    src={film.images.poster["1"].medium.film_image}
                    style={{
                      maxWidth: "100%",
                      objectFit: "cover",
                      display: "block",
                      margin: "0 auto",
                    }}
                  />
                )}
              </div>
              <div className="teaser-content teaser-content--half teaser-content--details">
                <span className="meta-details">
                  {film.duration_mins && (
                    <p>Duration: {film.duration_mins} Mins.</p>
                  )}

                  {film.age_rating && (
                    <p>
                      Certificate:
                      {film.age_rating.map((age_rating) => (
                        <React.Fragment key={age_rating.rating}>
                          {age_rating.rating}
                        </React.Fragment>
                      ))}
                    </p>
                  )}
                </span>

                <div className="teaser-content__inside">
                  <h5
                    className="teaser__subject"
                    style={{ textDecoration: "underline" }}
                  >
                    {film.film_name}
                  </h5>
                  <div
                    className="teaser__description"
                    style={{ fontWeight: "bold" }}
                  >
                    {film.synopsis_long}
                  </div>

                  <div className="">
                    {filmDatesAndTime.map(({ date, times }, index) => {
                      const showingsStandard =
                        times?.films[0].showings["Standard"].times;
                      // const showings3d = times.films[0].showings["3D"].times;

                      return (
                        showingsStandard?.length && (
                          <div key={date} className="showing">
                            <span className="date">
                              {moment(new Date(date)).format("dddd DD YYYY")}
                            </span>

                            {showingsStandard?.length && (
                              <div>
                                <div className="times">
                                  {showingsStandard.map(
                                    ({ start_time, end_time }, index) => {
                                      return (
                                        <button
                                          onClick={() => {
                                            setSelectedDate(date);
                                            setSelectedTime(start_time);
                                          }}
                                          className={`time ${
                                            selectedDate === date &&
                                            selectedTime === start_time &&
                                            "active"
                                          }`}
                                          key={index}
                                        >
                                          {moment(start_time, "hh:mm").format(
                                            "LT"
                                          )}
                                          -{" "}
                                          {moment(end_time, "hh:mm").format(
                                            "LT"
                                          )}
                                        </button>
                                      );
                                    }
                                  )}
                                </div>
                              </div>
                            )}

                            {/* {showings3d.length && (
                              <div className="">
                                <div>3D</div>
                                <div className="times">
                                  {showings3d.map(
                                    ({ start_time, end_time }, index) => {
                                      return (
                                        <div className="time" key={index}>
                                          {moment(start_time, "hh:mm").format(
                                            "LT"
                                          )}
                                          -{" "}
                                          {moment(end_time, "hh:mm").format(
                                            "LT"
                                          )}
                                        </div>
                                      );
                                    }
                                  )}
                                </div>
                              </div>
                            )} */}
                          </div>
                        )
                      );
                    })}
                  </div>

                  {purchaseUrl && (
                    <a href={purchaseUrl} className="cta" target="_blank">
                      Book tickets on Curzon site
                    </a>
                  )}

                  <div className="divider"></div>

                  <a
                    href="https://www.curzon.com/venues/kingston"
                    className="more-dates-link"
                  >
                    More dates
                    <span className="arrow-icon">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 29.789 8"
                      >
                        <g data-name="Group 461">
                          <path
                            data-name="Line 12"
                            fill="none"
                            stroke="#000"
                            d="M0 4.229h19.733"
                          />
                          <path data-name="Polygon 1" d="m29.789 4-11 4V0Z" />
                        </g>
                      </svg>
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export async function getServerSideProps({ req, res, params }) {
  // const film = (await getFilmById(params.id)) || null;

  res.setHeader(
    "Cache-Control",
    "public, s-maxage=10, stale-while-revalidate=59"
  );

  const cinema_id = 51305;

  const getFilmShowDates = async (film) => {
    const time = film?.show_dates.slice(0, 7).map(async ({ date }) => {
      const showTimes = await getCinemaShowTimesForAFilm(
        cinema_id,
        film.film_id,
        date
      );
      return {
        date,
        times: showTimes || null,
      };
    });
    return Promise.all(time);
  };

  const globalContentData = getOptions();
  const amenityCatsData = getContent("amenity-category", "id,name");
  const amenitiesData = getContent(
    "amenities",
    "title,content,acf,amenity-category,link"
  );
  const filmData = getFilmById(params.id);

  const [globalContent, amenities, amenityCats, film] =
    await Promise.allSettled([
      globalContentData,
      amenitiesData,
      amenityCatsData,
      filmData,
    ]);

  const filmDatesAndTime = film?.value
    ? await getFilmShowDates(film.value)
    : null;

  return {
    props: {
      film: film.value || null,
      filmDatesAndTime: filmDatesAndTime,
      globalContent: globalContent.value,
      amenityCats: amenityCats.value,
      amenities: amenities.value.filter((item) => item.status === "publish"),
    },
  };
}

export default Film;
