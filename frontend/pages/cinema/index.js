import React from "react";

import Layout from "../../components/Layout";
import FilmGrid from "../../components/films/FilmGrid";
import FilmHero from "../../components/films/FilmHero";

import { getContent, getOptions } from "../../services/content";
import { getCinemaShowTimes } from "../../services/helpers/getCinemaData";

const heroBanner = "/images/hero-curzon-cinema.jpg";

const HERO_DATA = {
  title: "Curzon",
  image: heroBanner,
  video: "https://vimeo.com/774318579/4312060468",
  links: [
    {
      label: "Cinema Information",
      url: "https://www.curzon.com/venues/kingston/",
      isExternal: true,
    },
    {
      label: "Become a Member",
      url: "https://www.curzon.com/membership/membership-sales-page/",
      isExternal: true,
    },
  ],
};

const Films = (props) => {
  if (!props) return <></>;

  const { globalContent, amenities, amenityCats } = props;

  // console.log(props);

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Curzon Cinema | Bentall Centre | Kingston",
      }}
      pageClass="cinema"
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
    >
      <div className="container services-page" style={{ marginBottom: "40px" }}>
        <FilmHero
          links={HERO_DATA.links}
          title={HERO_DATA.title}
          image={HERO_DATA.image}
          video={HERO_DATA.video}
        />
        <FilmGrid films={props?.cinemaShowTimes?.films || []} />
      </div>
    </Layout>
  );
};

export default Films;

export async function getServerSideProps({ req, res, params }) {
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=10, stale-while-revalidate=59"
  );

  const cinema_id = 51305;

  // Today's date
  let nowDate = new Date();

  const offset = nowDate.getTimezoneOffset();
  nowDate = new Date(nowDate.getTime() - offset * 60 * 1000);

  const nowDateString = nowDate.toISOString().split("T")[0];

  const cinemaShowTimesData = getCinemaShowTimes(cinema_id, nowDateString);

  const globalContentData = getOptions();
  const amenityCatsData = getContent("amenity-category", "id,name");
  const amenitiesData = getContent(
    "amenities",
    "title,content,acf,amenity-category,link"
  );

  const [globalContent, amenities, amenityCats, cinemaShowTimes] =
    await Promise.allSettled([
      globalContentData,
      amenitiesData,
      amenityCatsData,
      cinemaShowTimesData,
    ]);

  return {
    props: {
      cinemaShowTimes: cinemaShowTimes.value || null,
      globalContent: globalContent.value,
      amenityCats: amenityCats.value,
      amenities: amenities.value,
    },
  };
}
