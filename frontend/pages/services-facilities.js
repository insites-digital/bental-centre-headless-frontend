import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import Services from "../components/templates/services";
import Error from "../components/Error";

const ServicesFacilitiesPage = (props) => {
  if (!props || !props.amenities) return <></>;

  const {
    page,
    amenities,
    amenityCats,
    stores,
    events,
    products,
    campaigns,
    restaurants,
    globalContent,
  } = props;

  if (amenities.length === 0) {
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: [],
        }}
        pageClass={"page"}
        amenities={props.amenities}
        amenityCats={props.amenityCats}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        campaigns={props.campaigns}
      >
        <Error />
      </Layout>
    );
  }

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Services & Facilities | Bentall Centre | Kingston",
        meta: page.yoast_head,
      }}
      pageClass={page.slug}
      showServices={false}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <Services data={amenities} category={amenityCats} />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  const globalContent = await getOptions();
  const page = await getPageBySlug("services-facilities");
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    revalidate: 1,
    props: {
      page,
      amenityCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
      globalContent,
    },
  };
}

export default ServicesFacilitiesPage;
