import React from "react";
import {
  getProductBySlug,
  getContent,
  getOptions,
} from "../../services/content";
import Layout from "../../components/Layout";
import SinglePage from "../../components/templates/singlePage";
import Error from "../../components/Error";

const ProductPage = (props) => {
  if (!props || !props.product) return <></>;
  // Send to error handling

  const {
    products,
    campaigns,
    events,
    stores,
    restaurants,
    globalContent,
    amenities,
    amenityCats,
  } = props;

  if (props.product.length === 0)
    return (
      <Layout
        data={globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: null,
        }}
        pageClass={"page"}
        amenities={amenities}
        amenityCats={amenityCats}
        products={products}
        restaurants={restaurants}
        stores={stores}
        events={events}
        campaigns={campaigns}
      >
        <Error />
      </Layout>
    );

  const product = props.product;
  const post = product;
  post.show_opening = false;
  post.breadcrumbs = [
    { name: "Home", link: "/" },
    {
      name: product.title.rendered
        .replace("&#8217;", "'")
        .replace("&#038;", "&"),
      link: product.slug,
    },
  ];

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: product.title.rendered + " | Bentall Centre | Kingston",
        meta: product.yoast_head,
      }}
      pageClass={product.slug}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <SinglePage
        data={post}
        stores={props.stores}
        restaurants={props.restaurants}
        slug={post.slug}
        showOpeningTimes={false}
      />
    </Layout>
  );
};

// TEMP FOR STAGING
// REMOVE GET SERVER SIDE PROPS FOR GO LIVE AND BRING BACK GET STATIC PROPS
// export async function getServerSideProps(context) {
//   const res = await fetch(
//     `${Config.apiUrl}/wp/v2/products?slug=${context.params.slug}`
//   );
//   const product = await res.json();

//   const resStores = await fetch(`${Config.apiUrl}/wp/v2/stores?per_page=100`);
//   const stores = await resStores.json();

//   const resAmenities = await fetch(
//     `${Config.apiUrl}/wp/v2/amenities?per_page=100`
//   );
//   const amenities = await resAmenities.json();

//   const resAmenityCats = await fetch(
//     `${Config.apiUrl}/wp/v2/amenity-category?per_page=100`
//   );
//   const amenityCats = await resAmenityCats.json();

//   const resGlobal = await fetch(
//     `${Config.apiUrl}/wp/v2/acf/options?per_page=100`
//   );
//   const globalContent = await resGlobal.json();

//   const resProducts = await fetch(
//     `${Config.apiUrl}/wp/v2/products?per_page=100`
//   );
//   const products = await resProducts.json();

//   const resRestaurants = await fetch(
//     `${Config.apiUrl}/wp/v2/restaurants?per_page=100`
//   );
//   const restaurants = await resRestaurants.json();

//   const resEvents = await fetch(`${Config.apiUrl}/wp/v2/events?per_page=100`);
//   const events = await resEvents.json();

//   const resCampaigns = await fetch(
//     `${Config.apiUrl}/wp/v2/campaigns?per_page=100`
//   );
//   const campaigns = await resCampaigns.json();

//   return {
//     props: {
//       product,
//       products,
//       campaigns,
//       events,
//       stores,
//       restaurants,
//       globalContent,
//       amenities,
//       amenityCats,
//     },
//   };
// }

export async function getStaticProps(context) {
  const globalContent = await getOptions();
  const product = await getProductBySlug(context.params.slug);
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    revalidate: 1,
    props: {
      product,
      globalContent,
      amenityCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export async function getStaticPaths() {
  const products = await getContent("products");
  return {
    paths: products.map((product) => {
      return { params: { slug: product["slug"] } };
    }),
    fallback: true,
  };
}

export default ProductPage;
