import React from "react";
import {
  getCampaignBySlug,
  getContent,
  getOptions,
  getContentFromPages,
} from "../../services/content";
import Layout from "../../components/Layout";
import Campaign from "../../components/templates/campaign";

const CampaignPage = (props) => {
  const {
    campaign,
    campaigns,
    globalContent,
    stores,
    restaurants,
    restaurantCats,
    amenityCats,
    amenities,
    products,
    events,
    eventCats,
  } = props;

  if (!props || !campaign) return <></>;

  let template = "";

  if (campaign.acf.hasOwnProperty("template") && campaign.acf.template !== "") {
    template = campaign.acf.template;
  }

  return (
    <Layout
      data={globalContent}
      metaData={{
        title:
          campaign.title.rendered
            .replace("&#8217;", "'")
            .replace("&#038;", "&") + " | Bentall Centre | Kingston",
        meta: campaign.yoast_head,
      }}
      pageClass={"campaign " + template}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <Campaign
        data={campaign}
        products={products}
        globalContent={globalContent}
        stores={stores}
        restaurants={restaurants}
        restaurantCats={restaurantCats}
        events={events}
        eventCats={eventCats}
      />
    </Layout>
  );
};

export async function getStaticProps({ params }) {
  const globalContent = await getOptions();
  const campaign = await getCampaignBySlug(params.slug);

  if (!campaign) {
    return {
      notFound: true,
    };
  }

  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const eventCats = await getContent("event-category");
  const restaurants = await getContent("restaurants");
  const restaurantCats = await getContent("restaurant-category");
  const stores = await getContent("stores");

  return {
    props: {
      campaign,
      globalContent,
      amenityCats,
      restaurantCats,
      eventCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
    revalidate: 1,
  };
}

export async function getStaticPaths() {
  const campaigns = await getContent("campaigns");

  const paths = campaigns.map((campaign) => {
    return { params: { slug: campaign.slug } };
  });

  return {
    paths,
    fallback: "blocking",
  };
}

export default CampaignPage;
