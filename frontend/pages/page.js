import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React, { Component } from "react";
import Error from "../components/Error";
import Layout from "../components/Layout";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import PlainText from "../components/templates/plainText";
import Standard from "../components/templates/standard";

class Post extends Component {
  static async getInitialProps(context) {
    const { slug } = context.query;
    let globalContent,
      page,
      amenities,
      amenityCats,
      restaurants,
      stores,
      campaigns,
      products,
      events,
      eventCats;

    globalContent = await getOptions();
    page = await getPageBySlug(slug);
    amenityCats = await getContent("amenity-category");
    amenities = await getContent("amenities");
    products = await getContent("products");
    campaigns = await getContent("campaigns");
    events = await getContent("events");
    eventCats = await getContent("event-category");
    restaurants = await getContent("restaurants");
    stores = await getContent("stores");

    return {
      globalContent,
      page,
      amenityCats,
      eventCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    };
  }

  render() {
    const {
      globalContent,
      page,
      amenities,
      amenityCats,
      restaurants,
      stores,
      campaigns,
      products,
      events,
      eventCats,
    } = this.props;

    // if (!page) return <></>;

    if (!page || page.length === 0) {
      return (
        <Layout
          data={globalContent}
          metaData={{
            title: "Page not found | Bentall Centre | Kingston",
            meta: [],
          }}
          pageClass={page ? page.slug : "page"}
          amenities={amenities}
          amenityCats={amenityCats}
          products={products}
          restaurants={restaurants}
          stores={stores}
          events={events}
          campaigns={campaigns}
        >
          <Error />
        </Layout>
      );
    }

    return (
      <Layout
        data={globalContent}
        metaData={{
          title: page
            ? page.title.rendered
                .replace("&#8217;", "'")
                .replace("&#038;", "&") + " | Bentall Centre | Kingston"
            : "Bentall Centre | Kingston",
          meta: page.yoast_head,
        }}
        pageClass={page ? page.slug : "page"}
        showServices={true}
        amenities={amenities}
        amenityCats={amenityCats}
        products={products}
        restaurants={restaurants}
        stores={stores}
        events={events}
        campaigns={campaigns}
      >
        {page &&
          page.hasOwnProperty("acf") &&
          page.acf &&
          page.acf.hasOwnProperty("template") &&
          page.acf.template === "plain" && <PlainText data={page} />}

        {page &&
          page.hasOwnProperty("acf") &&
          page.acf &&
          page.acf.hasOwnProperty("template") &&
          page.acf.template === "standard" && (
            <Standard data={page} events={events} eventCats={eventCats} />
          )}
      </Layout>
    );
  }
}
export default Post;
