import { getOptions } from "../services/content";
import Layout from "../components/Layout";
import Error from "../components/Error";

const Custom404 = ({ global }) => {
  return (
    <Layout
      data={global}
      metaData={{
        title: "Page not found | Bentall Centre | Kingston",
        meta: null,
      }}
    >
      <Error />
    </Layout>
  );
};

export default Custom404;

export async function getStaticProps(context) {
  const global = await getOptions();

  return {
    props: {
      global,
    },
    revalidate: 1,
  };
}
