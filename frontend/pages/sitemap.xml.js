import React from "react";
import axios from "axios";
import Config from "../config";

const createSitemap = ({
  pages,
  eventPages,
  storePages,
  restaurantPages,
  productPages,
  campaignPages,
  amenityPages,
}) => {
  const cmsURL = "https://" + Config.hostName + "/";
  return `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${pages
          .map((page) => {
            return `
                    <url>
                        <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc>
                    </url>`;
          })
          .join("")}

        ${eventPages
          .map((page) => {
            return `
                    <url>
                        <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc>
                    </url>`;
          })
          .join("")}

        ${storePages
          .map((page) => {
            return `
                    <url>
                        <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc></url>`;
          })
          .join("")}

        ${restaurantPages
          .map((page) => {
            return `
                    <url>
                        <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc></url>`;
          })
          .join("")}

        ${productPages
          .map((page) => {
            return `
                <url><loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc></url>`;
          })
          .join("")}    

        ${campaignPages
          .map((page) => {
            return `
                    <url>
                        <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc></url>`;
          })
          .join("")}
                
        ${amenityPages
          .map((page) => {
            return `
                        <url>
                            <loc>${Config.frontend}/${page.link.replace(
              cmsURL,
              ""
            )}</loc></url>`;
          })
          .join("")}

        <url>
          <loc>${Config.frontend}/the-centre/opening-hours/</loc>
        </url>

        <url>
            <loc>${Config.frontend}/the-centre/about-us/</loc>
        </url>

        <url>
            <loc>${Config.frontend}/the-centre/directions/</loc>
        </url>

        <url>
            <loc>${Config.frontend}/the-centre/parking/</loc>
        </url>

        <url>
            <loc>${Config.frontend}/the-centre/sustainability/</loc>
        </url>

        <url>
            <loc>${Config.frontend}/the-centre/click-and-collect/</loc>
        </url>
    </urlset>`;
};

class Sitemap extends React.Component {
  static async getInitialProps({ res }) {
    let pages;

    await axios
      .get(`${Config.apiUrl}/wp/v2/pages?per_page=100`)
      .then((response) => {
        pages = response.data;
        pages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    let eventPages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/events?per_page=100`)
      .then((response) => {
        eventPages = response.data;
        eventPages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    let storePages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/stores?per_page=100`)
      .then((response) => {
        storePages = response.data;
        storePages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    let restaurantPages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/restaurants?per_page=100`)
      .then((response) => {
        restaurantPages = response.data;
        restaurantPages.sort((a, b) =>
          a.link.length > b.link.length ? 1 : -1
        );
      });

    let productPages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/products?per_page=100`)
      .then((response) => {
        productPages = response.data;
        productPages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    let campaignPages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/campaigns?per_page=100`)
      .then((response) => {
        campaignPages = response.data;
        campaignPages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    let amenityPages;
    await axios
      .get(`${Config.apiUrl}/wp/v2/amenities?per_page=100`)
      .then((response) => {
        amenityPages = response.data;
        amenityPages.sort((a, b) => (a.link.length > b.link.length ? 1 : -1));
      });

    res.setHeader("Content-Type", "text/xml");
    res.write(
      createSitemap({
        pages,
        eventPages,
        storePages,
        restaurantPages,
        productPages,
        campaignPages,
        amenityPages,
      })
    );
    res.end();
  }
}

export default Sitemap;
