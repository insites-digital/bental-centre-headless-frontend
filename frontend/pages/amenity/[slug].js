import React from "react";
import {
  getAmenityBySlug,
  getContent,
  getOptions,
} from "../../services/content";
import Layout from "../../components/Layout";
import SinglePage from "../../components/templates/singlePage";
import Error from "../../components/Error";
import parse from "html-react-parser";

const AmenityPage = (props) => {
  if (!props || !props.amenity) return <></>;

  const {
    products,
    events,
    amenities,
    campaigns,
    restaurants,
    amenityCats,
    stores,
    globalContent,
  } = props;
  // Send to error handling
  if (props.amenity.length === 0)
    return (
      <Layout
        data={globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: null,
        }}
        pageClass={"page"}
        amenities={amenities}
        amenityCats={amenityCats}
        products={products}
        restaurants={restaurants}
        stores={stores}
        events={events}
        campaigns={campaigns}
      >
        <Error />
      </Layout>
    );

  const amenity = props.amenity;
  const post = amenity;
  post.breadcrumbs = [
    { name: "Services & Facilities", link: "/services-facilities/" },
    {
      name: parse(amenity.title.rendered),
      link: amenity.slug,
    },
  ];

  return (
    <Layout
      data={globalContent}
      metaData={{
        title:
          amenity.title.rendered
            .replace("&#8217;", "'")
            .replace("&#038;", "&") + " | Bentall Centre | Kingston",
        meta: amenity.yoast_head,
      }}
      pageClass={amenity.slug}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <SinglePage
        data={post}
        stores={props.stores}
        restaurants={props.restaurants}
        slug={post.slug}
      />
    </Layout>
  );
};

// TEMP FOR STAGING
// REMOVE GET SERVER SIDE PROPS FOR GO LIVE AND BRING BACK GET STATIC PROPS
// export async function getServerSideProps(context) {
//   const res = await fetch(
//     `${Config.apiUrl}/wp/v2/amenities?slug=${context.params.slug}`
//   );
//   const amenity = await res.json();

//   const resStores = await fetch(`${Config.apiUrl}/wp/v2/stores?per_page=100`);
//   const stores = await resStores.json();

//   const resAmenities = await fetch(
//     `${Config.apiUrl}/wp/v2/amenities?per_page=100`
//   );
//   const amenities = await resAmenities.json();

//   const resAmenityCats = await fetch(
//     `${Config.apiUrl}/wp/v2/amenity-category?per_page=100`
//   );
//   const amenityCats = await resAmenityCats.json();

//   const resGlobal = await fetch(
//     `${Config.apiUrl}/wp/v2/acf/options?per_page=100`
//   );
//   const globalContent = await resGlobal.json();

//   const resRestaurants = await fetch(
//     `${Config.apiUrl}/wp/v2/restaurants?per_page=100`
//   );
//   const restaurants = await resRestaurants.json();

//   const resCampaigns = await fetch(
//     `${Config.apiUrl}/wp/v2/campaigns?per_page=100`
//   );
//   const campaigns = await resCampaigns.json();

//   const resEvents = await fetch(`${Config.apiUrl}/wp/v2/events?per_page=100`);
//   const events = await resEvents.json();

//   const resProducts = await fetch(
//     `${Config.apiUrl}/wp/v2/products?per_page=100`
//   );
//   const products = await resProducts.json();

//   return {
//     props: {
//       amenity,
//       products,
//       events,
//       amenities,
//       campaigns,
//       restaurants,
//       amenityCats,
//       stores,
//       globalContent,
//     },
//   };
// }

export async function getStaticProps(context) {
  const globalContent = await getOptions();
  const amenity = await getAmenityBySlug(context.params.slug);
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  // const res = await fetch(
  //   `${Config.apiUrl}/wp/v2/amenities?slug=${context.params.slug}`
  // );
  // const amenity = await res.json();

  // const resStores = await fetch(`${Config.apiUrl}/wp/v2/stores?per_page=100`);
  // const stores = await resStores.json();

  // const resAmenities = await fetch(
  //   `${Config.apiUrl}/wp/v2/amenities?per_page=100`
  // );
  // const amenities = await resAmenities.json();

  // const resAmenityCats = await fetch(
  //   `${Config.apiUrl}/wp/v2/amenity-category?per_page=100`
  // );
  // const amenityCats = await resAmenityCats.json();

  // const resGlobal = await fetch(
  //   `${Config.apiUrl}/wp/v2/acf/options?per_page=100`
  // );
  // const globalContent = await resGlobal.json();

  // const resRestaurants = await fetch(
  //   `${Config.apiUrl}/wp/v2/restaurants?per_page=100`
  // );
  // const restaurants = await resRestaurants.json();

  // const resCampaigns = await fetch(
  //   `${Config.apiUrl}/wp/v2/campaigns?per_page=100`
  // );
  // const campaigns = await resCampaigns.json();

  // const resEvents = await fetch(`${Config.apiUrl}/wp/v2/events?per_page=100`);
  // const events = await resEvents.json();

  // const resProducts = await fetch(
  //   `${Config.apiUrl}/wp/v2/products?per_page=100`
  // );
  // const products = await resProducts.json();

  return {
    revalidate: 1,
    props: {
      amenity,
      amenityCats,
      globalContent,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export async function getStaticPaths() {
  const amenities = await getContent("amenities");

  return {
    paths: amenities.map((amenity) => {
      return { params: { slug: amenity["slug"] } };
    }),
    fallback: true,
  };
}

export default AmenityPage;
