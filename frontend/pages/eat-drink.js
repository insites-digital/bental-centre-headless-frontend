import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import EatDrink from "../components/templates/eatDrink";
import Error from "../components/Error";

const Restaurants = (props) => {
  if (!props || !props.restaurants || !props.page) return <></>;

  //Error handling
  if (props.restaurants.length === 0)
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: [],
        }}
        pageClass={"page"}
        amenities={props.amenities}
        amenityCats={props.amenityCats}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        campaigns={props.campaigns}
      >
        <Error />
      </Layout>
    );

  const {
    globalContent,
    events,
    eventCats,
    amenities,
    amenityCats,
    stores,
    products,
    restaurantCats,
    restaurants,
    page,
    floors,
    campaigns,
  } = props;

  // Find any sticky restaurants
  let stickyRestaurants = restaurants.filter((restaurant) => {
    return (
      restaurant.hasOwnProperty("acf") &&
      restaurant.acf.hasOwnProperty("stick_post") &&
      restaurant.acf.stick_post === true
    );
  });

  const compare = (a, b) => {
    const bandA = a.acf.sticky_order;
    const bandB = b.acf.sticky_order;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  };

  stickyRestaurants.sort(compare);

  const filteredRestaurants = restaurants.filter(function (restaurant) {
    return !stickyRestaurants.find(function (sticky) {
      return restaurant.slug === sticky.slug;
    });
  });

  const orderedRestaraunts = [...stickyRestaurants, ...filteredRestaurants];

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: "Food & Drink | Bentall Centre | Kingston",
        meta: page.yoast_head,
      }}
      pageClass="eat-drink"
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <EatDrink
        data={page}
        events={events}
        eventCats={eventCats}
        stores={stores}
        products={products}
        restaurantCats={restaurantCats}
        restaurants={
          orderedRestaraunts.length === 0 ? restaurants : orderedRestaraunts
        }
        categories={restaurantCats}
        floors={floors}
      />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  let globalContent,
    page,
    amenities,
    amenityCats,
    restaurants,
    restaurantCats,
    stores,
    campaigns,
    products,
    events,
    eventCats,
    floors;

  globalContent = await getOptions();
  page = await getPageBySlug("eat-drink");
  amenityCats = await getContent("amenity-category");
  amenities = await getContent("amenities");
  products = await getContent("products");
  campaigns = await getContent("campaigns");
  events = await getContent("events");
  eventCats = await getContent("event-category");
  restaurants = await getContent("restaurants");
  restaurantCats = await getContent("restaurant-category");
  stores = await getContent("stores");
  floors = await getContent("floor");

  return {
    revalidate: 1,
    props: {
      page,
      amenityCats,
      eventCats,
      floors,
      restaurantCats,
      globalContent,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export default Restaurants;
