import React from "react";
import { getContent, getOptions } from "../services/content";

import Layout from "../components/Layout";
import DOTD from "../components/templates/DOTD";

const DealOfTheDay = (props) => {
  const {
    products,
    stores,
    restaurants,
    campaigns,
    events,
    globalContent,
    amenities,
    amenityCats,
  } = props;

  const meta =
    '<meta name="description" content="Discover our deal of the day at the Bentall Centre Kingston."><meta property="og:locale" content="en_GB"><meta property="og:type" content="article"><meta property="og:title" content="Deal of the day | Bentall Centre | Kingston"><meta property="og:description" content="Discover our deal of the day at the Bentall Centre Kingston."><meta property="og:url" content="/deal-of-the-day/"><meta property="og:site_name" content="Bentall Centre | Kingston">';

  const page = [
    {
      slug: "deal-of-the-day",
      yoast_title: "Deal of the Day | Bentall Centre | Kingston",
      yoast_head: meta,
      title: {
        rendered: "Deal of the Day",
      },
      deals: globalContent.daily_deals,
    },
  ];
  return (
    <Layout
      data={globalContent}
      metaData={{
        title: page[0]
          ? page[0].title.rendered
              .replace("&#8217;", "'")
              .replace("&#038;", "&") + " | Bentall Centre | Kingston"
          : "Deal of the Day | Bentall Centre | Kingston",
        meta: page[0].yoast_head,
      }}
      pageClass={"dotd"}
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      <DOTD data={page[0]} products={products} stores={stores} />
    </Layout>
  );
};

export async function getServerSideProps() {
  const globalContent = await getOptions();
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    props: {
      globalContent,
      amenityCats,
      amenities: amenities.filter((item) => item.status === "publish"),
      stores: stores.filter((item) => item.status === "publish"),
      events: events.filter((item) => item.status === "publish"),
      products: products.filter((item) => item.status === "publish"),
      campaigns: campaigns.filter((item) => item.status === "publish"),
      restaurants: restaurants.filter((item) => item.status === "publish"),
    },
  };
}

export default DealOfTheDay;
