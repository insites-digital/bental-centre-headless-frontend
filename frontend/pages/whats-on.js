import React from "react";
import { getPageBySlug, getContent, getOptions } from "../services/content";
import Layout from "../components/Layout";
import HeroText from "../components/HeroText";
import TabbedWhatsOn from "../components/tabbedContent/TabbedWhatsOn";
import Error from "../components/Error";

const WhatsOn = (props) => {
  if (!props || !props.events) return <></>;
  //Error handling
  if (props.events.length === 0)
    return (
      <Layout
        data={props.globalContent}
        metaData={{
          title: "Page not found | Bentall Centre | Kingston",
          meta: null,
        }}
        pageClass={"page"}
        amenities={props.amenities}
        amenityCats={props.amenityCats}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        campaigns={props.campaigns}
      >
        <Error />
      </Layout>
    );

  const {
    globalContent,
    events,
    products,
    restaurants,
    campaigns,
    stores,
    eventCats,
    amenities,
    amenityCats,
    page,
  } = props;

  const heroContent = page.acf.components[0].content;

  return (
    <Layout
      data={globalContent}
      metaData={{
        title: page.title.rendered.replace("&#8217;", "'"),
        meta: page.yoast_head,
      }}
      pageClass={
        page.title.rendered.replace("&#8217;", "'").replace("&#038;", "&") +
        " | Bentall Centre | Kingston"
      }
      showServices={true}
      amenities={amenities}
      amenityCats={amenityCats}
      products={products}
      restaurants={restaurants}
      stores={stores}
      events={events}
      campaigns={campaigns}
    >
      {heroContent && heroContent !== "" && (
        <HeroText
          data={{
            description: heroContent,
          }}
        />
      )}

      <TabbedWhatsOn
        events={events}
        categories={eventCats}
        title={page.title.rendered}
      />
    </Layout>
  );
};

// TODO: Change back to getStaticProps for go live.
export async function getStaticProps() {
  const globalContent = await getOptions();
  const page = await getPageBySlug("whats-on");
  const amenityCats = await getContent("amenity-category");
  const amenities = await getContent("amenities");
  const products = await getContent("products");
  const campaigns = await getContent("campaigns");
  const events = await getContent("events");
  const eventCats = await getContent("event-category");
  const restaurants = await getContent("restaurants");
  const stores = await getContent("stores");

  return {
    revalidate: 1,
    props: {
      page,
      stores: stores.filter((item) => item.status === "publish"),
      products,
      campaigns: campaigns.filter((item) => item.status === "publish"),
      eventCats,
      restaurants: restaurants.filter((item) => item.status === "publish"),
      amenities: amenities.filter((item) => item.status === "publish"),
      amenityCats,
      events: events.filter((item) => item.status === "publish"),
      globalContent,
    },
  };
}

export default WhatsOn;
