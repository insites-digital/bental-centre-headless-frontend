# Bentall Centre Next.js - Frontend

Staging Frontend 👉 https://bentall-headless-frontend.betastaging.co.uk

Staging CMS 👉 https://bentall-headless-cms.betastaging.co.uk/wp-login.php

Live Frontend 👉 https://www.bentallcentre.co.uk

Live Backend 👉 https://api.bentallcentre.co.uk/wp-login.php

This is the frontend code for the Bentalls Centre website using Next.js. It connects with a WordPress CMS to retrieve the website data using REST APIs and a mixture of static generation and server side generation (mainly static is used).

## Prerequisites

You will need Node.js version 10.0.3 or greater installed on your system.

**Using version 12.22.12 via NVM seemed to work. Version 16 Did not, due to node-sass issues**

You will also need Express version 4.17.1 or greater installed on your system. There are several other packages, but all packages should be downloadable from the package.json file.

## Setup

Clone the 'Bental Centre Headless Frontend' project in Sourcetree or,
Get the code by cloning this repository using git

git clone https://[INSERT-YOUR-USERNAME-HERE]@bitbucket.org/insites-digital/bental-centre-headless-frontend.git

Once downloaded, open the terminal in the project directory, and install dependencies with:

- ~~npm install~~
- npm install --legacy-peer-deps
- npm run dev

The app should now be up and running at http://localhost:3000 🚀

You can change the port in the .env file or the server.js file.

## Important things to note:

# Redux

Redux has been setup in this project, but is not used. At the time of creation, I (Becky) was unable to get it working with Next.js. This was my first ever Next.js project so I was learning as I coded.

# CSS

The CSS has become messy and makes use of non-ideal tags such as !important. This happened after CSS that was written for this project in support of the workload was written outside of these code files and then had to be added/pasted in. After adding/pasting in the new CSS files/code, it clashed with alot of styles that were already written, hence the use of non-ideal tags to get the styles to work without having to rewrite entire CSS when there was a tight deadline. In an ideal world, it would be redone and make use of a CSS framework like TailWind.

# Node.js / Express

This project makes use of node.js and has an express server set up. This server is what runs the project and determines what port to run it on. This has been used for several reasons:

- To handle redirects

- To be able to show a different page/component based on a specific URL. For example:

  # server.get("/the-centre/opening-hours", (req, res) => {

  # const actualPage = "/the-centre";

  # const queryParams = { q: "opening-hours" };

  # app.render(req, res, actualPage, queryParams);

  # });

  This example shows the server looking out for any get requests to the /the-centre/opening-hours url, and when requested it changes the page it should show. In this example it sets a page called 'the-centre' as the page to show to the user.

- To be able to pass query parameters to pages. In the above example it shows a query parameter variable which is set to an object 'q' with the value 'opening-hours', this is then passed to the page that will be displayed and can be accessed in the getServerSideProps or getStaticProps feature. This is handy if you want to tell a page to do something programmatically. In the case of the example above, we needed the users to be able to click on a link directly to the opening hours, however this has been made as a JavaScript tabbed section and not individual pages. So by using the example above, we were able to 'create' that opening hours url and tell that page to programatically open the opening hours tab on page load.
