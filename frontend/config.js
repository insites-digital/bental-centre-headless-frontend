let wpUrl = "https://api.bentallcentre.co.uk/wp-json";
let hostName = "api.bentallcentre.co.uk";
let frontend = "https://www.bentallcentre.co.uk";

const Config = {
  apiUrl: wpUrl,
  hostName: hostName,
  frontend: frontend,
};

export default Config;
