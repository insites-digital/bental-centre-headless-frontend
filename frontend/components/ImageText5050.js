import React from "react";
import parse from "html-react-parser";
import Image from "./Image";

const ImageText5050 = (props) => {
  const imageHeight = props.data.image.url.height;
  const imageWidth = props.data.image.url.width;

  return (
    <section className="image-text-50 container">
      <div className="image-text-50__image">
        <div
          style={{
            position: "relative",
            paddingTop: `calc(${imageHeight}/ ${imageWidth} * 100%)`,
          }}
        >
          {/* <img src={props.data.image.url} alt="" /> */}
          <Image data={props.data.image} imageSize={"50vw"} />
        </div>
      </div>
      <div className="image-text-50__text">{parse(props.data.Text)}</div>
    </section>
  );
};

export default ImageText5050;
