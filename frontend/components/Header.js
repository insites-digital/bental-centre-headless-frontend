import React from "react";
import Head from "next/head";
import stylesheet from "../styles/all.scss";
import Config from "../config";
import parse from "html-react-parser";
import FacebookPixel from "./FacebookPixel";
import GoogleAnalytics from "./GoogleAnalytics";
import TeadsPixel from "./TeadsPixel";

const Header = (props) => {
  let title =
    props.data.hasOwnProperty("title") &&
    props.data.title &&
    props.data.title !== ""
      ? props.data.title
      : "Bentall Centre | Kingston";
  let meta = null;
  if (
    props.data.hasOwnProperty("meta") &&
    props.data.meta &&
    props.data.meta.length > 0
  ) {
    meta = props.data.meta;
  }
  if (meta !== null) {
    const metaRobotsTest = /^<meta +name="robots" +content="([^"]+)" *\/>$/gm;
    const metaRobots = meta.match(metaRobotsTest);
    const replaceURL = "https://" + Config.hostName;
    const re = new RegExp(replaceURL, "g");
    meta = meta.replace(metaRobots, "");
    meta = meta.replace("https://" + Config.hostName, "");
    meta = meta.replace(re, Config.frontend);
    meta = parse(meta);
  }

  return (
    <Head>
      <style
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: stylesheet }}
      />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      {meta && meta}
      <title>{title.replace(/&amp;/g, "&").replace(/&#039;/g, "'")}</title>
      <link rel="shortcut icon" href="/static/favicon.ico" />
      {FacebookPixel()}
      {GoogleAnalytics()}
      {TeadsPixel()}
    </Head>
  );
};

export default Header;
