import React, { useState, useEffect } from "react";
import crossIcon from "../static/images/site/cross.svg";
import minusIcon from "../static/images/site/minus.svg";

const FooterSection = (props) => {
  const { classMod, title, innerContent, footContent } = props.data;

  const [windowSize, setWindowSize] = useState(961);
  const [openAccordion, setOpenAccordion] = useState(false);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  const toggleAccordion = () => {
    const openAccordions = document.querySelectorAll(
      ".footer__section-content--show"
    );
    if (openAccordions) {
      openAccordions.forEach((item) => {
        const accordionButton = item.parentNode.querySelector(
          ".footer__section-header"
        );
        if (accordionButton) {
          accordionButton.click();
        }
      });
    }

    setOpenAccordion(!openAccordion);
  };

  return (
    <div className={`footer__section ${classMod}`}>
      <div className="footer__section-header" onClick={toggleAccordion}>
        <h2>{title}</h2>
        {windowSize <= 960 && (
          <button className="footer-accordion__button">
            {!openAccordion && <img src={crossIcon} alt="" />}
            {openAccordion && <img src={minusIcon} alt="" />}
          </button>
        )}
      </div>
      <div
        className={
          "footer__section-content" +
          (openAccordion ? " footer__section-content--show" : "")
        }
      >
        {innerContent}
      </div>
      <div className="footer__section-foot">{footContent}</div>
    </div>
  );
};

export default FooterSection;
