import _ from "lodash";
import React, { Component, useEffect } from "react";
import { Search } from "semantic-ui-react";
import sw from "stopword";
import decode from "decode-html";
import he from "he";

const initialState = {
  isLoading: false,
  results: [],
  value: "",
  showResultsDropdown: true,
};

export default class ContentSearch extends Component {
  state = initialState;

  searchData = this.props.searchData;

  componentDidMount() {
    document
      .querySelector(".react-tabs input")
      .addEventListener("keypress", (e) => {
        if ((e.key === "Enter") & (this.state.value !== "")) {
          window.location.href = `/search?keyword=${decode(
            he.decode(this.state.value)
          )}`;
        }
      });
  }

  handleResultSelect = (e, { result }) => {
    this.setState({
      value: result.title.rendered.replace(/#038;/g, ""),
      showResultsDropdown: false,
    });
  };

  resultRenderer = (props) => {
    if (this.state.showResultsDropdown === false) {
      document.querySelector(".content-search .results").style.visibility =
        "hidden";
      return <></>;
    } else {
      document.querySelector(".content-search .results").style.visibility =
        "visible";
    }
    return (
      <p className="content-search__item">
        {decode(he.decode(props.title.rendered))}
      </p>
    );
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value, showResultsDropdown: true });

    setTimeout(() => {
      if (this.state.value.length < 1) {
        this.props.handleSearchChange([]);
        return this.setState(initialState);
      }

      let isMatch = (result) => {
        let searchTerms = sw.removeStopwords(this.state.value.split(" "));
        searchTerms = searchTerms.filter((el) => el != "");

        let match = false;
        let matches = 0;
        searchTerms.forEach((term) => {
          const termRegex = new RegExp(_.escapeRegExp(term), "i");
          if (termRegex.test(decode(he.decode(result.title.rendered)))) {
            match = true;

            if (
              decode(he.decode(result.title.rendered)).charAt(0) ===
              term.charAt(0)
            ) {
              matches = matches + 2000;
            } else {
              matches = matches + 1000;
            }
          }
          if (
            termRegex.test(result.content.rendered.replace(/(<([^>]+)>)/gi, ""))
          ) {
            match = true;
            matches = matches + 1;
          }
          if (matches === 0 && term.substr(term.length - 1) === "s") {
            let nonPluralRegex = new RegExp(
              _.escapeRegExp(term.substr(0, term.length - 1)),
              "i"
            );
            if (nonPluralRegex.test(decode(he.decode(result.title.rendered)))) {
              match = true;
              matches = matches + 1;
            }

            if (
              nonPluralRegex.test(
                decode(
                  he.decode(
                    result.content.rendered.replace(/(<([^>]+)>)/gi, "")
                  )
                )
              )
            ) {
              match = true;
              matches = matches + 1;
            }
          }
          if (matches > 0) {
            result.matches = matches;
          }
        });
        return match;
      };

      let newResults = _.filter(this.props.searchData, isMatch);

      newResults = _.orderBy(newResults, ["matches"], ["desc"]);

      this.setState(
        {
          isLoading: false,
          //results: _.filter(this.props.searchData, isMatch),
          results: newResults,
        },
        () => {
          this.props.handleSearchChange(this.state.results);
        }
      );
    }, 300);
  };

  render() {
    const { isLoading, value, results } = this.state;

    return (
      <div className="content-search">
        <div className="content-search__container">
          <Search
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {
              leading: true,
            })}
            placeholder={
              this.props.placeholder ? this.props.placeholder : "Search..."
            }
            resultRenderer={this.resultRenderer}
            results={results}
            value={decode(he.decode(value))}
            {...this.props}
          />
        </div>
      </div>
    );
  }
}
