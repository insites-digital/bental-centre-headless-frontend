import React, { Component } from "react";
import Config from "../config";
import fetch from "isomorphic-unfetch";
import _ from "lodash";
import parse from "html-react-parser";
import { Search } from "semantic-ui-react";
import sw from "stopword";
import Router from "next/router";
import Link from "next/link";
import { getSlug } from "../utils/getSlug";
import he from "he";

const initialState = { isLoading: false, results: [], value: "" };

const decodeString = (encodedStr) => {
  let parser = new DOMParser();
  let dom = parser.parseFromString(
    "<!doctype html><body>" + encodedStr,
    "text/html"
  );
  return dom.body.textContent;
};

class GlobalSearch extends Component {
  state = initialState;

  getCentreDetails() {
    let centreDetails = [];

    for (var prop in this.props.globalContent.centre_details) {
      if (
        Object.prototype.hasOwnProperty.call(
          this.props.globalContent.centre_details,
          prop
        )
      ) {
        if (this.props.globalContent.centre_details[prop]) {
          let property = {
            title: { rendered: "" },
            content: { rendered: "" },
            link: "",
          };
          switch (prop) {
            case "general":
              property.title.rendered = "Centre information";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre";
              break;
            case "opening_hours":
              property.title.rendered = "Centre opening hours";
              property.content.rendered =
                "opening opening hours Opening Hours Bentall Centre opening hours";
              property.link = "/the-centre/opening-hours";
              property.acf = {
                opening_hours: this.props.globalContent.centre_details
                  .opening_hours,
              };
              break;
            case "about_us":
              property.title.rendered = "About the Centre";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre/about-us";
              break;
            case "directions":
              property.title.rendered = "Directions to the Centre";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre/directions";
              break;
            case "parking":
              property.title.rendered = "Parking at the Centre";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre/parking";
              break;
            case "sustainability":
              property.title.rendered = "Sustainability";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre/sustainability";
              break;
            case "click_collect":
              property.title.rendered = "Click and collect";
              property.content.rendered = this.props.globalContent.centre_details[
                prop
              ].content;
              property.link = "/the-centre/click-and-collect";
              break;
          }
          centreDetails.push(property);
        }
      }
    }
    return centreDetails;
  }

  // set the search data from the data passed in props.
  searchData = {
    stores: {
      name: "Stores & Brands",
      results: this.props.stores,
    },
    centre: {
      name: "The Centre",
      results: this.getCentreDetails(),
    },
    products: {
      name: "Products",
      results: this.props.products,
    },
    restaurants: {
      name: "Restaurants",
      results: this.props.restaurants,
    },
    amenities: {
      name: "Services & Facilities",
      results: this.props.amenities,
    },
    events: {
      name: "Events",
      results: this.props.events,
    },
    campaigns: {
      name: "Campaigns",
      results: this.props.campaigns,
    },
  };

  componentDidMount() {
    // get the keyword/s
    let keyword = Router.router.asPath;
    if (keyword.indexOf("?keyword=") >= 0) {
      keyword = keyword.substr(
        keyword.indexOf("?keyword=") + 9,
        keyword.length
      );

      if (keyword !== "") {
        this.setState({ value: decodeURIComponent(keyword) });
      }
    }
    // navigate to the search results page if someone hits enter on the keyboard
    document
      .querySelector(".search input")
      .addEventListener("keypress", (e) => {
        if ((e.key === "Enter") & (this.state.value !== "")) {
          window.location.href = `/search?keyword=${decodeString(
            he.decode(this.state.value)
          )}`;
        }
      });
  }

  // Method to select the result
  handleResultSelect = async (e, { result }) => {
    this.setState({ value: result.title.rendered });
  };

  // Method to handle search change
  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value });
    // Below removes common stop words to improve the accuracy of the search results
    let searchTerms = sw.removeStopwords(this.state.value.split(" "));

    setTimeout(() => {
      if (this.state.value.length < 1) return this.setState(initialState);

      // Look for a match
      let isMatch = (result) => {
        searchTerms = searchTerms.filter((el) => el != "");

        let match = false;
        let matches = 0;
        searchTerms.forEach((term) => {
          const termRegex = new RegExp(_.escapeRegExp(term), "i");
          if (termRegex.test(decodeString(he.decode(result.title.rendered)))) {
            match = true;
            if (
              decodeString(he.decode(result.title.rendered)).charAt(0) ===
              term.charAt(0)
            ) {
              matches = matches + 2000;
            } else {
              matches = matches + 1000;
            }
          }
          if (
            termRegex.test(result.content.rendered.replace(/(<([^>]+)>)/gi, ""))
          ) {
            match = true;
            matches = matches + 1;
          }
          // Customised results for the opening hours as the client specified they would like the users to be able to search for opening hours.
          if (
            result.hasOwnProperty("acf") &&
            result.acf.hasOwnProperty("opening_hours")
          ) {
            if (
              termRegex.test("opening hours") ||
              termRegex.test("opening hour")
            ) {
              match = true;
              matches = matches + 1;
            }
          }
          // If the search term has been pluralised such as 'Bentalls' and it gives 0 results, check the match for the non plural version, i.e. 'Bentall'
          if (matches === 0 && term.substr(term.length - 1) === "s") {
            let nonPluralRegex = new RegExp(
              _.escapeRegExp(term.substr(0, term.length - 1)),
              "i"
            );
            if (
              nonPluralRegex.test(
                decodeString(he.decode(result.title.rendered))
              )
            ) {
              match = true;
              matches = matches + 1;
            }

            if (
              nonPluralRegex.test(
                decodeString(
                  he.decode(
                    result.content.rendered.replace(/(<([^>]+)>)/gi, "")
                  )
                )
              )
            ) {
              match = true;
              matches = matches + 1;
            }
          }
          if (matches > 0) {
            result.matches = matches;
          }
        });
        return match;
      };

      // Filter down the results to the 'isMatched' results declared above
      const filteredResults = _.reduce(
        this.searchData,
        (memo, data, name) => {
          let results = _.filter(data.results, isMatch);

          // Below is additional code to support the functionality for opening hours to show up when searched for.
          if (searchTerms.includes("opening") && searchTerms.includes("hour")) {
            if (results.length > 0) {
              results = results.sort(function (a, b) {
                var textA = a.title.rendered.toUpperCase();
                var textB = b.title.rendered.toUpperCase();
                return textA < textB ? -1 : textA > textB ? 1 : 0;
              });
            }
          } else {
            results = results.sort((a, b) => {
              return b.matches - a.matches;
            });
          }

          if (results.length && data && data.name) {
            let dataName = data.name;
            memo[name] = { name: dataName, results };
          } // eslint-disable-line no-param-reassign

          // return data
          return memo;
        },
        {}
      );
      //let newResults = _.orderBy(filteredResults, ["matches"], ["desc"]);
      // set state with updated search results
      this.setState({
        isLoading: false,
        results: filteredResults,
      });
    }, 300);
  };

  categoryLayoutRenderer = (props) => {
    const { categoryContent, resultsContent } = props;
    return (
      <div>
        <h3 className="category-title">{categoryContent}</h3>
        <div className="category-results">{resultsContent}</div>
      </div>
    );
  };

  resultRenderer = (props) => {
    let title = decodeString(he.decode(props.title.rendered));
    let link = getSlug(props.link);
    let splitTitle = title.split(" ");
    let splitTitleLowerCase = splitTitle.map((title) => title.toLowerCase());
    let searchTerms = sw.removeStopwords(this.state.value.split(" "));
    searchTerms = searchTerms.filter((el) => el != "");
    let formattedSearchTerms = new Set(searchTerms);
    [...formattedSearchTerms].forEach((term) => {
      const lowerCaseTerm = term.toLowerCase();
      let i;
      for (i = 0; i < splitTitleLowerCase.length; i++) {
        if (splitTitleLowerCase[i].indexOf(lowerCaseTerm) >= 0) {
          let formattedTitle;
          // If the item to be made bold is at the beggining of the sentence
          if (splitTitleLowerCase[i].indexOf(lowerCaseTerm) === 0) {
            // if the title is already made bold,don't do it again
            if (
              splitTitle[i].substr(0, 8) === "<strong>" &&
              splitTitle[i].substr(splitTitle[i].length - 9) === "</strong>"
            ) {
              formattedTitle = splitTitle.join(" ");
              // Otherwise, add the bold tag
            } else {
              var opening = "<strong>";
              var reOpening = new RegExp(opening, "g");
              var closing = "</strong>";
              var reClosing = new RegExp(closing, "g");
              splitTitle[i] = splitTitle[i].replace(reOpening, "");
              splitTitle[i] = splitTitle[i].replace(reClosing, "");
              formattedTitle =
                "<strong>" +
                splitTitle[i].substr(
                  splitTitleLowerCase[i].indexOf(lowerCaseTerm),
                  lowerCaseTerm.length
                ) +
                "</strong>";
              // if there are characters after the match, concatenate them to the end of the string
              if (splitTitle[i].length > lowerCaseTerm.length) {
                formattedTitle += splitTitle[i].substr(
                  lowerCaseTerm.length,
                  splitTitle[i].length
                );
              }
            }
          } else {
            // If the item is already made bold, don't do it again
            if (
              splitTitle[i].substr(0, 8) === "<strong>" &&
              splitTitle[i].substr(splitTitle[i].length - 9) === "</strong>"
            ) {
              formattedTitle = splitTitle[0];
            } else {
              // Otherwise remove any previous bold tags so that it doesn't intefere with the new set of bold tags.
              var opening = "<strong>";
              var reOpening = new RegExp(opening, "g");
              var closing = "</strong>";
              var reClosing = new RegExp(closing, "g");
              splitTitle[i] = splitTitle[i].replace(reOpening, "");
              splitTitle[i] = splitTitle[i].replace(reClosing, "");
              formattedTitle =
                splitTitle[i].substr(
                  0,
                  splitTitleLowerCase[i].indexOf(lowerCaseTerm)
                ) +
                "<strong>" +
                splitTitle[i].substr(
                  splitTitleLowerCase[i].indexOf(lowerCaseTerm),
                  lowerCaseTerm.length
                ) +
                "</strong>";
              if (
                parseInt(splitTitleLowerCase[i].indexOf(lowerCaseTerm)) +
                  lowerCaseTerm.length <
                splitTitleLowerCase[i].length
              ) {
                formattedTitle += splitTitle[i].substr(
                  splitTitleLowerCase[i].indexOf(lowerCaseTerm) +
                    lowerCaseTerm.length,
                  splitTitleLowerCase[i].length
                );
              }
            }
          }
          // Set the new formatted title
          splitTitle[i] = formattedTitle;
        } else if (lowerCaseTerm.substr(lowerCaseTerm.length - 1) === "s") {
          // If the last letter is plural but should still match. For example, you are searching for Lates but there is a match of Late
          if (
            splitTitleLowerCase[i].indexOf(
              lowerCaseTerm.substr(0, lowerCaseTerm.length - 1)
            ) >= 0
          ) {
            splitTitle[i] = "<strong>" + splitTitle[i] + "</strong>";
          }
        }
      }
    });

    return (
      <span>
        <Link as={link} href={link}>
          <a>{parse(splitTitle.join(" "))}</a>
        </Link>
      </span>
    );
  };

  render() {
    const { isLoading, value, results } = this.state;
    return (
      <Search
        category
        placeholder={"What are you searching for?"}
        categoryLayoutRenderer={this.categoryLayoutRenderer}
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={_.debounce(this.handleSearchChange, 500, {
          leading: true,
        })}
        resultRenderer={this.resultRenderer}
        results={results}
        value={value === "" ? "" : parse(value)}
        {...this.props}
      />
    );
  }
}

export default GlobalSearch;
