import React, { useState, useEffect } from "react";
import Swiper from "react-id-swiper";
import parse from "html-react-parser";
import "swiper/css/swiper.css";
import VideoPlayer from "./video/VideoPlayer";
import { getSlug } from "../utils/getSlug";
import blackArrow from "../static/images/general/arrow-black.svg";
import Image from "./Image";
import { useRouter } from "next/router";
import Vimeo from "@u-wave/react-vimeo";

const Hero = (props) => {
  const { data } = props;
  const router = useRouter();
  const [swiper, setSwiper] = useState(null);
  const [slidePos, setSlidePos] = useState(0);
  const [embedCode, setEmbedCode] = useState(
    data && data[0].video != undefined && data[0].video.video_link
      ? `${data[0].video.video_link.toString()}`
      : null
  );
  const [dataTiming, setDataTiming] = useState(
    data
      ? data.map((item) => {
          let itemDuration;
          if (item.type === "video") {
            if (item.video && item.video.video_duration) {
              itemDuration = item.video.video_duration * 1000;
            } else {
              itemDuration = 30000;
            }
          } else {
            itemDuration = 30000;
          }
          itemDuration += "";
          return itemDuration;
        })
      : null
  );

  const increaseSlidePos = () => {
    setSlidePos(slidePos + 1);
  };

  const decreaseSlidePos = () => {
    setSlidePos(slidePos - 1);
  };

  const goNext = () => {
    if (swiper !== null) {
      if (slidePos !== data.length - 1) {
        increaseSlidePos();
      }
      swiper.slideNext();
    }
  };

  const goPrev = () => {
    if (swiper !== null) {
      swiper.slidePrev();
      if (slidePos !== 0) {
        decreaseSlidePos();
      }
    }
  };

  const params = {
    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    },
    autoplay: {
      delay: 8000,
      disableOnInteraction: false,
    },
  };

  useEffect(() => {
    if (!swiper) return;
    swiper.on("slideChange", () => {
      setSlidePos(swiper.activeIndex);
    });
  }, [swiper]);

  return (
    <>
      {data && (
        <div className="hero">
          <div className="hero__container">
            <Swiper getSwiper={setSwiper} {...params}>
              {data.map((item, index) => {
                if (item.type === "video") {
                  return (
                    <div
                      key={`video-${index}`}
                      data-swiper-autoplay={dataTiming[index]}
                      onClick={(e) => {
                        if (
                          item.link &&
                          !e.target.classList.contains("video-react-control")
                        ) {
                          router.push(getSlug(item.link));
                        }
                      }}
                    >
                      {item.hasOwnProperty("hero_text") && (
                        <div className="campaign-feature__content">
                          {item.hero_text.hasOwnProperty("heading") &&
                            item.hero_text.heading !== "" && (
                              <h2
                                style={{
                                  color:
                                    item.hero_text.hasOwnProperty(
                                      "text_colour"
                                    ) && item.hero_text.text_colour !== ""
                                      ? item.hero_text.text_colour
                                      : "#000000",
                                }}
                              >
                                {parse(item.hero_text.heading)}
                              </h2>
                            )}
                          {item.hero_text.hasOwnProperty("sub_heading") &&
                            item.hero_text.sub_heading !== "" && (
                              <h4
                                style={{
                                  color:
                                    item.hero_text.hasOwnProperty(
                                      "text_colour"
                                    ) && item.hero_text.text_colour !== ""
                                      ? item.hero_text.text_colour
                                      : "#000000",
                                }}
                              >
                                {parse(item.hero_text.sub_heading)}
                              </h4>
                            )}
                          {item.hero_text.hasOwnProperty("description") &&
                            item.hero_text.description !== "" && (
                              <p
                                style={{
                                  color:
                                    item.hero_text.hasOwnProperty(
                                      "text_colour"
                                    ) && item.hero_text.text_colour !== ""
                                      ? item.hero_text.text_colour
                                      : "#000000",
                                }}
                              >
                                {item.hero_text.description}
                              </p>
                            )}

                          {item.hero_text.hasOwnProperty("button") &&
                            item.hero_text.button.url !== "" &&
                            item.hero_text.button.label !== "" && (
                              <a
                                className="link link--uppercase link--arrow"
                                href={getSlug(item.hero_text.button.url)}
                                style={{
                                  color:
                                    item.hero_text.hasOwnProperty(
                                      "text_colour"
                                    ) && item.hero_text.text_colour !== ""
                                      ? item.hero_text.text_colour
                                      : "#000000",
                                }}
                              >
                                {parse(item.hero_text.button.label)}{" "}
                                {/* <img className="arrow" src={blackArrow} alt="" /> */}
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="30"
                                  height="9"
                                  className="arrow"
                                  viewBox="0 0 30 9"
                                >
                                  <g
                                    fill="none"
                                    fillRule="evenodd"
                                    stroke="none"
                                    strokeWidth="1"
                                  >
                                    <g
                                      fill={
                                        item.hero_text.hasOwnProperty(
                                          "text_colour"
                                        ) && item.hero_text.text_colour !== ""
                                          ? item.hero_text.text_colour
                                          : "#000000"
                                      }
                                      fillRule="nonzero"
                                    >
                                      <path
                                        stroke={
                                          item.hero_text.hasOwnProperty(
                                            "text_colour"
                                          ) && item.hero_text.text_colour !== ""
                                            ? item.hero_text.text_colour
                                            : "#000000"
                                        }
                                        strokeWidth="0.5"
                                        d="M0 4.105L20.186 4.105"
                                      ></path>
                                      <path
                                        d="M24.3155 -1.3735L28.4155 9.5735 20.2155 9.5735z"
                                        transform="rotate(90 24.316 4.1)"
                                      ></path>
                                    </g>
                                  </g>
                                </svg>
                              </a>
                            )}
                        </div>
                      )}
                      {embedCode && (
                        <div className="embed-container">
                          <Vimeo
                            video={embedCode}
                            autoplay
                            controls={false}
                            loop={true}
                            muted={true}
                          />
                        </div>
                      )}
                    </div>
                  );
                }
                return (
                  <div className="home-hero" key={`image-${index}`}>
                    {item.link && item.link !== "" ? (
                      <a
                        href={getSlug(item.link)}
                        style={{
                          width: "100%",
                          height: "100%",
                          position: "relative",
                          display: "block",
                        }}
                      >
                        <div
                          style={{
                            position: "absolute",
                            top: "0",
                            left: "0",
                            width: "100%",
                            height: "100%",
                          }}
                        >
                          <Image data={item.image} imageSize="100vw" />
                          {item.hasOwnProperty("hero_text") && (
                            <div className="campaign-feature__content">
                              {item.hero_text.hasOwnProperty("heading") &&
                                item.hero_text.heading !== "" && (
                                  <h2
                                    style={{
                                      color:
                                        item.hero_text.hasOwnProperty(
                                          "text_colour"
                                        ) && item.hero_text.text_colour !== ""
                                          ? item.hero_text.text_colour
                                          : "#000000",
                                    }}
                                  >
                                    {parse(item.hero_text.heading)}
                                  </h2>
                                )}
                              {item.hero_text.hasOwnProperty("sub_heading") &&
                                item.hero_text.sub_heading !== "" && (
                                  <h4
                                    style={{
                                      color:
                                        item.hero_text.hasOwnProperty(
                                          "text_colour"
                                        ) && item.hero_text.text_colour !== ""
                                          ? item.hero_text.text_colour
                                          : "",
                                    }}
                                  >
                                    {parse(item.hero_text.sub_heading)}
                                  </h4>
                                )}
                              {item.hero_text.hasOwnProperty("description") &&
                                item.hero_text.description !== "" && (
                                  <p
                                    style={{
                                      color:
                                        item.hero_text.hasOwnProperty(
                                          "text_colour"
                                        ) && item.hero_text.text_colour !== ""
                                          ? item.hero_text.text_colour
                                          : "#000000",
                                    }}
                                  >
                                    {item.hero_text.description}
                                  </p>
                                )}

                              {item.hero_text.hasOwnProperty("button") &&
                                item.hero_text.button.url !== "" &&
                                item.hero_text.button.label !== "" && (
                                  <a
                                    className="link link--uppercase link--arrow"
                                    href={getSlug(item.hero_text.button.url)}
                                    style={{
                                      color:
                                        item.hero_text.hasOwnProperty(
                                          "text_colour"
                                        ) && item.hero_text.text_colour !== ""
                                          ? item.hero_text.text_colour
                                          : "#000000",
                                    }}
                                  >
                                    {parse(item.hero_text.button.label)}{" "}
                                    {/* <img className="arrow" src={blackArrow} alt="" /> */}
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="30"
                                      height="9"
                                      className="arrow"
                                      viewBox="0 0 30 9"
                                    >
                                      <g
                                        fill="none"
                                        fillRule="evenodd"
                                        stroke="none"
                                        strokeWidth="1"
                                      >
                                        <g
                                          fill={
                                            item.hero_text.hasOwnProperty(
                                              "text_colour"
                                            ) &&
                                            item.hero_text.text_colour !== ""
                                              ? item.hero_text.text_colour
                                              : "#000000"
                                          }
                                          fillRule="nonzero"
                                        >
                                          <path
                                            stroke={
                                              item.hero_text.hasOwnProperty(
                                                "text_colour"
                                              ) &&
                                              item.hero_text.text_colour !== ""
                                                ? item.hero_text.text_colour
                                                : "#000000"
                                            }
                                            strokeWidth="0.5"
                                            d="M0 4.105L20.186 4.105"
                                          ></path>
                                          <path
                                            d="M24.3155 -1.3735L28.4155 9.5735 20.2155 9.5735z"
                                            transform="rotate(90 24.316 4.1)"
                                          ></path>
                                        </g>
                                      </g>
                                    </svg>
                                  </a>
                                )}
                            </div>
                          )}
                        </div>
                      </a>
                    ) : (
                      <Image data={item.image} imageSize="100vw" />
                    )}
                  </div>
                );
              })}
            </Swiper>
            <button
              className={
                "hero__navigation hero__navigation--prev" +
                (slidePos === 0 ? " hero__navigation--hide" : "")
              }
              onClick={goPrev}
            >
              {slidePos !== 0 && (
                <span className="hero__navigation-title">
                  {parse(data[slidePos - 1].heading)}
                </span>
              )}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="29.789"
                height="8.21"
                viewBox="0 0 29.789 8.21"
              >
                <g
                  fill="#fff"
                  data-name="Group 1360"
                  transform="rotate(180 85.654 1095.589)"
                >
                  <path
                    stroke="#fff"
                    strokeWidth="0.5"
                    d="M141.519 2187.072h20.186"
                    data-name="Path 759"
                  ></path>
                  <g data-name="Polygon 1">
                    <path
                      d="M7.488 10.447H.721l3.384-9.023 3.383 9.023z"
                      transform="rotate(90 -1005.83 1177.138)"
                    ></path>
                    <path
                      d="M4.105 2.848L1.443 9.947h5.324L4.105 2.848m0-2.848L8.21 10.947H0L4.105 0z"
                      transform="rotate(90 -1005.83 1177.138)"
                    ></path>
                  </g>
                </g>
              </svg>
            </button>
            <button
              className={
                "hero__navigation hero__navigation--next" +
                (slidePos === data.length - 1 ? " hero__navigation--hide" : "")
              }
              onClick={goNext}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="29.789"
                height="8.21"
                viewBox="0 0 29.789 8.21"
              >
                <g
                  fill="#fff"
                  data-name="Group 1359"
                  transform="translate(-141.519 -2182.967)"
                >
                  <path
                    stroke="#fff"
                    strokeWidth="0.5"
                    d="M141.519 2187.072h20.186"
                    data-name="Path 759"
                  ></path>
                  <path
                    d="M4.1 0l4.1 10.947H0z"
                    data-name="Polygon 1"
                    transform="rotate(90 -1005.83 1177.138)"
                  ></path>
                </g>
              </svg>
              {slidePos !== data.length - 1 && (
                <span className="hero__navigation-title">
                  {parse(data[slidePos + 1].heading)}
                </span>
              )}
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Hero;
