import React, { useEffect, useState } from "react";
import parse from "html-react-parser";
import moment from "moment";
import "moment-timezone";
import { getSlug } from "../utils/getSlug";
import Tag from "./Tag";
import Image from "./Image";

const EventGrid = (props) => {
  const [windowSize, setWindowSize] = useState(0);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  useEffect(() => {
    if (windowSize !== 0) {
      const firstTeaser = document.querySelector(".event-grid .teaser__img");
      const height = firstTeaser.clientHeight;
      const allTeasers = document.querySelectorAll(".event-grid .teaser__img");
      allTeasers.forEach((teaser) => {
        teaser.style.paddingTop = height + "px";
      });
    }
  }, [windowSize]);

  if (!props.hasOwnProperty("data") || !props.data.hasOwnProperty("events"))
    return <></>;

  const eventIds = props.data.events.map((item) => item.ID);

  const events = props.events.filter((eventItem) => {
    if (!eventIds) return false;
    return eventIds.includes(eventItem.id);
  });

  const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;
  let emptyProductGrids = [];
  let emptyGridAmounts;

  if (windowSize >= 940) {
    if (events.length % 4 !== 0) {
      emptyGridAmounts = roundUp(events.length, 4) - events.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyProductGrids.push(
          '<div class="teaser-content teaser-content--fourth"></div>'
        );
      }
    }
  } else if (windowSize >= 700 && windowSize < 940) {
    if (events.length % 2 !== 0) {
      emptyGridAmounts = roundUp(events.length, 2) - events.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyProductGrids.push(
          '<div class="teaser-content teaser-content--fourth"></div>'
        );
      }
    }
  }

  return (
    <section className="event-grid container">
      <div className="teaser-container">
        <div className="teaser-container__top">
          <div className="teaser__title">
            <h2>Events</h2>
          </div>
        </div>
        <div className="teaser-content__container">
          {events.map((eventItem, index) => {
            let tags = props.eventCats.filter((eventCat) => {
              if (!eventItem["event-category"]) return false;
              return eventItem["event-category"].includes(eventCat.id);
            });
            tags.sort((a, b) => {
              if (!a.acf || !b.acf) return -1;
              return a.acf.colour.length < b.acf.colour.length ? 1 : -1;
            });
            const splitDate = (str) => str.split("/");
            const formattedDate = new Date(
              `${
                splitDate(eventItem.acf.dates.start_date)[1] +
                "/" +
                splitDate(eventItem.acf.dates.start_date)[0] +
                "/" +
                splitDate(eventItem.acf.dates.start_date)[2]
              }`
            );
            const formattedEndDate = new Date(
              `${
                splitDate(eventItem.acf.dates.end_date)[1] +
                "/" +
                splitDate(eventItem.acf.dates.end_date)[0] +
                "/" +
                splitDate(eventItem.acf.dates.end_date)[2]
              }`
            );
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, "0");
            var mm = String(today.getMonth() + 1).padStart(2, "0");
            var yyyy = today.getFullYear();
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, "0");
            var mm = String(today.getMonth() + 1).padStart(2, "0");
            var yyyy = today.getFullYear();

            today = mm + "/" + dd + "/" + yyyy;
            moment().tz("Europe/London");

            function getdateFormated(date) {
              var a = moment(today);
              var b = moment(date);

              var days = b.diff(a, "days");
              var weeks = Math.round(b.diff(a, "week", true));

              var calback = function () {
                if (weeks === 0) {
                  return "[In " + days + " days]";
                } else if (weeks === 1) {
                  return "[Next Week]";
                } else {
                  return `[In ${weeks} weeks]`;
                }
              };
              return moment(date).calendar(null, {
                sameDay: "[Today]",
                nextDay: "[Tomorrow]",
                nextWeek: calback,
                sameElse: calback,
              });
            }

            let dateOverview, dateHasPassed, startDateHasPassed, endsOnSameDay;

            if (formattedEndDate && formattedDate) {
              dateHasPassed = moment(formattedEndDate).isBefore(today);

              if (dateHasPassed) return <></>;

              startDateHasPassed = moment(formattedDate).isBefore(today);

              dateOverview = startDateHasPassed
                ? "today"
                : getdateFormated(formattedDate);

              endsOnSameDay = false;

              let startDayArray = eventItem.acf.dates.start_date.split("/");
              let endDayArray = eventItem.acf.dates.end_date.split("/");
              if (startDayArray[1] === endDayArray[1]) {
                if (startDayArray[0] === endDayArray[0]) {
                  endsOnSameDay = true;
                }
              }
            }

            return (
              <div className="teaser-content teaser-content--fourth">
                <a
                  href={getSlug(eventItem.link)}
                  style={{
                    display: "flex",
                    height: "100%",
                    width: "100%",
                    flexDirection: "column",
                  }}
                >
                  <div
                    className="teaser__img"
                    style={{ paddingTop: "calc(410 / 302.5 * 100%)" }}
                  >
                    <Image data={eventItem.acf.image} imageSize={"50vw"} />
                  </div>
                  <div
                    className="teaser-content__inside"
                    style={{ flexGrow: "1" }}
                  >
                    <div className="teaser__span-container teaser__span-container--date">
                      <span className="teaser__span">{dateOverview}</span>
                    </div>
                    <h2 className="teaser__subject">
                      {parse(eventItem.title.rendered)}
                    </h2>
                    <h3 className="teaser__date">
                      {parse(eventItem.acf.dates.start_date)}
                      {!endsOnSameDay && <> - {eventItem.acf.dates.end_date}</>}
                    </h3>
                  </div>
                  <div className="teaser__tag-container">
                    {tags.map((tagItem) => {
                      if (!tagItem) return;
                      <Tag colour={tagItem.acf.colour} title={tagItem.name} />;
                    })}
                  </div>
                </a>
              </div>
            );
          })}
          {emptyProductGrids.length > 0 &&
            emptyProductGrids.map((productItem) => {
              return parse(productItem);
            })}
        </div>
      </div>
    </section>
  );
};

export default EventGrid;
