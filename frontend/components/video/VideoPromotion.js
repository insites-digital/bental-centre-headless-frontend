import React from "react";
import VideoPlayer from "./VideoPlayer";
import parse from "html-react-parser";

const VideoPromotion = props => {
  return (
    <section className="video-promotion container">
      <div className="video-promotion__video-container">
        <VideoPlayer
          data={{
            image: {
              url: props.data.video.video_banner
            },
            video: { url: props.data.video.url }
          }}
        />
      </div>
      <div className="video-promotion__text">{parse(props.data.content)}</div>
    </section>
  );
};

export default VideoPromotion;
