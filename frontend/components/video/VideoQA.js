import React from "react";
import VideoPlayer from "./VideoPlayer";

const VideoQA = props => {
  return (
    <section className="video-qa container">
      <div className="video-qa__video">{/* <VideoPlayer /> */}</div>
      <article className="video-qa__text">
        <div className="video-qa__text-container">
          <h2>Sustainable Gifts from our stores</h2>
          <p>
            We spoke with &OtherStories Store Stylist here at the Bentall Centre
            to explore the new trends for SS2020.
          </p>
          <p>
            <strong>
              Q: What colours and materials are in trend this season?
            </strong>
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
            luctus pretium nisl.
          </p>

          <p>
            <strong>
              Q: What colours and materials are in trend this season?
            </strong>
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
            luctus pretium nisl. Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Quisque luctus pretium nisl. Lorem ipsum dolor sit
            amet, consectetur adipiscing elit. Quisque luctus pretium nisl.
          </p>
          <p>
            <strong>
              Q: What colours and materials are in trend this season?
            </strong>
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
            luctus pretium nisl. Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Quisque luctus pretium nisl. Lorem ipsum dolor sit
            amet, consectetur adipiscing elit. Quisque luctus pretium nisl.
          </p>
        </div>
      </article>
    </section>
  );
};

export default VideoQA;
