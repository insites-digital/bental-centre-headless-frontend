import React, { Component } from "react";
import {
  Player,
  Shortcut,
  ControlBar,
  ReplayControl,
  ForwardControl,
  CurrentTimeDisplay,
  TimeDivider,
  PlaybackRateMenuButton,
  VolumeMenuButton,
} from "video-react";

export default class VideoPlayer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = { posterImg: "" };
    //this.videoContainer = React.createRef();
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.hasOwnProperty("active") &&
      prevProps.active !== this.props.active
    ) {
      if (this.props.active) {
        //this.play();
        this.player.load();
      } else {
        this.pause();
        //this.setState({ posterImg: this.props.data.image.url });
      }
    }
  }

  play() {
    this.player.play();
  }

  pause() {
    this.player.pause();
  }

  render() {
    return (
      <>
        <div className={this.props.active ? "video-active" : ""}>
          <Player
            ref={(player) => {
              this.player = player;
            }}
            muted={true}
            autoPlay={true}
            playsInline={this.props.playsInline}
            loop={true}
          >
            <Shortcut clickable={false} />
            <source src={this.props.data.video.url} />
            <ControlBar autoHide={false}>
              <ReplayControl disabled />
              <ForwardControl disabled />
              <CurrentTimeDisplay disabled />
              <TimeDivider disabled />
              <PlaybackRateMenuButton disabled />
              <VolumeMenuButton />
            </ControlBar>
          </Player>
        </div>
      </>
    );
  }
}

VideoPlayer.defaultProps = {
  playsInline: false,
};
