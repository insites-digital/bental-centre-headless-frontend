import React, { useEffect, useState, useRef } from "react";
import NewsletterForm from "./NewsletterForm";
import closeIcon from "../static/images/site/close-icon.svg";
import { createCookie } from "../utils/createCookie";

const NewsLetterPopup = (props) => {
  const [headerHeight, setHeaderHeight] = useState(0);
  const [showPopup, setShowPopup] = useState(false);
  const popup = useRef(null);

  const exitPopupTrigger = (event) => {
    if (event.toElement === null && event.relatedTarget === null) {
      setShowPopup(true);
      document.removeEventListener("mouseout", exitPopupTrigger, true);
    }
  };

  const closePopup = () => {
    setShowPopup(false);
    createCookie("newsletterSignedUp", "false", 1);
    setTimeout(() => {
      popup.current.classList.add("popup--pushed-back");
    }, 600);
  };

  // check cookie exists: newsletterSignedUp
  const handleOnSubmit = (props) => {
    //closePopup();
  };

  useEffect(() => {
    const header = document.querySelector("header");
    const cookieExists = document.cookie.match(
      /^(.*;)?\s*newsletterSignedUp\s*=\s*[^;]+(.*)?$/
    );

    if (cookieExists === null) {
      setTimeout(() => {
        if (header) {
          setHeaderHeight(header.clientHeight);
        }
        document.addEventListener("mouseout", exitPopupTrigger, true);
      }, 5000);

      window.addEventListener("resize", () =>
        setHeaderHeight(header.clientHeight)
      );
    }

    return () => {
      window.removeEventListener("resize", () =>
        setHeaderHeight(header.clientHeight)
      );
      document.removeEventListener("mouseout", exitPopupTrigger);
    };
  }, []);
  return (
    <div
      className={`popup popup--hide popup--pushed-back ${
        showPopup ? "popup--show" : "popup--hide popup--pushed-back"
      }`}
      ref={popup}
      style={{
        top: headerHeight + "px",
        height: `calc(100vh - ${headerHeight}px)`,
        opacity: "0",
      }}
    >
      <div className="popup__close" onClick={closePopup}>
        <a role="button">
          <img src={closeIcon} alt="" />
        </a>
      </div>
      <div className="popup__form">
        <h2>Let's keep in touch</h2>
        <p>
          We only keep you up to date with the cool stuff! You can unsubscribe
          any time.
        </p>
        <NewsletterForm data={handleOnSubmit} />
      </div>
    </div>
  );
};

export default NewsLetterPopup;
