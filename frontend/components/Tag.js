import React, { useState } from "react";
import styled from "styled-components";

const HoverTag = styled.span``;

// color: ${(props) => (props.colour !== "" ? props.colour : "#000")};
// border: 1px solid ${(props) => (props.colour !== "" ? props.colour : "#000")};
// :hover {
//   background: ${(props) => (props.colour !== "" ? props.colour : "#000")};
//   color: #fff;
// }

const Tag = (props) => {
  const [tagColour, setTagColour] = useState(
    props.colour !== "" ? props.colour : "#000"
  );
  const styles = {
    border: `solid 1px ${tagColour}`,
    color: tagColour,
  };
  const hoverStyles = {
    background: tagColour,
    border: `solid 1px ${tagColour}`,
    color: "#fff",
  };
  const [mainStyles, setMainStyles] = useState(styles);
  return (
    <HoverTag
      colour={props.colour}
      style={mainStyles}
      className="teaser__tag"
      // onMouseEnter={() => setMainStyles(hoverStyles)}
      // onMouseLeave={() => setMainStyles(styles)}
    >
      {props.title}
    </HoverTag>
  );
};

export default Tag;
