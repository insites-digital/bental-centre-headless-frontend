import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import closeIcon from "../static/images/site/close-icon.svg";
import Footer from "./Footer";
import GlobalSearch from "./globalSearch";
import blackArrowIcon from "../static/images/general/arrow-black.svg";
import parse from "html-react-parser";

const MainHeader = (props) => {
  const { logo, navigation_primary, navigation_secondary, footer } = props.data;
  const {
    products,
    restaurants,
    stores,
    events,
    amenities,
    campaigns,
    globalContent,
  } = props;
  const [windowSize, setWindowSize] = useState(961);
  const [showMenu, setShowMenu] = useState(false);
  const [additionalClass, setAdditionalClass] = useState(" ");
  const [openSearch, setOpenSearch] = useState(false);
  const router = useRouter();

  useEffect(() => {
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => setWindowSize(window.innerWidth));
    let keyword = router.asPath;
    if (keyword.indexOf("?keyword=") >= 0) {
      keyword = keyword.substr(
        keyword.indexOf("?keyword=") + 9,
        keyword.length
      );
      if (keyword !== "") {
        setOpenSearch(true);
      } else {
        setOpenSearch(false);
      }
    }

    if (router.pathname === "/search") {
      setOpenSearch(false);
    }

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  useEffect(() => {
    const setMenuClass = () => {
      setAdditionalClass("hide-menu ");
    };
    if (!showMenu) {
      setTimeout(setMenuClass, 600);
    } else {
      setAdditionalClass(" ");
    }
  }, [showMenu]);

  const getSlug = (url) => {
    if (!url) return url;
    let formatUrl;
    if (url.includes(".co.uk")) {
      formatUrl = url.split(".co.uk")[1];
      return formatUrl === "/home/" ? "/" : formatUrl;
    }
    formatUrl = url.split(".com")[1];
    return formatUrl === "/home/" ? "/" : formatUrl;
  };

  const toggleMenu = () => {
    const openAccordions = document.querySelectorAll(
      "#mobile-menu-dropdown .footer__section-content--show"
    );
    if (openAccordions) {
      openAccordions.forEach((item) => {
        const accordionButton = item.parentNode.querySelector(
          ".footer__section-header"
        );
        if (accordionButton) {
          accordionButton.click();
        }
      });
    }
    setShowMenu(!showMenu);
  };

  const openSearchField = () => {
    setOpenSearch(true);
  };
  const closeSearchField = () => {
    setOpenSearch(false);
  };

  const showSubMenu = (e) => {
    e.target.nextSibling.style.marginLeft = "0%";
  };

  const hideSubMenu = (e) => {
    e.target.closest(".sub-menu").style.marginLeft = "-100%";
  };

  const handleHover = (e) => {
    let item = e.target;
    if (e.target.tagName === "A") {
      item = e.target.parentNode;
    }
    if (item.tagName !== "LI") return;
    item.classList.remove("inactive");
    item.classList.add("active");
    const inactiveItems = document.querySelectorAll(
      ".menu-section li:not(.active)"
    );
    inactiveItems.forEach((inactive) => {
      inactive.classList.add("inactive");
    });
  };
  const handleMouseLeave = (e) => {
    let item = e.target;
    if (e.target.tagName === "A") {
      item = e.target.parentNode;
    }
    if (item.tagName !== "LI") return;

    item.classList.remove("inactive");
    item.classList.remove("active");
    const inactiveItems = document.querySelectorAll(".menu-section .inactive");
    inactiveItems.forEach((inactive) => {
      inactive.classList.remove("inactive");
      inactive.classList.remove("active");
    });
  };

  const SubMenu = (props) => {
    return (
      <div className="sub-menu">
        <div className="sub-menu__back-link" onClick={hideSubMenu}>
          <a role="button" className="link link--arrow-left">
            <img className="arrow" src={blackArrowIcon} />{" "}
            {parse(props.data.label)}
          </a>
        </div>
        <div className="sub-menu__links">
          <ul className="dropdown-main-menu">
            {props.data.sub.map((item, index) => (
              <li key={`${item.label}-${index}`}>
                <a href={getSlug(item.url)}>{parse(item.label)}</a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  };

  return (
    <div id="main-header" className={openSearch ? "search-active" : ""}>
      <div className="container" id="header-search">
        <GlobalSearch
          products={products}
          restaurants={restaurants}
          stores={stores}
          events={events}
          amenities={amenities}
          campaigns={campaigns}
          globalContent={globalContent}
        />
        <button>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="28"
            height="28"
            fill="none"
            stroke="#000"
            style={{ cursor: "pointer" }}
            onClick={closeSearchField}
          >
            <path d="M.353.353l27.405 27.405" />
            <path d="M27.758.353L.353 27.758" />
          </svg>
        </button>
      </div>

      <div className="container header-menu-mobile">
        <div className="menu" onClick={toggleMenu}>
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
            <path d="M0 12h18v-2H0zm0-5h18V5H0zm0-7v2h18V0z" />
          </svg>
        </div>

        <div className="logo">
          <Link href="/">
            <a href="/">{parse(logo.svg)}</a>
          </Link>
        </div>

        <div className="search" onClick={openSearchField}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16">
            <path d="M11.435 10.063h-.723l-.256-.247c2.085-2.43 1.877-6.07-.47-8.247S4-.537 1.727 1.727-.606 7.637 1.57 9.985s5.818 2.556 8.247.47l.247.256v.723L14.637 16 16 14.637zm-5.49 0a4.12 4.12 0 0 1-4.117-4.117 4.12 4.12 0 0 1 4.117-4.117 4.12 4.12 0 0 1 4.117 4.117 4.11 4.11 0 0 1-4.117 4.117z" />
          </svg>
        </div>
        {windowSize < 960 && (
          <div
            id="mobile-menu-dropdown"
            className={additionalClass + (showMenu ? "show" : "hide")}
            onClick={(e) => {
              if (e.target === e.currentTarget) toggleMenu();
            }}
          >
            <div className="dropdown-close" onClick={toggleMenu}>
              <a role="button">
                <img src={closeIcon} alt="" />
              </a>
            </div>
            <div
              className={"dropdown-container " + (showMenu ? "show" : "hide")}
            >
              <nav className="container dropdown-elements">
                <ul className="dropdown-main-menu">
                  {navigation_primary.map((primeNav, index) => {
                    return (
                      <>
                        {primeNav.sub == false && (
                          <li key={`primenav${index}`}>
                            <Link href={getSlug(primeNav.url)}>
                              <a
                                href={getSlug(primeNav.url)}
                                className={
                                  router.asPath === getSlug(primeNav.url)
                                    ? "on-page"
                                    : ""
                                }
                              >
                                {parse(primeNav.label)}
                              </a>
                            </Link>
                          </li>
                        )}
                        {primeNav.sub && (
                          <>
                            <li key={`primenav${index}`}>
                              <a role="button" onClick={showSubMenu}>
                                {parse(primeNav.label)}
                              </a>
                              <SubMenu data={primeNav} />
                            </li>
                          </>
                        )}
                      </>
                    );
                  })}

                  {navigation_secondary.map((secondaryNav, index) => {
                    const url = secondaryNav.url;
                    return (
                      <>
                        {secondaryNav.sub == false && (
                          <li key={`secondaryNav${index}`}>
                            <Link href={getSlug(url)}>
                              <a
                                href={getSlug(url)}
                                className={
                                  router.asPath === getSlug(url)
                                    ? "on-page"
                                    : ""
                                }
                              >
                                {parse(secondaryNav.label)}
                              </a>
                            </Link>
                          </li>
                        )}
                        {secondaryNav.sub && (
                          <>
                            <li key={`secondaryNav${index}`}>
                              <a role="button" onClick={showSubMenu}>
                                {parse(secondaryNav.label)}
                              </a>
                              <SubMenu data={secondaryNav} />
                            </li>
                          </>
                        )}
                      </>
                    );
                  })}
                </ul>

                <ul className="dropdown-main-secondary ">
                  <Footer data={props.footerData} />
                </ul>
              </nav>
            </div>
          </div>
        )}
      </div>
      {windowSize > 960 && (
        <div className="container header-menu-main">
          <ul className="menu-section">
            {navigation_primary.map((primaryNav, index) => {
              return (
                <li
                  key={`primaryNav${index}`}
                  onMouseEnter={handleHover}
                  onMouseLeave={handleMouseLeave}
                >
                  <Link href={getSlug(primaryNav.url)}>
                    <a
                      className={
                        router.asPath === getSlug(primaryNav.url)
                          ? "on-page"
                          : ""
                      }
                    >
                      {primaryNav.label}
                    </a>
                  </Link>
                </li>
              );
            })}
          </ul>

          <div className="menu-section logo">
            <Link href="/">
              <a title="Bentall Centre Home">{parse(logo.svg)}</a>
            </Link>
          </div>

          <ul className="menu-section">
            {navigation_secondary.map((secondNav, index) => (
              <li
                key={`secondNav${index}`}
                onMouseEnter={handleHover}
                onMouseLeave={handleMouseLeave}
              >
                <Link href={getSlug(secondNav.url)}>
                  <a
                    className={
                      router.asPath === getSlug(secondNav.url) ? "on-page" : ""
                    }
                  >
                    {secondNav.label}
                  </a>
                </Link>
              </li>
            ))}
            <li
              className="search-toggle"
              onClick={openSearchField}
              onMouseEnter={handleHover}
              onMouseLeave={handleMouseLeave}
            >
              <a role="button">
                Search
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12">
                  <path d="M8.576 7.547h-.542l-.192-.185C9.405 5.54 9.25 2.81 7.49 1.177s-4.496-1.58-6.194.118-1.75 4.433-.118 6.194 4.363 1.917 6.185.353l.185.192v.542L10.978 12 12 10.978zm-4.117 0a3.09 3.09 0 0 1-3.086-3.088A3.09 3.09 0 0 1 4.46 1.373 3.09 3.09 0 0 1 7.547 4.46c.001.82-.324 1.605-.903 2.184s-1.365.904-2.184.903z" />
                </svg>
              </a>
            </li>
          </ul>
        </div>
      )}
    </div>
  );
};

export default MainHeader;
