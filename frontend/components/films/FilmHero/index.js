import React from "react";
import Image from "next/image";
import Vimeo from "@u-wave/react-vimeo";

const FilmHero = ({ title, links, image, video }) => {
  const buttonClasses = video ? "video" : "image";

  return (
    <div className="film-hero">
      <MediaBackground video={video} image={image} />

      <div className="film-hero__container">
        {/* <h1 className="title">{title}</h1> */}
        <div className={`button-container ${buttonClasses}`}>
          {links.map(({ label, url, isExternal }) => {
            return (
              <a key={label} target={isExternal ? "_blank" : ""} href={url}>
                {label}
              </a>
            );
          })}
        </div>
      </div>
    </div>
  );
};

const MediaBackground = ({ video, image }) => {
  if (video) {
    return (
      <div className="media-container">
        <Vimeo video={video} autoplay controls={false} loop muted />
      </div>
    );
  }

  if (image) {
    return (
      <div className="image-container">
        <Image
          src={image}
          layout="responsive"
          width="5833"
          height="2313"
          quality={100}
        />
      </div>
    );
  }

  return null;
};

export default FilmHero;
