import React, { useState, useEffect } from "react";
import Link from "next/link";

const ensureSquareOfFour = (array) => {
  const remainder = array.length % 4;
  if (remainder !== 0) {
    const filler = 4 - remainder;
    for (let i = 0; i < filler; i += 1) {
      array.push(null);
    }
  }
  return array;
};

const FilmTab = (props) => {
  const { searchResults, windowSize, films } = props;
  const [filmResults, setFilmResults] = useState([]);
  const [initialRenderSize, setInitialRenderSize] = useState(
    films.length < 8 ? films.length : 8
  );

  const showMore = () => {
    // show more films
    setInitialRenderSize(initialRenderSize + 4);
  };

  useEffect(() => {
    if (searchResults !== null && searchResults.length > 0)
      setFilmResults(searchResults);

    if (searchResults !== null && searchResults.length === 0) {
      setFilmResults(films);
    }
  }, [searchResults]);

  const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;
  let emptyGridItems = [];
  let emptyGridAmounts = 0;
  if (windowSize >= 700 && windowSize < 941) {
    if (films.slice(0, initialRenderSize).length % 2 !== 0) {
      emptyGridAmounts =
        roundUp(films.slice(0, initialRenderSize).length, 2) -
        films.slice(0, initialRenderSize).length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyGridItems.push('<div class="teaser-content--forth"></div>');
      }
    }
  } else if (windowSize >= 941) {
    if (films.slice(0, initialRenderSize).length % 3 !== 0) {
      emptyGridAmounts =
        roundUp(films.slice(0, initialRenderSize).length, 3) -
        films.slice(0, initialRenderSize).length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyGridItems.push('<div class="teaser-content--forth"></div>');
      }
    }
  }

  // const {synopsis_long} = getFilmDetails(filmId)
  // const { synopsis_long } = filmDetails;

  const FilmsMarkup = () => {
    return ensureSquareOfFour(films.slice(0, initialRenderSize)).map((film) => {
      // console.log("what is it?", film);
      if (!film) return <div className="empty-grid-item"></div>;

      const imgSrc = film?.images?.still
        ? film?.images?.still["1"]?.medium?.film_image
        : film?.images?.poster["1"]?.medium?.film_image;

      const orientation = film?.images?.still ? "landscape" : "portrait";

      console.log(film?.images);

      return (
        <div className="films-grid__item">
          <Link href={`/cinema/${film.film_id}`}>
            <a>
              <div className={`teaser__img ${orientation}`}>
                <img src={imgSrc} style={{ objectFit: "cover" }} />
                {/* <Image src={imgSrc} layout="fill" objectFit="cover" /> */}
              </div>
              <div className="teaser-content__inside">
                <h2 className="teaser__subject">{film.film_name}</h2>

                <div
                  className="teaser__tag-container"
                  style={{ display: "flex", flexWrap: "wrap" }}
                >
                  {film.showings.Standard?.times?.length &&
                    film.showings.Standard.times
                      .slice(0, 6)
                      .map(({ start_time }) => {
                        return (
                          <span className="teaser__tag">{start_time}</span>
                        );
                      })}
                </div>
                {film.showings.Standard?.times?.length > 6 && (
                  <>See more times</>
                )}
              </div>
            </a>
          </Link>
        </div>
      );
    });
  };
  return (
    <>
      <div className="container teaser-container">
        <div className="_teaser-content films-grid">
          {FilmsMarkup()}
          {/* {emptyGridItems.map((item) => parse(item))} */}
        </div>
      </div>
      {films !== null && (
        <div className="pagination">
          <div className="pagination__amount">
            <span>
              Showing {films.slice(0, initialRenderSize).length} out of{" "}
              {films.length} films
            </span>
          </div>

          <button
            className={
              "pagination__view-more" +
              (films.slice(0, initialRenderSize).length === films.length
                ? " pagination__view-more--disabled"
                : "")
            }
            onClick={showMore}
          >
            See More showings
          </button>
        </div>
      )}
    </>
  );
};

export default FilmTab;
