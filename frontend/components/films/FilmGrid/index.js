import React, { useState, useEffect } from "react";

import { Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import FilmTab from "../FilmTab";

import moment from "moment";
import Link from "next/link";
import { useRouter } from "next/router";

const FilmGrid = (props) => {
  const [windowSize, setWindowSize] = useState(961);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [searchResults, setSearchResults] = useState(null);
  const [showDropDown, setShowDropDown] = useState(false);
  const router = useRouter();

  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const weekdays = [];

  for (let i = 0; i < 7; i++) {
    weekdays.push(moment().add(i, "days"));
  }

  const weekdayTabs = weekdays.map((tab, index) => {
    const tabNames = weekdays.map((day) => day.format("dddd, Do"));

    const formattedDate = `${tabNames[index].split(",")[0]} ${
      tabNames[index].split(",")[1]
    }`;

    return {
      name: index == 0 ? "Today" : index == 1 ? "Tomorrow" : formattedDate,
      value: moment(tab).format("YYYY-MM-DD"),
    };
  });

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  };

  const resetSearchResults = () => {
    setSearchResults([]);
  };
  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 300);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      slider.removeEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.removeEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
      slider.removeEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.removeEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
    };
  }, []);

  const handleSearchChange = (items) => {
    setSearchResults(items);
  };

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  return (
    <section className="tabbed-content">
      <div className="film-grid__header">
        <ul className="films-grid-tab__panel">
          {weekdayTabs.map((tab) => {
            const isMatchingDate =
              router.query.date == tab.value ||
              (router.query.date == undefined &&
                tab.value == moment().format("YYYY-MM-DD"));

            return (
              <React.Fragment key={tab.value}>
                <li
                  style={{
                    backgroundColor: isMatchingDate && "black",
                  }}
                >
                  <Link href={`/cinema/date/${tab.value}`}>
                    <a
                      style={{
                        color: isMatchingDate && "white",
                      }}
                    >
                      {tab.name}
                      {/* {tab.value} */}
                    </a>
                  </Link>
                </li>
              </React.Fragment>
            );
          })}
        </ul>

        <a
          className="see-more"
          href="https://www.curzon.com/venues/kingston"
          target="_blank"
        >
          <span className="text">See More on Curzon Site</span>
          <span className="icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29.789 8">
              <g data-name="Group 461">
                <path
                  data-name="Line 12"
                  fill="none"
                  stroke="#000"
                  d="M0 4.229h19.733"
                />
                <path data-name="Polygon 1" d="m29.789 4-11 4V0Z" />
              </g>
            </svg>
          </span>
        </a>
      </div>

      <Tabs
        selectedIndex={selectedIndex}
        onSelect={(index, lastIndex) => {
          if (index === lastIndex) {
            setSelectedIndex(0);
          } else {
            setSelectedIndex(index);
          }
        }}
      >
        {windowSize < 777 && (
          <div className="tabbed-content__dropdown">
            <></>

            <TabList
              className={
                "teaser-container__top teaser-container__top--tabs " +
                (showDropDown ? showDropDownClass : hideDropDownClass)
              }
            ></TabList>
          </div>
        )}

        <TabPanel style={{ margin: "0" }}>
          <div className="tabbed-content__teasers tabbed-content__teasers--films">
            {/* Tab corresponds to today's date here */}
            <FilmTab
              films={props.films}
              searchResults={searchResults}
              windowSize={windowSize}
            />
          </div>
        </TabPanel>
      </Tabs>
    </section>
  );
};

export default FilmGrid;
