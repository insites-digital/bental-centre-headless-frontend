import React from "react";
import errorIcon from "../static/images/site/error.svg";
import blackArrow from "../static/images/general/arrow-black.svg";
import Link from "next/link";

const Error = props => {
  return (
    <section className="error container">
      <div className="error__container">
        <img src={errorIcon} />
        <h1>The page you are looking for could not be found.</h1>
        <p>
          <Link as={`/`} href={`/`}>
            <a className="link link--uppercase link--arrow">
              Back to homepage <img src={blackArrow} />
            </a>
          </Link>
        </p>
      </div>
    </section>
  );
};

export default Error;
