import React, { useEffect, useState } from "react";
import parse from "html-react-parser";
import { getSlug } from "../utils/getSlug";
import Image from "./Image";
import ImageHoverItem from "./ImageHoverItem";

const ProductGrid = (props) => {
  const [windowSize, setWindowSize] = useState(0);

  // console.log("PRODUCTS ::::: ", props.products);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  if (
    !props.hasOwnProperty("data") ||
    !props.data.hasOwnProperty("products") ||
    !props.data.products.length
  )
    return <></>;

  const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;
  let emptyProductGrids = [];
  let emptyGridAmounts;
  if (windowSize >= 1920) {
    if (props.data.products.length % 4 !== 0) {
      emptyGridAmounts =
        roundUp(props.data.products.length, 4) - props.data.products.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyProductGrids.push(
          '<div class="teaser-content teaser-content--third"></div>'
        );
      }
    }
  } else if (windowSize >= 700) {
    if (props.data.products.length % 3 !== 0) {
      emptyGridAmounts =
        roundUp(props.data.products.length, 3) - props.data.products.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyProductGrids.push(
          '<div class="teaser-content teaser-content--third"></div>'
        );
      }
    }
  } else {
    if (props.data.products.length % 2 !== 0) {
      emptyGridAmounts =
        roundUp(props.data.products.length, 2) - props.data.products.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyProductGrids.push(
          '<div class="teaser-content teaser-content--third"></div>'
        );
      }
    }
  }

  return (
    <section className="product-grid">
      <ul
        className="teaser-container__top container teaser-container__top--tabs teaser-container__top--tab-hide"
        role="tablist"
      >
        <li className="react-tabs__tab react-tabs__tab--title teaser__title">
          {props.data && props.data.name && <h2>{parse(props.data.name)}</h2>}
        </li>
      </ul>
      <div className="product-grid__products teaser-content__container container">
        {props.data.products &&
          props.data.products.map((product) => {
            const productData = props.products.filter((item) => {
              return item.id === product.ID;
            });

            if (productData.length < 1) return <></>;
            const store = props.stores.filter((store) => {
              return store.id === productData[0].acf.store[0];
            });
            return (
              <div
                className="teaser-content teaser-content--third"
                key={product.ID}
              >
                <a href={getSlug(productData[0].link)}>
                  <ImageHoverItem
                    data={productData[0]}
                    size={"50vw"}
                    className=""
                  />
                  <div className="teaser-content__inside teaser-content__inside--featured-products">
                    {store.length > 0 && (
                      <h2 className="teaser__subject teaser__subject--no-underline">
                        {parse(store[0].title.rendered)}
                      </h2>
                    )}
                    {productData[0].title &&
                      productData[0].title.rendered !== "" && (
                        <span className="teaser__subject teaser__subject--no-underline teaser__subject--not-bold">
                          {parse(productData[0].title.rendered)}
                        </span>
                      )}
                    {productData[0].acf &&
                      productData[0].acf.price &&
                      productData[0].acf.price !== "" && (
                        <span className="teaser__subject teaser__subject--no-underline teaser__subject--not-bold">
                          £{parse(productData[0].acf.price)}
                        </span>
                      )}
                  </div>
                </a>
              </div>
            );
          })}

        {emptyProductGrids &&
          emptyProductGrids.length > 0 &&
          emptyProductGrids.map((emptyProduct) => {
            return parse(emptyProduct);
          })}
      </div>
    </section>
  );
};

export default ProductGrid;
