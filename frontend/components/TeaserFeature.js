import React, { useState, useEffect } from "react";
import { getSlug } from "../utils/getSlug";
import Link from "next/link";
import moment from "moment";
import "moment-timezone";
import blackArrow from "../static/images/general/arrow-black.svg";

const TeaserFeature = (props) => {
  const [dealTimer, setDealTimer] = useState(null);
  const [cta, setCta] = useState(null);
  const [deal_ends, setDealEnds] = useState(null);
  const [deal_banner, setDealBanner] = useState(null);
  const [strapline, setStrapline] = useState(null);
  const [showDealOfDay, setShowDealOfDay] = useState(null);
  const [product, setProduct] = useState(null);
  const [dealEnds, setDealEndsMessage] = useState("");

  const setTimer = () => {
    if (!deal_ends) return;
    setDealEndsMessage("Ends in ");
    const now = moment();
    const expiration = moment(deal_ends);
    const diff = expiration.diff(now);
    const diffDuration = moment.duration(diff);
    const days = diffDuration.days();
    const hours = diffDuration.hours();
    const minutes = diffDuration.minutes();
    const seconds = diffDuration.seconds();
    let dealEndsStr = "";
    if (days > 0) {
      dealEndsStr += days === 1 ? "1 day " : days + " days ";
    }
    if (hours >= 1) {
      dealEndsStr += hours > 1 ? hours + "hours " : "1hour ";
    }
    if (minutes >= 1) {
      dealEndsStr += minutes + "min ";
    }
    dealEndsStr += seconds + "sec";

    setDealEndsMessage(dealEnds + dealEndsStr);

    if (days <= 0 && hours <= 0 && minutes <= 0 && seconds < 1) {
      setDealTimer("Deal ended");
      return;
    }
    setDealTimer(dealEnds + dealEndsStr);
  };

  useEffect(() => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0");
    var yyyy = today.getFullYear();
    today = mm + "/" + dd + "/" + yyyy;
    props.data.forEach((teaser) => {
      moment.tz.setDefault("Europe/London");
      const splitDate = teaser.display_date.split("/");
      const day = splitDate[0];
      const month = splitDate[1];
      const year = splitDate[2];

      if (!month || !day || !year) return;
      let difference = moment(`${month}/${day}/${year}`).diff(today, "days");

      if (difference === 0) {
        setShowDealOfDay(true);
        setProduct(
          props.products.filter((item) => item.id === teaser.product[0])
        );
        setCta(teaser.cta);
        setStrapline(teaser.strapline);
        setDealEnds(teaser.deal_ends);
        setDealBanner(teaser.deal_banner);
      }
    });
  }, []);

  useEffect(() => {
    let startTimer;
    if (deal_ends !== null && !dealTimer)
      startTimer = setInterval(() => {
        setTimer();
      }, 1000);
    return () => {
      clearInterval(startTimer);
    };
  }, [deal_ends]);

  {
    showDealOfDay === null && <></>;
  }

  return (
    <>
      {showDealOfDay && dealTimer && (
        <section className="container">
          <div className="teaser-container teaser-container--dotd">
            <div className="teaser-container__top">
              <div className="teaser__title">
                <h2>Deal of the Day</h2>
              </div>

              <div className="teaser__view-more">
                <Link as={`/deal-of-the-day/`} href={`/deal-of-the-day/`}>
                  <a className="link link--uppercase link--arrow">
                    Deal of the Day
                    <img className="arrow" src={blackArrow} alt="" />
                  </a>
                </Link>
              </div>
            </div>
            <Link as={`/deal-of-the-day/`} href={`/deal-of-the-day/`}>
              <div className="teaser-content" style={{ cursor: "pointer" }}>
                <div className="teaser-content--full">
                  <div className="teaser__img teaser__img--dotd">
                    <img src={deal_banner} alt="" />
                  </div>

                  <div className="teaser-content__inside">
                    <div className="flex flex--space-between">
                      <div className="teaser__tag-container">
                        <span className="teaser__tag teaser__tag--deal">
                          {dealTimer}
                        </span>
                      </div>

                      <div className="teaser__span-container">
                        <span className="teaser__span">{strapline}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </section>
      )}
    </>
  );
};

export default TeaserFeature;
