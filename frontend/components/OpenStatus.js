import React from "react";
import moment from "moment";
import "moment-timezone";
import { isBST } from "../utils/isBST";

const OpenStatus = (props) => {
  const { openingHours } = props;

  const britishSummerTime = isBST(new Date());

  if (!openingHours || openingHours.length < 1) return <></>;
  let open = false;
  moment.tz.setDefault("Europe/London");
  const now = moment();
  let dayNumber = now.day();
  let day;

  switch (parseInt(dayNumber)) {
    case 0:
      day = "sunday";
      break;
    case 1:
      day = "monday";
      break;
    case 2:
      day = "tuesday";
      break;
    case 3:
      day = "wednesday";
      break;
    case 4:
      day = "thursday";
      break;
    case 5:
      day = "friday";
      break;
    case 6:
      day = "saturday";
      break;
    default:
      return;
  }

  let isOpenToday = openingHours.filter((time) => {
    return time.day === day;
  });

  let message = "";
  if (isOpenToday && isOpenToday.length > 0) {
    let currentTime = moment(now.format("HH:mm:ss")),
      openingTime = moment(new Date("1970-01-01T" + isOpenToday[0].start_time)),
      closingTime = moment(new Date("1970-01-01T" + isOpenToday[0].end_time));

    currentTime = moment(new Date("1970-01-01T" + currentTime._i));

    if (
      isOpenToday[0] &&
      isOpenToday[0].start_time &&
      currentTime.isBefore(openingTime)
    ) {
      // store has not yet opened
      message = `Opens today at ${moment(
        new Date("1970-01-01T" + isOpenToday[0].start_time).setTime(
          new Date("1970-01-01T" + isOpenToday[0].start_time + "Z") -
            new Date().getTimezoneOffset() * 60 * 1000
        )
      )
        .subtract(britishSummerTime ? 2 : 1, "hour")
        .format("h:mma")
        .replace(":00", "")}`;
    } else if (
      isOpenToday[0] &&
      isOpenToday[0].end_time &&
      currentTime.isBefore(closingTime)
    ) {
      // store has opened and has not yet closed so say open untill ...

      // if (isOpenToday[0].end_time === "23:59:00")
      const endTime =
        isOpenToday[0].end_time === "23:59:00"
          ? "00:00:00"
          : isOpenToday[0].end_time;

      open = true;
      message = `Open until ${moment(
        new Date("1970-01-01T" + endTime).setTime(
          new Date("1970-01-01T" + endTime + "Z") -
            new Date().getTimezoneOffset() * 60 * 1000
        )
      )
        .subtract(britishSummerTime ? 2 : 1, "hour")
        .format("h:mma")
        .replace(":00", "")}`;
    } else {
      // store has closed
      message = `Closed`;
    }
  } else {
    message = "Closed today";
  }
  return (
    <div className="teaser__span-container">
      <span
        className={
          "teaser__span " +
          (open ? "teaser__span--status-open" : "teaser__span--status-closed")
        }
      >
        {message}
      </span>
    </div>
  );
};

export default OpenStatus;
