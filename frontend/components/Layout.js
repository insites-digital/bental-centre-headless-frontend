import React, { Component } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Menu from "./Menu";
import TabbedServicesFacilitiesComponent from "./tabbedContent/TabbedServicesFacilitiesComponent";
// import NewsLetterPopup from "./NewsLetterPopup";
// import CovidPopup from "./CovidPopup";

import PropTypes from "prop-types";

class Layout extends Component {
  state = {
    id: "",
    headerHeight: 83,
    timeoutID: null,
  };

  componentDidMount() {
    const mainHeader = document.querySelector("header");
    if (!mainHeader) return;

    this.setState({
      timeoutID: window.setTimeout(() => {
        this.setState({ headerHeight: mainHeader.clientHeight });
      }, 600),
    });

    window.addEventListener("resize", () =>
      this.setState({ headerHeight: mainHeader.clientHeight })
    );
  }

  componentWillUnmount() {
    window.removeEventListener("resize", () =>
      this.setState({ headerHeight: mainHeader.clientHeight })
    );
    window.clearTimeout(this.state.timeoutID);
  }

  render() {
    const { children, pageClass, data } = this.props;

    return (
      <>
        {/* Google Tag Manager (noscript) */}
        <noscript
          dangerouslySetInnerHTML={{
            __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JF7BXW" height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
          }}
        />
        {/* End Google Tag Manager (noscript) */}
        <div
          className={"page-wrapper " + (pageClass ? pageClass : "")}
          style={{ marginTop: this.state.headerHeight + "px" }}
        >
          <Header data={this.props.metaData} />
          <Menu
            data={data}
            pageClass={pageClass}
            products={this.props.products}
            restaurants={this.props.restaurants}
            stores={this.props.stores}
            events={this.props.events}
            amenities={this.props.amenities}
            campaigns={this.props.campaigns}
            globalContent={data}
          />
          <main>
            {/* <CovidPopup /> */}
            {children}
          </main>
          {this.props.showServices && (
            <div>
              <TabbedServicesFacilitiesComponent
                amenities={this.props.amenities}
                amenityCats={this.props.amenityCats}
              />
            </div>
          )}

          <Footer data={data} />
        </div>
      </>
    );
  }
}

Layout.defaultProps = {
  showServices: true,
};

export default Layout;
