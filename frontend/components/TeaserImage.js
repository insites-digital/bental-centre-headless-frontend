import React, { useState, useEffect } from "react";
import { SimpleImg } from "react-simple-img";

const TeaserImage = (props) => {
  const [imageSrcset, setImageSrcset] = useState("");
  const [imageState, setImageState] = useState(<></>);

  if (!props.data.url.sizes) return <></>;

  const setImagesSizes = () => {
    if (props.imageSize === "100vw") {
      setImageSrcset(
        props.data.url.sizes.medium +
          " 640w, " +
          props.data.url.sizes.medium_large +
          " 768w, " +
          props.data.url.sizes.large +
          " 1024w, " +
          props.data.url.sizes.large_large +
          " 1366w, " +
          props.data.url.sizes.xlarge +
          " 1600w, " +
          props.data.url.sizes.xlarge_large +
          " 1920w "
      );
    } else if (props.imageSize === "33vw") {
      setImageSrcset(
        props.data.url.sizes.xsmall_33vw +
          " 640w, " +
          props.data.url.sizes.xsmall_33vw +
          " 768w, " +
          props.data.url.sizes.small_33vw +
          " 1024w, " +
          props.data.url.sizes.medium_33vw +
          " 1366w, " +
          props.data.url.sizes.large_33vw +
          " 1600w, " +
          props.data.url.sizes.xlarge_33vw +
          " 1920w "
      );
    } else if (props.imageSize === "50vw") {
      setImageSrcset(
        props.data.url.sizes.xsmall_50vw +
          " 640w, " +
          props.data.url.sizes.xsmall_50vw +
          " 768w, " +
          props.data.url.sizes.small_50vw +
          " 1024w, " +
          props.data.url.sizes.medium_50vw +
          " 1366w, " +
          props.data.url.sizes.large_50vw +
          " 1600w, " +
          props.data.url.sizes.large_large +
          " 1920w "
      );
    }
  };

  useEffect(() => {
    setImagesSizes();
  }, []);

  useEffect(() => {
    const positionStyle = props.imageSize === "100vw" ? "initial" : "absolute";
    setImageState(
      <SimpleImg
        style={{ position: positionStyle, top: "0", left: "0", right: "0" }}
        height={"100%"}
        placeholder={"rgb(211,211,211)"}
        src={props.data.url.sizes.medium}
        srcSet={imageSrcset}
        alt={props.data.heading}
      />
    );
  }, [imageSrcset]);

  useEffect(() => {
    setImageState(<></>);
    setImagesSizes();
  }, [props.data.url]);

  return <>{imageState}</>;
};

export default TeaserImage;
