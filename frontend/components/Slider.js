import React from "react";
import Swiper from "react-id-swiper";

const Slider = (props) => {
  const { children } = props;
  const params = {
    slidesPerView: "auto",
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };
  return <Swiper {...params}>{{ children }}</Swiper>;
};
export default Slider;
