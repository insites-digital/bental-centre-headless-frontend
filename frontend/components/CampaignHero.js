import React from "react";
import Image from "./Image";

const CampaignHero = ({ image }) => {
  return (
    <div className="campaign-feature campaign-feature--full-width">
      <div className="campaign-feature__full-link">
        <div className="campaign-feature__img">
          <Image data={image} imageSize="100vw" />
        </div>
      </div>
    </div>
  );
};

export default CampaignHero;
