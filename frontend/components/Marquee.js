import React, { useState, useEffect } from "react";
import Ticker from "react-ticker";

const Marquee = (props) => {
  const { data } = props;

  const mobileStyles = {
    fontSize: "24px",
    lineHeight: "25px",
    marginRight: "5.20835vw",
    whiteSpace: "nowrap",
    textTransform: "uppercase",
    padding: "3.5px 0",
  };

  const desktopStyles = {
    fontSize: "48px",
    lineHeight: "50px",
    marginRight: "5.20835vw",
    whiteSpace: "nowrap",
    textTransform: "uppercase",
    padding: "7px 0",
  };

  const [styles, setStyles] = useState(desktopStyles);

  useEffect(() => {
    window.innerWidth < 800
      ? setStyles(mobileStyles)
      : setStyles(desktopStyles);

    window.addEventListener("resize", () =>
      window.innerWidth < 800
        ? setStyles(mobileStyles)
        : setStyles(desktopStyles)
    );

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  if (data === "") return <></>;

  return (
    <>
      <div style={{ borderBottom: "solid black 1px" }}>
        <Ticker offset="run-in" speed={10}>
          {() => (
            <div className="marquee_item" style={styles}>
              {data}
            </div>
          )}
        </Ticker>
      </div>
    </>
  );
};

export default Marquee;
