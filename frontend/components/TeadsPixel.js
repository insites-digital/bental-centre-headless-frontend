import React from "react";

const TeadsPixel = () => (
  <>
    <script
      type="text/javascript"
      src="https://p.teads.tv/teads-fellow.js"
      async="true"
    />
    <script
      dangerouslySetInnerHTML={{
        __html: `window.teads_e = window.teads_e || [];
       window.teads_buyer_pixel_id = 3945;`,
      }}
    />
  </>
);

export default TeadsPixel;
