import React from "react";
import parse from "html-react-parser";

const HeroText = (props) => {
  return (
    <div className="container hero-text">
      {props.data.breadcrumbs && (
        <nav>
          <ul>
            {props.data.breadcrumbs.map((crumb, index) => {
              if (!crumb && !crumb.name) return;
              if (props.data.breadcrumbs.length - 1 === index) {
                return (
                  <li style={{ textTransform: "uppercase" }} key={crumb.name}>
                    {crumb.name}
                  </li>
                );
              }
              return (
                <li key={crumb.name}>
                  <a
                    href={crumb.link}
                    className={crumb.link === "" ? "disabled-link" : ""}
                  >
                    {crumb.name}
                  </a>
                </li>
              );
            })}
          </ul>
        </nav>
      )}

      {props.data.description && (
        <div className="hero-text__content hero-text__content--full">
          {props.data.subHeading && <h2>{parse(props.data.subHeading)}</h2>}
          {props.data.heading && <h1>{parse(props.data.heading)}</h1>}
          {props.data.description && parse(props.data.description)}
          {props.data.button && (
            <a className="hero-text__button" href={button.link}>
              {button.text}
            </a>
          )}
        </div>
      )}
    </div>
  );
};

export default HeroText;
