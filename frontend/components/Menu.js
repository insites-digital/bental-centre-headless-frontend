import React from "react";
import SubHeader from "./SubHeader";
import MainHeader from "./MainHeader";

const Menu = (props) => {
  if (
    !props ||
    !props.hasOwnProperty("data") ||
    !props.data ||
    !props.data.hasOwnProperty("header")
  )
    return <></>;
  const {
    navigation_primary,
    navigation_secondary,
    ticker,
    logo,
  } = props.data.header;

  return (
    <header>
      {ticker !== false && <SubHeader data={ticker} />}

      <MainHeader
        data={{
          logo,
          navigation_primary,
          navigation_secondary,
          footer: props.data.footer,
        }}
        footerData={props.data}
        layout={props.pageClass}
        products={props.products}
        restaurants={props.restaurants}
        stores={props.stores}
        events={props.events}
        amenities={props.amenities}
        campaigns={props.campaigns}
        globalContent={props.globalContent}
      />
    </header>
  );
};

export default Menu;
