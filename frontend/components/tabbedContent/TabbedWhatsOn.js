import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import ContentSearch from "../ContentSearch";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "moment-timezone";
import moment from "moment";
import EventTab from "./EventTab";

const TabbedWhatsOn = (props) => {
  const splitDate = (str) => str.split("/");
  const [windowSize, setWindowSize] = useState(961);
  const [searchResults, setSearchResults] = useState(null);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [eventsData, setEvents] = useState(
    props.events.filter((elem) => {
      const formattedEndDate = new Date(
        `${
          splitDate(elem.acf.dates.end_date)[1] +
          "/" +
          splitDate(elem.acf.dates.end_date)[0] +
          "/" +
          splitDate(elem.acf.dates.end_date)[2]
        }`
      );
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0");
      var yyyy = today.getFullYear();

      today = mm + "/" + dd + "/" + yyyy;
      moment().tz("Europe/London");
      const dateHasPassed = moment(formattedEndDate).isBefore(today);
      return dateHasPassed === false;
    })
  );
  const [showDropDown, setShowDropDown] = useState(false);
  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const tabs = props.categories.filter((category) => {
    let match = false;
    eventsData.map((item) => {
      if (item["event-category"].includes(category.id)) match = true;
    });
    return match;
  });

  const handleSearchChange = (items) => {
    setSearchResults(items);
  };

  const resetSearchResults = () => {
    setSearchResults([]);
  };

  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 300);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      slider.removeEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.removeEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
      slider.removeEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.removeEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
    };
  }, []);

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  };

  return (
    <section className="tabbed-content container tabbed-content--whats-on">
      <Tabs
        selectedIndex={selectedIndex}
        onSelect={(index, lastIndex, event) => {
          if (index === lastIndex) {
            setSelectedIndex(0);
          } else {
            setSelectedIndex(index);
          }
        }}
      >
        <div className="tabbed-content__dropdown">
          {windowSize < 700 && (
            <>
              <ul className="teaser-container__top teaser-container__top--mobile">
                <li className="teaser__title">
                  <h2>{parse(props.title)}</h2>
                </li>
              </ul>
              <div className="tabbed-content__dropdown-button">
                <button onClick={toggleDropdown}>
                  Select Event Type{" "}
                  <div
                    className={
                      "tabbed-content__dropdown-button-icon" +
                      (showDropDown ? " rotate" : "")
                    }
                  >
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="11"
                        height="5"
                      >
                        <path d="M5.5 5L0 0h11z" data-name="Polygon 9"></path>
                      </svg>
                    </span>
                  </div>
                </button>
              </div>
            </>
          )}
          <TabList
            className={
              "teaser-container__top teaser-container__top--tabs " +
              (showDropDown ? showDropDownClass : hideDropDownClass)
            }
          >
            {windowSize > 700 && (
              <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                <h2>{parse(props.title)}</h2>
              </li>
            )}
            <Tab className="react-tabs__tab">
              <h3>All</h3>
            </Tab>
            {tabs.map((tab) => (
              <Tab key={tab.id}>
                <h3
                  onClick={(e) => {
                    closeDropDown();
                    resetSearchResults();
                  }}
                >
                  {tab && tab.name && parse(tab.name)}
                </h3>
              </Tab>
            ))}
          </TabList>
        </div>

        <TabPanel>
          <ContentSearch
            searchData={eventsData}
            handleSearchChange={handleSearchChange}
            placeholder={"Search for an event"}
          />
          <div className="tabbed-content__teasers">
            <div className="container teaser-container">
              <div className="teaser-content">
                <EventTab
                  data={{
                    events: eventsData,
                    categories: props.categories,
                    searchResults,
                    windowSize,
                  }}
                />
              </div>
            </div>
          </div>
        </TabPanel>

        {tabs.map((tab, index) => {
          let events = eventsData.filter((item) => {
            if (!item["event-category"]) return false;
            return item["event-category"].includes(tab.id);
          });

          return (
            <TabPanel>
              <ContentSearch
                searchData={events}
                handleSearchChange={handleSearchChange}
                placeholder={"Search for an event"}
              />
              <div className="tabbed-content__teasers">
                <div className="container teaser-container">
                  <div className="teaser-content">
                    <EventTab
                      data={{
                        events,
                        categories: props.categories,
                        searchResults,
                        windowSize,
                      }}
                    />
                  </div>
                </div>
              </div>
            </TabPanel>
          );
        })}
      </Tabs>
    </section>
  );
};

export default TabbedWhatsOn;
