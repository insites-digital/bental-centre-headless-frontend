import React, { useState, useEffect } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import ContentSearch from "../ContentSearch";
import parse from "html-react-parser";
import "react-tabs/style/react-tabs.css";
import StoreTab from "./StoreTab";
import { getUrlVars } from "../../utils/getUrlVars";

const TabbedStores = (props) => {
  const [windowSize, setWindowSize] = useState(961);
  const [searchResults, setSearchResults] = useState(null);
  const [tabIndex, setTabIndex] = useState(0);
  let tabs = props.storeCats.filter((category) => category.count > 0);
  const [showDropDown, setShowDropDown] = useState(false);
  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const handleSearchChange = (items) => {
    setSearchResults(items);
  };

  for (var x in tabs) {
    if (tabs[x] && tabs[x].name) {
      tabs[x].name == "Bentalls" ? tabs.push(tabs.splice(x, 1)[0]) : 0;
    }
  }

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  };

  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");

    const tabNames = tabs.map((tab) => {
      if (!tab || !tab.name) return;
      return tab.name.toLowerCase().split(" ").join("").replace(/&amp;/g, "&");
    });

    const urlVars = getUrlVars();
    if (urlVars.length > 0) {
      urlVars.forEach((urlVar) => {
        for (let i = 0; i <= tabNames.length - 1; i++) {
          if (
            urlVar.key.toLowerCase().split(" ").join("").replace("and", "&") ===
            tabNames[i]
          ) {
            setTabIndex(i + 1);
          }
        }
      });
    } else {
      setTabIndex(0);
    }
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 300);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      slider.removeEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.removeEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
      slider.removeEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.removeEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
    };
  }, []);

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  if (props.stores.length < 1 || props.storeCats.length < 1) return <></>;

  return (
    <section className="tabbed-content container tabbed-content--stores">
      {tabIndex !== null && (
        <Tabs
          selectedIndex={tabIndex}
          onSelect={(index, lastIndex, event) => {
            if (index === lastIndex) {
              setTabIndex(0);
            } else {
              setTabIndex(index);
            }
          }}
        >
          <div className="tabbed-content__dropdown">
            {windowSize < 700 && (
              <>
                <ul className="teaser-container__top teaser-container__top--mobile">
                  <li className="teaser__title">
                    <h2>Our Stores</h2>
                  </li>
                </ul>
                <div className="tabbed-content__dropdown-button">
                  <button onClick={toggleDropdown}>
                    Select Department{" "}
                    <div
                      className={
                        "tabbed-content__dropdown-button-icon" +
                        (showDropDown ? " rotate" : "")
                      }
                    >
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="11"
                          height="5"
                        >
                          <path d="M5.5 5L0 0h11z" data-name="Polygon 9"></path>
                        </svg>
                      </span>
                    </div>
                  </button>
                </div>
              </>
            )}
            <TabList
              className={
                "teaser-container__top teaser-container__top--tabs " +
                (showDropDown ? showDropDownClass : hideDropDownClass)
              }
            >
              {windowSize > 700 && (
                <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                  <h2>Our Stores</h2>
                </li>
              )}
              <Tab
                onClick={() => {
                  setSearchResults([]);
                }}
                className="react-tabs__tab"
              >
                <h3 onClick={closeDropDown}>All</h3>
              </Tab>
              {tabs &&
                tabs.map((item) => (
                  <Tab
                    key={`tab-${item.id}`}
                    onClick={() => {
                      setSearchResults([]);
                    }}
                    className={
                      item.id !== 19
                        ? "react-tabs__tab"
                        : "react-tabs__tab react-tabs__tab--highlight-green"
                    }
                  >
                    {item && item.name && (
                      <h3 onClick={closeDropDown}>{parse(item.name)}</h3>
                    )}
                  </Tab>
                ))}
            </TabList>
          </div>

          <TabPanel>
            <ContentSearch
              searchData={props.stores}
              handleSearchChange={handleSearchChange}
              placeholder={"Search for a store"}
            />
            <StoreTab
              stores={props.stores}
              department={{ id: -1 }}
              floors={props.floors}
              windowSize={windowSize}
              storeCats={props.storeCats}
              searchResults={searchResults}
            />
          </TabPanel>

          {tabs.map((department) => {
            let storesData = props.stores.filter((store) => {
              if (!store["store-category"]) return false;
              return store["store-category"].includes(department.id);
            });

            storesData = storesData.filter((store) => store.id !== 252);

            return (
              <TabPanel key={department.id}>
                <ContentSearch
                  searchData={storesData}
                  handleSearchChange={handleSearchChange}
                  placeholder={"Search for a store"}
                />
                <StoreTab
                  stores={props.stores}
                  department={department}
                  floors={props.floors}
                  windowSize={windowSize}
                  storeCats={props.storeCats}
                  searchResults={searchResults}
                />
              </TabPanel>
            );
          })}
        </Tabs>
      )}
    </section>
  );
};

export default TabbedStores;
