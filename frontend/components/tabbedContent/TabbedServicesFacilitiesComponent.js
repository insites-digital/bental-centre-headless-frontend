import React, { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import blackArrow from "../../static/images/general/arrow-black.svg";
import { getSlug } from "../../utils/getSlug";
import Link from "next/link";
import parse from "html-react-parser";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Truncate from "react-truncate";

const TabbedServicesFacilitiesComponent = (props) => {
  const { amenities, amenityCats } = props;

  const [windowSize, setWindowSize] = useState(961);
  const [showDropDown, setShowDropDown] = useState(false);
  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    let isDown = false;
    let startX;
    let scrollLeft;
    if (slider) {
      slider.addEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.addEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.addEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
    }
  };

  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 300);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      slider.removeEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.removeEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
      slider.removeEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.removeEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
    };
  }, []);

  if (!amenities || !amenityCats) return <></>;

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  let orderedCats = [...amenityCats];

  const bentalls = amenityCats.filter((item) => item.id === 10);
  orderedCats = orderedCats.filter((item) => item.id !== 10);
  orderedCats = [bentalls[0], ...orderedCats];

  return (
    <section className="tabbed-content tabbed-content--services-facilities-footer tabbed-content--border">
      <Tabs>
        <div
          className={
            "tabbed-content__dropdown" + (windowSize > 700 ? " container" : "")
          }
        >
          {windowSize < 700 && (
            <>
              <ul className="services-header-container">
                <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                  <h2>Services & Facillities</h2>
                </li>
                <li className="tabbed-content__link">
                  <a
                    className="link link--uppercase link--arrow"
                    href="/services-facilities"
                  >
                    See all{" "}
                    <span className="arrow">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="29.789"
                        height="8.21"
                      >
                        <g data-name="Group 1359">
                          <path
                            data-name="Path 759"
                            d="M0 4.105h20.186"
                            stroke="#000"
                            strokeWidth=".5"
                          />
                          <path
                            data-name="Polygon 1"
                            d="M29.789 4.1L18.842 8.2V0z"
                          />
                        </g>
                      </svg>
                    </span>
                  </a>
                </li>
              </ul>
              <div className="tabbed-content__dropdown-button">
                <button onClick={toggleDropdown}>
                  Select department{" "}
                  <div
                    className={
                      "tabbed-content__dropdown-button-icon" +
                      (showDropDown ? " rotate" : "")
                    }
                  >
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="11"
                        height="5"
                      >
                        <path d="M5.5 5L0 0h11z" data-name="Polygon 9"></path>
                      </svg>
                    </span>
                  </div>
                </button>
              </div>
            </>
          )}
          <TabList
            className={
              "teaser-container__top teaser-container__top--tabs " +
              (showDropDown ? showDropDownClass : hideDropDownClass)
            }
          >
            {windowSize > 700 && (
              <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                <h2>Services & Facillities</h2>
              </li>
            )}

            {orderedCats.map((category) => {
              if (!category || !category.name) return;
              return (
                <Tab key={category.id}>
                  <h3 onClick={closeDropDown}>{parse(category.name)}</h3>
                </Tab>
              );
            })}
            {windowSize > 700 && (
              <li className="tabbed-content__link">
                <a
                  className="link link--uppercase link--arrow"
                  href="/services-facilities"
                >
                  All services & facilities{" "}
                  <span className="arrow">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="29.789"
                      height="8.21"
                    >
                      <g data-name="Group 1359">
                        <path
                          data-name="Path 759"
                          d="M0 4.105h20.186"
                          stroke="#000"
                          strokeWidth=".5"
                        />
                        <path
                          data-name="Polygon 1"
                          d="M29.789 4.1L18.842 8.2V0z"
                        />
                      </g>
                    </svg>
                  </span>
                </a>
              </li>
            )}
          </TabList>
        </div>

        {orderedCats.map((amenityCat) => {
          let amenity = amenities.filter((item) => {
            if (!item["amenity-category"]) return false;
            return item["amenity-category"].includes(amenityCat.id);
          });
          amenity = amenity.slice(0, 4);
          return (
            <TabPanel key={amenityCat.id}>
              <div className="tabbed-content__teasers">
                <div className="container teaser-container">
                  <div className="teaser-content">
                    {amenity.map((amenityItem) => {
                      const regex = /(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
                      let content = amenityItem.content.rendered;
                      const includesLink = amenityItem.content.rendered.match(
                        regex
                      );
                      if (includesLink) {
                        includesLink.forEach((link) => {
                          content = content.replace(link, getSlug(link));
                        });
                      }

                      return (
                        <div
                          className="teaser-content--fourth teaser-content--services"
                          key={`amenity-${uuidv4()}`}
                        >
                          <Link
                            as={
                              amenityItem.acf.hasOwnProperty("external_link") &&
                              amenityItem.acf.external_link !== ""
                                ? amenityItem.acf.external_link
                                : `${getSlug(amenityItem.link)}`
                            }
                            href={
                              amenityItem.acf.hasOwnProperty("external_link") &&
                              amenityItem.acf.external_link !== ""
                                ? amenityItem.acf.external_link
                                : `${getSlug(amenityItem.link)}`
                            }
                          >
                            <a>
                              <h2 className="teaser__subject">
                                {parse(amenityItem.title.rendered)}
                              </h2>

                              <div className="teaser__img">
                                {/* <img src={amenityItem.acf.image.url} alt="" /> */}
                                {parse(amenityItem.acf.image.svg)}
                              </div>
                            </a>
                          </Link>

                          <div className="teaser-content__inside">
                            <div className="teaser__description">
                              <Truncate lines={4} ellipsis={<span>...</span>}>
                                {parse(content)}
                              </Truncate>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </TabPanel>
          );
        })}
      </Tabs>
    </section>
  );
};

export default TabbedServicesFacilitiesComponent;
