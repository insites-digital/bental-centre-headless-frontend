import React, { useEffect, useState } from "react";
import moment from "moment";
import "moment-timezone";
import Tag from "../Tag";
import Link from "next/link";
import { getSlug } from "../../utils/getSlug";
import parse from "html-react-parser";
import TeaserImage from "../TeaserImage";

let emptyProductGrids = [];
let emptyGridAmounts;

const EventTab = (props) => {
  const { events, categories, searchResults, windowSize } = props.data;
  const [eventData, setEventData] = useState(events);
  const [initialRenderSize, setInitialRenderSize] = useState(
    events.length < 8 ? events.length : 8
  );

  useEffect(() => {
    if (searchResults === null) return;
    if (searchResults.length === 0) setEventData(events);
    if (searchResults !== null && searchResults.length > 0)
      setEventData(searchResults);
  }, [searchResults]);

  const showMore = () => {
    // show more stores
    setInitialRenderSize(initialRenderSize + 4);
  };

  const EventMarkup = () => {
    // Add in empty grids if un even amount of events
    emptyProductGrids = [];
    const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;

    let count = eventData.slice(0, initialRenderSize).length;
    if (windowSize >= 940) {
      if (count % 4 !== 0) {
        emptyGridAmounts = roundUp(count, 4) - count;
        for (let i = 1; i <= emptyGridAmounts; i++) {
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
        }
      }
    } else if (windowSize >= 700 && windowSize < 940) {
      if (count % 2 !== 0) {
        emptyGridAmounts = roundUp(count, 2) - count;
        for (let i = 1; i <= emptyGridAmounts; i++) {
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
        }
      }
    }
    return eventData.slice(0, initialRenderSize).map((elem, index) => {
      const eventCategories = categories.filter((category) => {
        if (!elem["event-category"]) return false;
        return elem["event-category"].includes(category.id);
      });
      eventCategories.sort((a, b) => {
        if (!a.acf || !b.acf) return -1;
        return a.acf.colour.length < b.acf.colour.length ? 1 : -1;
      });
      const splitDate = (str) => str.split("/");
      const formattedDate = new Date(
        `${
          splitDate(elem.acf.dates.start_date)[1] +
          "/" +
          splitDate(elem.acf.dates.start_date)[0] +
          "/" +
          splitDate(elem.acf.dates.start_date)[2]
        }`
      );
      const formattedEndDate = new Date(
        `${
          splitDate(elem.acf.dates.end_date)[1] +
          "/" +
          splitDate(elem.acf.dates.end_date)[0] +
          "/" +
          splitDate(elem.acf.dates.end_date)[2]
        }`
      );
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0");
      var yyyy = today.getFullYear();

      today = mm + "/" + dd + "/" + yyyy;
      moment().tz("Europe/London");
      const dateHasPassed = moment(formattedEndDate).isBefore(today);

      if (dateHasPassed) return <></>;

      const startDateHasPassed = moment(formattedDate).isBefore(today);

      function getdateFormated(date) {
        var a = moment(today);
        var b = moment(date);

        var days = b.diff(a, "days");
        var weeks = Math.round(b.diff(a, "week", true));

        var calback = function () {
          if (weeks === 0) {
            return "[In " + days + " days]";
          } else if (weeks === 1) {
            return "[Next Week]";
          } else {
            return `[In ${weeks} weeks]`;
          }
        };
        return moment(date).calendar(null, {
          sameDay: "[Today]",
          nextDay: "[Tomorrow]",
          nextWeek: calback,
          sameElse: calback,
        });
      }

      const dateOverview = startDateHasPassed
        ? "today"
        : getdateFormated(formattedDate);

      let endsOnSameDay = false;

      let startDayArray = elem.acf.dates.start_date.split("/");
      let endDayArray = elem.acf.dates.end_date.split("/");
      if (startDayArray[1] === endDayArray[1]) {
        if (startDayArray[0] === endDayArray[0]) {
          endsOnSameDay = true;
        }
      }

      return (
        <div className="teaser-content--fourth">
          <Link as={`${getSlug(elem.link)}`} href={`${getSlug(elem.link)}`}>
            <a
              style={{
                display: "flex",
                flexDirection: "column",
                height: "100%",
                width: "100%",
              }}
            >
              <div className="teaser__img">
                {windowSize < 700 && (
                  <TeaserImage data={elem.acf.image} imageSize={"100vw"} />
                )}
                {windowSize > 700 && windowSize < 940 && (
                  <TeaserImage data={elem.acf.image} imageSize={"50vw"} />
                )}
                {windowSize > 940 && (
                  <TeaserImage data={elem.acf.image} imageSize={"33vw"} />
                )}
              </div>
              <div className="teaser-content__inside" style={{ flexGrow: "1" }}>
                <div className="teaser__span-container">
                  <span className="teaser__span">{dateOverview}</span>
                </div>
                <h2 className="teaser__subject">
                  {parse(elem.title.rendered)}
                </h2>
                <h3 className="teaser__date">
                  {parse(elem.acf.dates.start_date)}
                  {!endsOnSameDay && <> - {elem.acf.dates.end_date}</>}
                </h3>
              </div>
              <div className="teaser__tag-container">
                {eventCategories.map((item) => {
                  if (!item || !item.name) return;
                  <Tag colour={item.acf.colour} title={item.name} />;
                })}
              </div>
            </a>
          </Link>
        </div>
      );
      // }
    });
  };
  return (
    <>
      {EventMarkup()}
      {emptyProductGrids.length > 0 &&
        emptyProductGrids.map((productItem) => {
          return parse(productItem);
        })}
      {eventData !== null && (
        <div className="pagination">
          <div className="pagination__amount">
            <span>
              Showing {eventData.slice(0, initialRenderSize).length} out of{" "}
              {eventData.length} events
            </span>
          </div>

          <button
            className={
              "pagination__view-more" +
              (eventData.slice(0, initialRenderSize).length === eventData.length
                ? " pagination__view-more--disabled"
                : "")
            }
            onClick={showMore}
          >
            See More Events
          </button>
        </div>
      )}
    </>
  );
};

export default EventTab;
