import React, { useState, useEffect } from "react";
import OpenStatus from "../OpenStatus";
import Tag from "../Tag";
import moment from "moment";
import Link from "next/link";
import { getSlug } from "../../utils/getSlug";
import parse from "html-react-parser";
import { decodeString } from "../../utils/decodeString";
import he from "he";
import { truncate } from "../../utils/truncate";
import Image from "../TeaserImage";

const StoreTab = (props) => {
  const [initialRenderSize, setInitialRenderSize] = useState(null);
  const [initialStores, setInitialStores] = useState(null);
  const { windowSize } = props;
  const [stores, setStores] = useState(null);
  const [showStores, setShowStores] = useState(false);
  const [searchResults, setSearchResults] = useState(props.searchResults);
  const [storesData, setStoresData] = useState([]);
  const [bentallStore, setBentallStore] = useState([]);
  const [displayProductGrids, setEmptyProductGrids] = useState([]);

  let emptyProductGrids = [];
  let emptyGridAmounts;

  useEffect(() => {
    let tempStoresData = props.stores.filter((store) => {
      if (!store["store-category"]) return false;
      return store["store-category"].includes(props.department.id);
    });

    setBentallStore(props.stores.filter((store) => store.id === 252));

    tempStoresData = tempStoresData.filter((store) => {
      return store.id !== 252;
    });

    if (props.department.id === -1) {
      tempStoresData = props.stores;
    }

    if (initialStores === null) {
      setInitialStores(tempStoresData);
    }

    if (props.searchResults !== null && props.searchResults.length > 0) {
      tempStoresData = props.searchResults;
    }

    if (stores === null) setStores(tempStoresData);

    if (!initialRenderSize) {
      if (props.department.id !== 19) {
        setInitialRenderSize(
          tempStoresData.length < 10 ? tempStoresData.length : 10
        );
      } else {
        setInitialRenderSize(tempStoresData.length);
      }
    }

    setStoresData(tempStoresData);
    setStores(tempStoresData);
  }, []);

  useEffect(() => {
    if (stores !== null && stores.length > 0) {
      setShowStores(true);
    }
  }, [stores]);

  useEffect(() => {
    if (props.searchResults !== null && props.searchResults.length > 0) {
      setStores(props.searchResults);
      setSearchResults(props.searchResults);
    } else {
      if (initialStores) setStores(initialStores);
    }
  }, [props.searchResults]);

  useEffect(() => {
    if (initialRenderSize) {
      const roundUp = (number, roundTo) =>
        Math.ceil(number / roundTo) * roundTo;
      if (props.department.id !== 19) {
        if (windowSize >= 700) {
          if (stores.slice(0, initialRenderSize).length === 1) {
            // add another half
            emptyProductGrids.push('<div class="teaser-content--half"></div>');
          } else if (stores.slice(0, initialRenderSize).length === 2) {
            emptyProductGrids.push(
              '<div class="teaser-content--fourth"></div>'
            );
          }
        }
        if (windowSize >= 940) {
          if (stores.slice(0, initialRenderSize).length === 4) {
            // add 1 empty fourths
            emptyProductGrids.push(
              '<div class="teaser-content--fourth"></div>'
            );
            emptyProductGrids.push('<div class="teaser-content--half"></div>');
          } else if (stores.slice(0, initialRenderSize).length === 5) {
            emptyProductGrids.push('<div class="teaser-content--half"></div>');
          } else if (stores.slice(0, initialRenderSize).length > 5) {
            // need to find modulus of 4
            let count = stores.slice(0, initialRenderSize).length - 6;
            if (count % 4 !== 0) {
              emptyGridAmounts = roundUp(count, 4) - count;
              for (let i = 1; i <= emptyGridAmounts; i++) {
                emptyProductGrids.push(
                  '<div class="teaser-content--fourth"></div>'
                );
              }
            }
          }
        } else if (windowSize >= 700 && windowSize < 940) {
          if (stores.slice(0, initialRenderSize).length === 3) {
            emptyProductGrids.push(
              '<div class="teaser-content--fourth"></div>'
            );
          }
          if (stores.slice(0, initialRenderSize).length >= 5) {
            let count = stores.slice(0, initialRenderSize).length;
            if (count % 2 !== 0) {
              emptyGridAmounts = roundUp(count, 2) - count;
              for (let i = 1; i <= emptyGridAmounts; i++) {
                emptyProductGrids.push(
                  '<div class="teaser-content--fourth"></div>'
                );
              }
            }
          }
        }
      } else {
        if (
          stores.slice(0, initialRenderSize).length === 1 &&
          windowSize >= 940
        ) {
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
        } else if (
          stores.slice(0, initialRenderSize).length === 2 &&
          windowSize >= 940
        ) {
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
        } else if (
          stores.slice(0, initialRenderSize).length === 4 &&
          windowSize >= 940
        ) {
          emptyProductGrids.push('<div class="teaser-content--fourth"></div>');
          emptyProductGrids.push('<div class="teaser-content--half"></div>');
        } else if (
          stores.slice(0, initialRenderSize).length === 5 &&
          windowSize >= 940
        ) {
          emptyProductGrids.push('<div class="teaser-content--half"></div>');
        } else if (
          stores.slice(0, initialRenderSize).length === 7 &&
          windowSize >= 940
        ) {
          emptyProductGrids.push('<div class="teaser-content--half"></div>');
        } else if (
          stores.slice(0, initialRenderSize).length > 8 &&
          windowSize >= 940
        ) {
          let count = stores.slice(0, initialRenderSize).length - 8;
          if (count % 4 !== 0) {
            emptyGridAmounts = roundUp(count, 4) - count;
            for (let i = 1; i <= emptyGridAmounts; i++) {
              emptyProductGrids.push(
                '<div class="teaser-content--fourth"></div>'
              );
            }
          }
        }
      }

      setEmptyProductGrids(emptyProductGrids);
    }
  }, [initialRenderSize]);

  const showMore = () => {
    // show more stores
    if (
      props.department.hasOwnProperty("name") &&
      props.department.name === "Bentalls"
    ) {
      var win = window.open("https://www.fenwick.co.uk/brands", "_blank");
      win.focus();
    } else {
      setInitialRenderSize(initialRenderSize + 4);
    }
  };

  const RenderStores = ({ store, index, department }) => {
    const floors = props.floors.filter((floor) => {
      if (!store.floor) return false;
      return store.floor.includes(floor.id);
    });
    const clickNCollect = store.acf.facilities.filter((facility) => {
      return facility.toLowerCase().replace(/ /g, "") === "click&collect";
    });
    const bentallsTag = props.storeCats.filter(
      (category) => category.id === 19
    );

    if (props.department.id === 19) {
      if (
        index === 1 ||
        index === 2 ||
        index === 3 ||
        index === 4 ||
        index > 7
      ) {
        return (
          <div className="teaser-content--fourth">
            <Link as={`${getSlug(store.link)}`} href={`${getSlug(store.link)}`}>
              <a>
                {store.acf.image && (
                  <div className="teaser__img">
                    <Image data={store.acf.image} imageSize={"100vw"} />
                  </div>
                )}

                <OpenStatus openingHours={store.acf.opening_hours} />

                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(store.title.rendered)}
                  </h2>
                  {store.content.rendered !== "" && (
                    <div className="teaser__description">
                      {truncate.apply(
                        decodeString(he.decode(store.content.rendered)),
                        [200, true]
                      )}
                    </div>
                  )}
                </div>
                {floors && floors.length > 0 && (
                  <div className="teaser__tag-container">
                    {floors.map((floor) => {
                      if (!floor || !floor.name) return;
                      return <span className="teaser__tag">{floor.name}</span>;
                    })}
                    {clickNCollect &&
                      clickNCollect.length > 0 &&
                      clickNCollect.map((facility) => (
                        <span className="teaser__tag">{facility}</span>
                      ))}

                    {store["store-category"] &&
                      store["store-category"].includes(19) &&
                      bentallsTag &&
                      bentallsTag.length > 0 &&
                      bentallsTag.map((tag, index) => {
                        if (!tag || !tag.name) return;
                        return (
                          <>
                            <Tag colour={tag.acf.colour} title={tag.name} />
                          </>
                        );
                      })}
                  </div>
                )}
              </a>
            </Link>
          </div>
        );
      } else {
        return (
          <div className="teaser-content--half">
            <Link as={`${getSlug(store.link)}`} href={`${getSlug(store.link)}`}>
              <a>
                {store.acf.image && (
                  <div className="teaser__img">
                    <Image data={store.acf.image} imageSize={"50vw"} />
                  </div>
                )}

                <OpenStatus openingHours={store.acf.opening_hours} />

                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(store.title.rendered)}
                  </h2>

                  {store.content.rendered !== "" && (
                    <div className="teaser__description">
                      {truncate.apply(
                        decodeString(he.decode(store.content.rendered)),
                        [200, true]
                      )}
                    </div>
                  )}
                </div>
                {floors && floors.length > 0 && (
                  <div className="teaser__tag-container">
                    {floors.map((floor) => {
                      if (!floor || !floor.name) return;

                      return <span className="teaser__tag">{floor.name}</span>;
                    })}
                    {clickNCollect &&
                      clickNCollect.length > 0 &&
                      clickNCollect.map((facility) => (
                        <span className="teaser__tag">{facility}</span>
                      ))}
                    {store["store-category"] &&
                      store["store-category"].includes(19) &&
                      bentallsTag &&
                      bentallsTag.length > 0 &&
                      bentallsTag.map((tag, index) => {
                        if (!tag || !tag.name) return;
                        return (
                          <>
                            <Tag colour={tag.acf.colour} title={tag.name} />
                          </>
                        );
                      })}
                  </div>
                )}
              </a>
            </Link>
          </div>
        );
      }
    } else {
      if (index === 0 || index === 5) {
        return (
          <div className="teaser-content--half">
            <Link as={`${getSlug(store.link)}`} href={`${getSlug(store.link)}`}>
              <a>
                {store.acf.image && (
                  <div className="teaser__img">
                    {/* <img src={store.acf.image.url} alt={store.title.rendered} /> */}
                    <Image data={store.acf.image} imageSize={"50vw"} />
                  </div>
                )}
                <OpenStatus openingHours={store.acf.opening_hours} />

                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(store.title.rendered)}
                  </h2>
                  {store.content.rendered !== "" && (
                    <div className="teaser__description">
                      {/* {parse(store.content.rendered)} */}
                      {truncate.apply(
                        decodeString(he.decode(store.content.rendered)),
                        [200, true]
                      )}
                    </div>
                  )}
                </div>
                {floors && floors.length > 0 && (
                  <div className="teaser__tag-container">
                    {floors.map((floor) => {
                      if (!floor || !floor.name) return;
                      return <span className="teaser__tag">{floor.name}</span>;
                    })}
                    {clickNCollect &&
                      clickNCollect.length > 0 &&
                      clickNCollect.map((facility) => (
                        <span className="teaser__tag">{facility}</span>
                      ))}
                    {store["store-category"] &&
                      store["store-category"].includes(19) &&
                      bentallsTag &&
                      bentallsTag.length > 0 &&
                      bentallsTag.map((tag, index) => {
                        if (!tag || !tag.name) return;
                        return (
                          <>
                            <Tag colour={tag.acf.colour} title={tag.name} />
                          </>
                        );
                      })}
                  </div>
                )}
              </a>
            </Link>
          </div>
        );
      } else if (
        index === 1 ||
        index === 2 ||
        index === 3 ||
        index === 4 ||
        index >= 6
      ) {
        return (
          <div className="teaser-content--fourth">
            <Link as={`${getSlug(store.link)}`} href={`${getSlug(store.link)}`}>
              <a>
                {store.acf.image && (
                  <div className="teaser__img">
                    {/* <img src={store.acf.image.url} alt={store.title.rendered} /> */}
                    <Image data={store.acf.image} imageSize={"33vw"} />
                  </div>
                )}
                <OpenStatus openingHours={store.acf.opening_hours} />

                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(store.title.rendered)}
                  </h2>
                  {store.content.rendered !== "" && (
                    <div className="teaser__description">
                      {/* {parse(store.content.rendered)} */}
                      {truncate.apply(
                        decodeString(he.decode(store.content.rendered)),
                        [200, true]
                      )}
                    </div>
                  )}
                </div>
                {floors && floors.length > 0 && (
                  <div className="teaser__tag-container">
                    {floors.map((floor) => {
                      if (!floor || !floor.name) return;
                      return <span className="teaser__tag">{floor.name}</span>;
                    })}
                    {clickNCollect &&
                      clickNCollect.length > 0 &&
                      clickNCollect.map((facility) => (
                        <span className="teaser__tag">{facility}</span>
                      ))}
                    {store["store-category"] &&
                      store["store-category"].includes(19) &&
                      bentallsTag &&
                      bentallsTag.length > 0 &&
                      bentallsTag.map((tag, index) => {
                        if (!tag || !tag.name) return;
                        return (
                          <>
                            <Tag colour={tag.acf.colour} title={tag.name} />
                          </>
                        );
                      })}
                  </div>
                )}
              </a>
            </Link>
          </div>
        );
      }
    }
  };

  return (
    <>
      <div className="tabbed-content__teasers">
        <div className="container teaser-container">
          <div
            className={
              "teaser-content" +
              (props.department.id === 19 ? " teaser-content--bentalls" : "")
            }
          >
            {props.department.id === 19 &&
              bentallStore[0] &&
              bentallStore[0].acf && (
                <>
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <Image
                        data={bentallStore[0].acf.image}
                        imageSize={"50vw"}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <h2 className="teaser__subject">
                      {parse(bentallStore[0].title.rendered)}
                    </h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        {parse(bentallStore[0].content.rendered)}
                        {bentallStore[0].acf.opening_hours.length > 0 && (
                          <div className="teaser__span-container">
                            <table className="opening-times">
                              <tbody>
                                {bentallStore[0].acf.opening_hours.map(
                                  (opening) => (
                                    <tr>
                                      <td className="capitalize">
                                        {opening.day}
                                      </td>
                                      <td>
                                        {moment(
                                          new Date(
                                            "1970-01-01T" + opening.start_time
                                          )
                                        )
                                          .format("h:mma")
                                          .replace(":00", "")}{" "}
                                        <span className="dash">-</span>{" "}
                                        {moment(
                                          new Date(
                                            "1970-01-01T" + opening.end_time
                                          )
                                        )
                                          .format("h:mma")
                                          .replace(":00", "")}
                                      </td>
                                    </tr>
                                  )
                                )}
                              </tbody>
                            </table>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </>
              )}

            {showStores &&
              stores !== null &&
              stores.slice(0, initialRenderSize).map((store, index) => {
                return RenderStores({
                  index: index,
                  store: store,
                  department: props.department,
                });
              })}

            {showStores &&
              stores !== null &&
              displayProductGrids.length > 0 &&
              displayProductGrids.map((productItem) => {
                return parse(productItem);
              })}
          </div>
        </div>

        {showStores && stores !== null && (
          <div className="pagination">
            <div className="pagination__amount">
              <span>
                Showing {stores.slice(0, initialRenderSize).length} out of{" "}
                {stores.length} brands
              </span>
            </div>
            {props.department.hasOwnProperty("name") &&
              props.department.name === "Bentalls" && (
                <button className={"pagination__view-more"} onClick={showMore}>
                  See More Bentall Brands
                </button>
              )}
            {props.department.id !== 19 && (
              <button
                className={
                  "pagination__view-more" +
                  (stores.slice(0, initialRenderSize).length === stores.length
                    ? " pagination__view-more--disabled"
                    : "")
                }
                onClick={showMore}
              >
                See more brands
              </button>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default StoreTab;
