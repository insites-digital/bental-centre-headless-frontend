import React, { useState, useEffect } from "react";
import Link from "next/link";
import { getSlug } from "../../utils/getSlug";
import OpenStatus from "../OpenStatus";
import parse from "html-react-parser";
import { truncate } from "../../utils/truncate";
import { decodeString } from "../../utils/decodeString";
import TeaserImage from "../TeaserImage";
import he from "he";

const RestaurantTab = (props) => {
  const { searchResults, windowSize } = props;
  const [restaurants, setRestaurants] = useState(props.restaurants);
  const [initialRenderSize, setInitialRenderSize] = useState(
    restaurants.length < 6 ? restaurants.length : 6
  );

  const showMore = () => {
    // show more stores
    setInitialRenderSize(initialRenderSize + 3);
  };

  useEffect(() => {
    if (searchResults !== null && searchResults.length > 0)
      setRestaurants(searchResults);

    if (searchResults !== null && searchResults.length === 0) {
      setRestaurants(props.restaurants);
    }
  }, [searchResults]);

  const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;
  let emptyGridItems = [];
  let emptyGridAmounts = 0;
  if (windowSize >= 700 && windowSize < 941) {
    if (restaurants.slice(0, initialRenderSize).length % 2 !== 0) {
      emptyGridAmounts =
        roundUp(restaurants.slice(0, initialRenderSize).length, 2) -
        restaurants.slice(0, initialRenderSize).length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyGridItems.push('<div class="teaser-content--third"></div>');
      }
    }
  } else if (windowSize >= 941) {
    if (restaurants.slice(0, initialRenderSize).length % 3 !== 0) {
      emptyGridAmounts =
        roundUp(restaurants.slice(0, initialRenderSize).length, 3) -
        restaurants.slice(0, initialRenderSize).length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyGridItems.push('<div class="teaser-content--third"></div>');
      }
    }
  }

  const RestaurantsMarkup = () => {
    return restaurants.slice(0, initialRenderSize).map((restaurant) => {
      const floor = props.floors.filter(
        (item) => item.id === restaurant.floor[0]
      );

      let hoverSource = "";

      if (restaurant.acf.image.hover) {
        hoverSource =
          restaurant.acf.image.hover.sizes.xsmall_33vw +
          " 640w, " +
          restaurant.acf.image.hover.sizes.xsmall_33vw +
          " 768w, " +
          restaurant.acf.image.hover.sizes.small_33vw +
          " 1024w, " +
          restaurant.acf.image.hover.sizes.medium_33vw +
          " 1366w, " +
          restaurant.acf.image.hover.sizes.large_33vw +
          " 1600w, " +
          restaurant.acf.image.hover.sizes.xlarge_50vw +
          " 1920w ";
      }

      return (
        <div className="teaser-content--third">
          <Link
            as={`${getSlug(restaurant.link)}`}
            href={`${getSlug(restaurant.link)}`}
          >
            <a>
              <div className="teaser__img">
                {windowSize < 700 && (
                  <TeaserImage
                    data={restaurant.acf.image}
                    imageSize={"100vw"}
                  />
                )}
                {windowSize > 700 && windowSize < 940 && (
                  <TeaserImage data={restaurant.acf.image} imageSize={"50vw"} />
                )}
                {windowSize > 940 && (
                  <TeaserImage data={restaurant.acf.image} imageSize={"33vw"} />
                )}
                {restaurant.acf.image.hover && (
                  <img
                    className="hover"
                    src={restaurant.acf.image.hover.sizes.medium_33vw}
                    srcset={hoverSource}
                    alt=""
                  />
                )}
              </div>
              <OpenStatus openingHours={restaurant.acf.opening_hours} />
              <div className="teaser-content__inside">
                <h2 className="teaser__subject">
                  {parse(restaurant.title.rendered)}
                </h2>
                <div className="teaser__description">
                  {truncate.apply(
                    decodeString(he.decode(restaurant.content.rendered)),
                    [200, true]
                  )}
                </div>
                <div className="teaser__tag-container">
                  {floor[0] && floor[0].name && (
                    <span className="teaser__tag">{parse(floor[0].name)}</span>
                  )}

                  {restaurant.acf.facilities &&
                    restaurant.acf.facilities.length > 0 &&
                    restaurant.acf.facilities.map((facility) => {
                      return <span className="teaser__tag">{facility}</span>;
                    })}
                </div>
              </div>
            </a>
          </Link>
        </div>
      );
    });
  };
  return (
    <>
      <div className="container teaser-container">
        <div className="teaser-content">
          {RestaurantsMarkup()}
          {emptyGridItems.map((item) => parse(item))}
        </div>
      </div>
      {restaurants !== null && (
        <div className="pagination">
          <div className="pagination__amount">
            <span>
              Showing {restaurants.slice(0, initialRenderSize).length} out of{" "}
              {restaurants.length} restaurants
            </span>
          </div>

          <button
            className={
              "pagination__view-more" +
              (restaurants.slice(0, initialRenderSize).length ===
              restaurants.length
                ? " pagination__view-more--disabled"
                : "")
            }
            onClick={showMore}
          >
            See More Restaurants
          </button>
        </div>
      )}
    </>
  );
};

export default RestaurantTab;
