import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import RestaurantTab from "./RestaurantTab";
import ContentSearch from "../ContentSearch";

const TabbedRestaurants = (props) => {
  const [windowSize, setWindowSize] = useState(961);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [searchResults, setSearchResults] = useState(null);
  const [restaurantData, setRestaurantData] = useState(null);
  const [showDropDown, setShowDropDown] = useState(false);
  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const tabs = props.categories.filter((cat) => cat.count > 0);

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  };

  const resetSearchResults = () => {
    setSearchResults([]);
  };
  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");
    setWindowSize(window.innerWidth);
    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 300);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      slider.removeEventListener("mousedown", (e) => {
        isDown = true;
        slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.removeEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
      slider.removeEventListener("mouseleave", () => {
        isDown = false;
        slider.classList.remove("active");
      });
      slider.removeEventListener("mouseup", () => {
        isDown = false;
        slider.classList.remove("active");
      });
    };
  }, []);

  const handleSearchChange = (items) => {
    setSearchResults(items);
  };

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  return (
    <section className="tabbed-content container tabbed-content--restaurants">
      <Tabs
        selectedIndex={selectedIndex}
        onSelect={(index, lastIndex, event) => {
          if (index === lastIndex) {
            setSelectedIndex(0);
          } else {
            setSelectedIndex(index);
          }
        }}
      >
        <div className="tabbed-content__dropdown">
          {windowSize < 700 && (
            <>
              <ul className="teaser-container__top teaser-container__top--mobile">
                <li className="teaser__title">
                  <h2>{parse(props.title)}</h2>
                </li>
              </ul>
              <div className="tabbed-content__dropdown-button">
                <button onClick={toggleDropdown}>
                  Select Category{" "}
                  <div
                    className={
                      "tabbed-content__dropdown-button-icon" +
                      (showDropDown ? " rotate" : "")
                    }
                  >
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="11"
                        height="5"
                      >
                        <path d="M5.5 5L0 0h11z" data-name="Polygon 9"></path>
                      </svg>
                    </span>
                  </div>
                </button>
              </div>
            </>
          )}
          <TabList
            className={
              "teaser-container__top teaser-container__top--tabs " +
              (showDropDown ? showDropDownClass : hideDropDownClass)
            }
          >
            {windowSize > 700 && (
              <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                <h2>{parse(props.title)}</h2>
              </li>
            )}
            <Tab
              className="react-tabs__tab"
              // style={{
              //   visibility: "hidden",
              //   pointerEvents: "none",
              //   position: "absolute",
              //   left: "-1000%",
              // }}
            >
              <h3>All</h3>
            </Tab>
            {tabs &&
              tabs.length > 0 &&
              tabs.map((tab) => (
                <Tab>
                  <h3
                    onClick={() => {
                      closeDropDown();
                      resetSearchResults();
                    }}
                  >
                    {tab && tab.name ? tab.name : ""}
                  </h3>
                </Tab>
              ))}
          </TabList>
        </div>

        <TabPanel>
          <ContentSearch
            searchData={props.restaurants}
            handleSearchChange={handleSearchChange}
            placeholder={"Search for a restaurant"}
          />
          <div className="tabbed-content__teasers tabbed-content__teasers--restaurants">
            <RestaurantTab
              restaurants={props.restaurants}
              searchResults={searchResults}
              floors={props.floors}
              windowSize={windowSize}
            />
          </div>
        </TabPanel>

        {tabs.map((tab) => {
          const categoryId = tab.id;
          const restaurants = props.restaurants.filter((restaurant) => {
            if (!restaurant["restaurant-category"]) return false;
            return restaurant["restaurant-category"].includes(categoryId);
          });
          if (restaurantData === null) setRestaurantData(restaurants);
          if (restaurants.length < 1) return <></>;

          return (
            <TabPanel key={tab.id}>
              <ContentSearch
                searchData={restaurants}
                handleSearchChange={handleSearchChange}
                placeholder={"Search for a restaurant"}
              />
              <div className="tabbed-content__teasers tabbed-content__teasers--restaurants">
                {restaurantData !== null && (
                  <>
                    <RestaurantTab
                      restaurants={restaurants}
                      searchResults={searchResults}
                      floors={props.floors}
                      windowSize={windowSize}
                    />
                  </>
                )}
              </div>
            </TabPanel>
          );
        })}
      </Tabs>
    </section>
  );
};

export default TabbedRestaurants;
