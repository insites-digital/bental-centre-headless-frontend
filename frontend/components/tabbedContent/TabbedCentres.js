import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import { getSlug } from "../../utils/getSlug";
import { getUrlVars } from "../../utils/getUrlVars";
import moment from "moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import disabledParkingIcon from "../../static/images/site/disabled-parking.svg";
import "react-tabs/style/react-tabs.css";
import Map from "../Map";
import Config from "../../config";
import { useRouter } from "next/router";

const TabbedCentres = (props) => {
  // console.log("props :", props);
  const router = useRouter();
  const [windowSize, setWindowSize] = useState(961);
  const [showDropDown, setShowDropDown] = useState(false);
  const [openingHours, setOpeningHours] = useState(props.data.opening_hours);
  const [tabIndex, setTabIndex] = useState(null);
  const showDropDownClass = "teaser-container__top--tab-show";
  const hideDropDownClass = "teaser-container__top--tab-hide";

  const initDraggableTabs = () => {
    const slider = document.querySelector(".teaser-container__top");
    if (!slider) return;
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      slider.classList.remove("active");
    });
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  };

  useEffect(() => {
    const slider = document.querySelector(".teaser-container__top");
    setWindowSize(window.innerWidth);

    const getTabIndex = (expression) => {
      switch (expression) {
        case "opening-hours":
          setTabIndex(0);
          break;
        case "about-us":
          setTabIndex(1);
          break;
        case "how-to-get-here":
          setTabIndex(2);
          break;
        case "parking":
          setTabIndex(3);
          break;
        case "services-facilities":
          setTabIndex(4);
          break;
        case "sustainability":
          setTabIndex(4);
          break;
        case "click-collect":
          setTabIndex(5);
          break;
        default:
        //
      }
    };

    // Check if any tabs should be open from url parameters
    const urlVars = getUrlVars();
    if (urlVars.length > 0) {
      urlVars.forEach((urlVar) => {
        if (urlVar.key === "opening-hours") {
          setTabIndex(0);
        } else if (urlVar.key === "about-us") {
          setTabIndex(1);
        } else if (urlVar.key === "how-to-get-here") {
          setTabIndex(2);
        } else if (urlVar.key === "parking") {
          setTabIndex(3);
        } else if (urlVar.key === "services-facilities") {
          setTabIndex(4);
        } else if (urlVar.key === "sustainability") {
          setTabIndex(4);
        } else if (urlVar.key === "click-collect") {
          setTabIndex(6);
        }
      });
    } else {
      setTabIndex(0);
    }

    if (props.query && props.query.q) {
      getTabIndex(props.query.q);
    }

    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
    });
    setTimeout(() => {
      initDraggableTabs();
    }, 600);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
      if (slider) {
        slider.removeEventListener("mousedown", (e) => {
          isDown = true;
          slider.classList.add("active");
          startX = e.pageX - slider.offsetLeft;
          scrollLeft = slider.scrollLeft;
        });
        slider.removeEventListener("mousemove", (e) => {
          if (!isDown) return;
          e.preventDefault();
          const x = e.pageX - slider.offsetLeft;
          const walk = (x - startX) * 3; //scroll-fast
          slider.scrollLeft = scrollLeft - walk;
        });
        slider.removeEventListener("mouseleave", () => {
          isDown = false;
          slider.classList.remove("active");
        });
        slider.removeEventListener("mouseup", () => {
          isDown = false;
          slider.classList.remove("active");
        });
      }
    };
  }, []);

  const today = moment().format("dddd").toLowerCase();

  const toggleDropdown = () => {
    setShowDropDown(!showDropDown);
  };

  const closeDropDown = () => {
    setShowDropDown(false);
  };

  const formatLinks = (text) => {
    let string = text;
    let originalLink = text.match(/href="([^"]*)/);

    if (
      originalLink &&
      !originalLink.includes("mailto:") &&
      !originalLink.includes("tel:")
    ) {
      originalLink = originalLink[1];
    } else {
      return text;
    }
    let formattedLink;
    if (originalLink.indexOf(Config.hostName) < 0) {
      formattedLink = originalLink;
    } else {
      formattedLink = getSlug(originalLink);
    }

    string = string.replace(originalLink, formattedLink);
    return string;
  };

  const bentallStoreText = formatLinks(props.data.general.content);
  const aboutUsContent = formatLinks(props.data.about_us.content);
  const directionsContent = formatLinks(props.data.directions.content);
  const parkingContent = formatLinks(props.data.parking.content);
  const sustainabilityContent = formatLinks(props.data.sustainability.content);
  const clickCollectContent = formatLinks(props.data.click_collect.content);
  const centreMapContent = formatLinks(props.data.centre_map.content);
  return (
    <section className="tabbed-content container tabbed-content--centres">
      {tabIndex !== null && (
        <Tabs defaultIndex={tabIndex}>
          <div className="tabbed-content__dropdown">
            {windowSize < 700 && (
              <>
                <ul className="teaser-container__top teaser-container__top--mobile">
                  <li className="teaser__title">
                    <h2>The Centre</h2>
                  </li>
                </ul>
                <div className="tabbed-content__dropdown-button">
                  <button onClick={toggleDropdown}>
                    More Info{" "}
                    <div
                      className={
                        "tabbed-content__dropdown-button-icon" +
                        (showDropDown ? " rotate" : "")
                      }
                    >
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="11"
                          height="5"
                        >
                          <path d="M5.5 5L0 0h11z" data-name="Polygon 9"></path>
                        </svg>
                      </span>
                    </div>
                  </button>
                </div>
              </>
            )}
            <TabList
              className={
                "teaser-container__top teaser-container__top--tabs " +
                (showDropDown ? showDropDownClass : hideDropDownClass)
              }
            >
              {windowSize > 700 && (
                <li className="react-tabs__tab react-tabs__tab--title teaser__title">
                  <h2>The Centre</h2>
                </li>
              )}
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/centre-map" },
                    "",
                    "/the-centre/centre-map"
                  );
                  closeDropDown();
                }}
              >
                <h3>Centre Map</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/opening-hours" },
                    "",
                    "/the-centre/opening-hours"
                  );
                  closeDropDown();
                }}
              >
                <h3>Opening Hours</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/about-us" },
                    "",
                    "/the-centre/about-us"
                  );

                  closeDropDown();
                }}
              >
                <h3>About us</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/directions" },
                    "",
                    "/the-centre/directions"
                  );

                  closeDropDown();
                }}
              >
                <h3>How to get here</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/parking" },
                    "",
                    "/the-centre/parking"
                  );
                  closeDropDown();
                }}
              >
                <h3>Parking</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/sustainability" },
                    "",
                    "/the-centre/sustainability"
                  );

                  closeDropDown();
                }}
              >
                <h3>Sustainability</h3>
              </Tab>
              <Tab
                onClick={() => {
                  history.pushState(
                    { urlPath: "/the-centre/click-and-collect" },
                    "",
                    "/the-centre/click-and-collect"
                  );
                  closeDropDown();
                }}
              >
                <h3>Click&amp;Collect</h3>
              </Tab>
            </TabList>
          </div>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--about-us">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        src={props.data.centre_map.image.url}
                        style={{ width: "100%", height: "auto" }}
                        alt={props.data.centre_map.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <div className="teaser-content__inside">
                      {parse(centreMapContent)}
                      {props.data.centre_map.downloadable_pdf &&
                      props.data.centre_map.downloadable_pdf.pdf_value ? (
                        <div className="teaser__pdf">
                          <a
                            href={
                              props.data.centre_map.downloadable_pdf.pdf.url
                            }
                            download
                          >
                            {props.data.centre_map.downloadable_pdf.message}
                          </a>
                        </div>
                      ) : null}
                    </div>
                  </div>

                  {props.data.general.information_blocks &&
                    props.data.general.information_blocks.map((info, index) => (
                      <div className={`teaser-content--fourth item-${index}`}>
                        <h2 className="teaser__subject">{info.title}</h2>
                        <div className="teaser-content__inside">
                          <p className="teaser__description">
                            {parse(info.content)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--opening-hours">
              <div className="container teaser-container">
                <div className="teaser-content">
                  {props.data.general.image && (
                    <div className="teaser-content--half">
                      <div className="teaser__img">
                        <img
                          src={props.data.general.image.url}
                          style={{ width: "100%", height: "auto" }}
                          alt={props.data.general.image.alt}
                        />
                      </div>
                    </div>
                  )}

                  <div className="teaser-content--half">
                    {parse(bentallStoreText)}
                    <div className="teaser__span-container">
                      <table className="opening-times">
                        <tbody>
                          {openingHours &&
                            openingHours.map((opening, index) => {
                              return (
                                <tr key={`opening-item-${index}`}>
                                  <td
                                    style={{
                                      textTransform: "capitalize",
                                      fontSize: "18px",
                                    }}
                                  >
                                    {opening.day === today
                                      ? "Today"
                                      : opening.day}
                                  </td>
                                  <td style={{ fontSize: "18px" }}>
                                    {!opening.isClosed
                                      ? moment(
                                          new Date("1970-01-01T" + opening.open)
                                        )
                                          .format("h:mma")
                                          .replace(":00", "") +
                                        "-" +
                                        moment(
                                          new Date(
                                            "1970-01-01T" + opening.close
                                          )
                                        )
                                          .format("h:mma")
                                          .replace(":00", "")
                                      : "CLOSED"}
                                  </td>
                                </tr>
                              );
                            })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="teaser-content--full-width">
                    <Map />
                  </div>

                  {props.data.general.information_blocks &&
                    props.data.general.information_blocks.map((info, index) => (
                      <div className={`teaser-content--fourth item-${index}`}>
                        <h2 className="teaser__subject">{info.title}</h2>
                        <div className="teaser-content__inside">
                          <p className="teaser__description">
                            {parse(info.content)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--about-us">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        src={props.data.about_us.image.url}
                        style={{ width: "100%", height: "auto" }}
                        alt={props.data.about_us.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <div className="teaser-content__inside">
                      {parse(aboutUsContent)}
                    </div>
                  </div>

                  {props.data.general.information_blocks &&
                    props.data.general.information_blocks.map((info, index) => (
                      <div className={`teaser-content--fourth item-${index}`}>
                        <h2 className="teaser__subject">{info.title}</h2>
                        <div className="teaser-content__inside">
                          <p className="teaser__description">
                            {parse(info.content)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--getting-here">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        style={{ width: "100%", height: "auto" }}
                        src={props.data.directions.image.url}
                        alt={props.data.directions.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <div className="teaser-content__inside">
                      {parse(directionsContent)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--parking">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        src={props.data.parking.image.url}
                        style={{ width: "100%", height: "auto" }}
                        alt={props.data.parking.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    {parse(parkingContent)}

                    {props.data.parking.spaces.length > 0 && (
                      <>
                        <h2 className="teaser__subject">Car Park Spaces</h2>{" "}
                        {props.data.parking.spaces.map((space) => (
                          <div className="flex flex--align-center parking-icons">
                            {space.icon && space.icon !== "" && (
                              <img
                                src={space.icon}
                                alt=""
                                style={{ marginRight: "15px" }}
                              />
                            )}

                            {parse(space.content)}
                          </div>
                        ))}
                      </>
                    )}
                  </div>
                  <div className="teaser-content--half">
                    <h2 className="teaser__subject">Opening hours</h2>
                    <div className="teaser-content__inside">
                      {props.data.parking &&
                        props.data.parking.opening_hours &&
                        props.data.parking.opening_hours.length > 0 &&
                        props.data.parking.opening_hours.map((opening) => {
                          if (!opening || !opening.name) return;
                          return (
                            <>
                              <table className="table table--three-col">
                                <tbody>
                                  <tr>
                                    <th colspan="3">{parse(opening.name)}</th>
                                  </tr>
                                  {opening.list.map((item, index) => (
                                    <>
                                      <tr
                                        key={`${opening.name}item-item-${index}`}
                                      >
                                        {item.day && <td>{item.day}</td>}
                                        {item.times && <td>{item.times}</td>}
                                        {item.note && (
                                          <td className="grey">{item.note}</td>
                                        )}
                                      </tr>
                                    </>
                                  ))}
                                </tbody>
                              </table>
                              {opening.footnote && opening.footnote !== "" && (
                                <p className="teaser__description grey">
                                  {opening.footnote}
                                </p>
                              )}
                            </>
                          );
                        })}
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <h2 className="teaser__subject">Parking Tariffs</h2>
                    <div className="teaser-content__inside">
                      <div className="flex">
                        {props.data.parking &&
                          props.data.parking.tariffs &&
                          props.data.parking.tariffs.length > 0 &&
                          props.data.parking.tariffs.map((tariff) => {
                            if (!tariff || !tariff.name) return;
                            return (
                              <table
                                key={tariff.name}
                                className="table--parking"
                              >
                                <tbody>
                                  <tr>
                                    <th colspan="3">{parse(tariff.name)}</th>
                                  </tr>
                                  {tariff.list.map((item) => (
                                    <tr>
                                      <td>{parse(item.hours)}</td>
                                      <td>{parse(item.cost)}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </table>
                            );
                          })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--sustainability">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        src={props.data.sustainability.image.url}
                        style={{ width: "100%", height: "auto" }}
                        alt={props.data.sustainability.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    {parse(sustainabilityContent)}
                  </div>

                  {props.data.general.information_blocks &&
                    props.data.general.information_blocks.map((info, index) => (
                      <div className={`teaser-content--fourth item-${index}`}>
                        <h2 className="teaser__subject">{info.title}</h2>
                        <div className="teaser-content__inside">
                          <p className="teaser__description">
                            {parse(info.content)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </TabPanel>

          <TabPanel>
            <div className="tabbed-content__teasers tabbed-content__teasers--click-collect">
              <div className="container teaser-container">
                <div className="teaser-content">
                  <div className="teaser-content--half">
                    <div className="teaser__img">
                      <img
                        src={props.data.click_collect.image.url}
                        style={{ width: "100%", height: "auto" }}
                        alt={props.data.click_collect.image.alt}
                      />
                    </div>
                  </div>
                  <div className="teaser-content--half">
                    <div className="teaser-content__inside">
                      {parse(clickCollectContent)}
                    </div>
                  </div>

                  {props.data.general.information_blocks &&
                    props.data.general.information_blocks.map((info, index) => (
                      <div className={`teaser-content--fourth item-${index}`}>
                        <h2 className="teaser__subject">{info.title}</h2>
                        <div className="teaser-content__inside">
                          <p className="teaser__description">
                            {parse(info.content)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </TabPanel>
        </Tabs>
      )}
    </section>
  );
};

export default TabbedCentres;
