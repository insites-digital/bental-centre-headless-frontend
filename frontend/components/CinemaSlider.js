import React from "react";
import Swiper from "react-id-swiper";
import blackArrow from "../static/images/general/arrow-black.svg";
const CinemaSlider = () => {
  const params = {
    slidesPerView: "auto",
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };
  return (
    <section className="container">
      <div className="teaser-container">
        <div className="teaser-container__top">
          <div className="teaser__title">
            <h2>Today at Curzon Cinema</h2>
          </div>

          <div className="teaser__view-more">
            <a className="link link--uppercase link--arrow" href="#">
              Cinema
              <img src={blackArrow} alt="" />
            </a>
          </div>
        </div>
        <div
          className="teaser-content__container"
          // style={{
          //   cursor: "url(/static/images/site/drag.svg), auto",
          // }}
        >
          <Swiper {...params}>
            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>

            <div className="teaser-content teaser-content--medium teaser-content--cinema">
              <div className="teaser__img">
                <img
                  src="https://www.bentallcentre.co.uk/wp-content/uploads/2020/03/MW-Rings-555x375.jpg"
                  alt=""
                />
              </div>

              <div className="teaser-content__inside">
                <h2 className="teaser__subject">Maison Du Mezze</h2>

                <p className="teaser__description">
                  Join us at Maison du Mezzé to experience authentic Middle
                  Eastern dining and hospitality.
                </p>

                <div className="teaser__tag-container">
                  <a href="#" className="teaser__tag">
                    11:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    12:00AM
                  </a>
                  <a href="#" className="teaser__tag">
                    1:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    3:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    4:00PM
                  </a>
                  <a href="#" className="teaser__tag">
                    6:00PM
                  </a>
                </div>
              </div>
            </div>
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default CinemaSlider;
