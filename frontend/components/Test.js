import React, { Component } from "react";
import FlickitySlider from "../components/FlickitySlider";

const images = ["hello1", "hello2", "hello3", "hello 4"];

export default class Test extends Component {
  render() {
    return (
      <div style={{ height: "600px" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }} />
        <FlickitySlider
          options={{
            autoPlay: 4000,
            pauseAutoPlayOnHover: true,
            wrapAround: true,
            fullscreen: true,
            adaptiveHeight: true,
          }}
        >
          {images.map((image, index) => (
            <div
              style={{ width: "80%", height: "400px", margin: "0 0.5em" }}
              key={index}
            >
              <p>{image}</p>
            </div>
          ))}
        </FlickitySlider>
      </div>
    );
  }
}
