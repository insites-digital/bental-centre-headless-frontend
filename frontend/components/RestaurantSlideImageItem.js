import React from "react";
import Image from "./Image";

const RestaurantSlideItem = (props) => {
  if (!props.hasOwnProperty("data")) return;
  let hoverSource = "";

  if (props.data.acf.image.hover) {
    hoverSource =
      props.data.acf.image.hover.sizes.xsmall_33vw +
      " 640w, " +
      props.data.acf.image.hover.sizes.xsmall_33vw +
      " 768w, " +
      props.data.acf.image.hover.sizes.small_33vw +
      " 1024w, " +
      props.data.acf.image.hover.sizes.medium_33vw +
      " 1366w, " +
      props.data.acf.image.hover.sizes.large_33vw +
      " 1600w, " +
      props.data.acf.image.hover.sizes.xlarge_33vw +
      " 1920w ";
  }
  return (
    <div className="teaser__img teaser__img--restaurant">
      <Image data={props.data.acf.image} imageSize={"33vw"} />
      {props.data.acf.image.hover && (
        <img
          className="hover"
          src={props.data.acf.image.hover.sizes.medium_33vw}
          srcSet={hoverSource}
          alt=""
        />
      )}
    </div>
  );
};

export default RestaurantSlideItem;
