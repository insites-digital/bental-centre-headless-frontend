import React, { useState } from "react";
import GoogleMapReact from "google-map-react";

const AnyReactComponent = ({ text, subText, address, icon }) => {
  const [showPopup, setShowPopup] = useState(false);
  return (
    <>
      {/* <div
        className="marker-popup"
        style={{
          position: "absolute",
          visibility: showPopup ? "" : "hidden",
          zIndex: "99999",
          width: "150px",
          padding: "20px 15px 10px 15px",
          background: "white",
          left: "-50%",
          transform: "translateX(-50%)",
          cursor: "pointer",
          boxShadow: "2px 2px 8px rgba(0, 0, 0, 0.25)",
        }}
      >
        <span
          onClick={() => setShowPopup(!showPopup)}
          style={{ position: "absolute", top: "5px", left: "5px" }}
        >
          <img src="/static/images/black-cross.svg" width="10px" />
        </span>
        {address}
      </div>
       */}
      <div
        className="marker"
        onClick={() => setShowPopup(!showPopup)}
        style={{
          position: "absolute",
          width: "200px",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          zIndex: "999",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          fontSize: "12px",
        }}
      >
        <img
          width="20"
          height="auto"
          src={
            icon === "park"
              ? "/static/images/parking.svg"
              : "/static/images/pin.svg"
          }
          style={{ marginBottom: "5px", display: "block" }}
        />
        <span>{text}</span>
        {subText && (
          <>
            <span style={{ fontSize: "8px" }}>{subText}</span>
          </>
        )}
      </div>
    </>
  );
};

const Map = () => {
  //const center = { lat: 51.41267, lng: -0.30368 };
  const center = { lat: 51.41260664985392, lng: -0.30523188465215956 };
  const zoom = 17;
  const mapStyles = [
    {
      featureType: "all",
      elementType: "labels.text.fill",
      stylers: [
        {
          saturation: 36,
        },
        {
          color: "#333333",
        },
        {
          lightness: 40,
        },
      ],
    },
    {
      featureType: "all",
      elementType: "labels.text.stroke",
      stylers: [
        {
          visibility: "on",
        },
        {
          color: "#ffffff",
        },
        {
          lightness: 16,
        },
      ],
    },
    {
      featureType: "all",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "administrative",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#fefefe",
        },
        {
          lightness: 20,
        },
      ],
    },
    {
      featureType: "administrative",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#fefefe",
        },
        {
          lightness: 17,
        },
        {
          weight: 1.2,
        },
      ],
    },
    {
      featureType: "landscape",
      elementType: "geometry",
      stylers: [
        {
          color: "#f5f5f5",
        },
        {
          lightness: 20,
        },
      ],
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [
        {
          color: "#dedede",
        },
        {
          lightness: 21,
        },
        {
          visibility: "on",
        },
      ],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 17,
        },
      ],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 29,
        },
        {
          weight: 0.2,
        },
      ],
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 18,
        },
      ],
    },
    {
      featureType: "road.local",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 16,
        },
      ],
    },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [
        {
          color: "#f2f2f2",
        },
        {
          lightness: 19,
        },
      ],
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        {
          color: "#e9e9e9",
        },
        {
          lightness: 17,
        },
      ],
    },
  ];

  const mapOptions = {
    styles: mapStyles,
    gestureHandling: "greedy",
    scrollwheel: false,
    minZoom: 17,
    maxZoom: 19,
  };
  return (
    <div style={{ height: "466px", width: "100%" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyAJqFBjnyCUHzG6W4NVCBEwX9S3Xn6vzG0" }}
        defaultCenter={center}
        defaultZoom={zoom}
        options={mapOptions}
        yesIWantToUseGoogleMapApiInternals
      >
        <AnyReactComponent
          lat={51.4118}
          lng={-0.3046}
          text="The Bentall Centre"
          // subText={"Shopping Centre"}
          icon="shop"
          address={
            <>
              The Bentall Centre
              <br />
              Kingston upon Thames
              <br />
              KT1 1TP
              <br />
              United Kingdom
            </>
          }
        />
        <AnyReactComponent
          lat={51.4123}
          lng={-0.3046}
          text={"Bentalls Department Store"}
          // subText={"Department Store"}
          icon="shop"
          address={
            <>
              Bentalls Wood St <br /> Kingston upon Thames <br /> KT1 1TX <br />{" "}
              United Kingdom
            </>
          }
        />

        <AnyReactComponent
          //lat={51.4133474}
          lat={51.41375}
          lng={-0.3048}
          text={"Seven Kings Car Park"}
          // subText={"Car Park"}
          icon="park"
          address={
            <>
              Bentalls Wood St <br /> Kingston upon Thames <br /> KT1 1TX <br />{" "}
              United Kingdom
            </>
          }
        />

        <AnyReactComponent
          lat={51.4128739}
          lng={-0.3060297}
          text={"Car Park A & B"}
          // subText={"Car Park"}
          icon="park"
          address={
            <>
              Bentalls Wood St <br /> Kingston upon Thames <br /> KT1 1TX <br />{" "}
              United Kingdom
            </>
          }
        />
      </GoogleMapReact>
    </div>
  );
};

export default Map;
