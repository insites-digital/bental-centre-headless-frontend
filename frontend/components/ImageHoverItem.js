import React from "react";
import Image from "./Image";

const ImageHoverItem = (props) => {
  if (!props.hasOwnProperty("data")) return;
  let hoverSource = "";

  if (props.data.acf.image.hover) {
    hoverSource =
      props.data.acf.image.hover.sizes[
        `xsmall_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 640w, " +
      props.data.acf.image.hover.sizes[
        `xsmall_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 768w, " +
      props.data.acf.image.hover.sizes[
        `small_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 1024w, " +
      props.data.acf.image.hover.sizes[
        `medium_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 1366w, " +
      props.data.acf.image.hover.sizes[
        `large_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 1600w, " +
      props.data.acf.image.hover.sizes[
        `xlarge_${props.size === "100vw" ? "50vw" : props.size}`
      ] +
      " 1920w ";
  }
  return (
    <div className={`teaser__img ${props.className}`}>
      {/* <Image data={props.data.acf.image} imageSize={props.size} /> */}
      <img
        src={props.data.acf.image.url.sizes.large_50vw}
        alt={props.data.acf.image.url.alt}
      />

      {props.data.acf.image.hover && (
        <img
          className="hover"
          src={
            props.data.acf.image.hover.sizes[
              `medium_${props.size === "100vw" ? "50vw" : props.size}`
            ]
          }
          srcSet={hoverSource}
          alt={props.data.acf.image.url.alt}
        />
      )}
    </div>
  );
};

export default ImageHoverItem;
