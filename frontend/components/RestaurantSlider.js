import React, { useEffect, useState, useRef } from "react";
import Swiper from "react-id-swiper";
import blackArrow from "../static/images/general/arrow-black.svg";
import Link from "next/link";
import parse from "html-react-parser";
import { getSlug } from "../utils/getSlug";
import Tag from "./Tag";
import OpenStatus from "./OpenStatus";
import { truncate } from "../utils/truncate";
import { decodeString } from "../utils/decodeString";
import RestaurantSlideItem from "./RestaurantSlideImageItem.js";
import he from "he";

const RestaurantSlider = (props) => {
  const [windowSize, setWindowSize] = useState(0);
  const [selectedRestaurants, setSelectedRestaurants] = useState(null);
  const [swiper, setSwiper] = useState(null);
  const [slidePos, setSlidePos] = useState(0);
  const slider = useRef(null);
  const [sliderRef, setSliderRef] = useState(null);

  if (!props.data || props.data.length < 1) return <></>;
  const params = {
    slidesPerView: "auto",
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  useEffect(() => {
    const setup = async () => {
      const restaurants = props.data;
      let randomRestaurants;

      if (restaurants.length <= 9) {
        randomRestaurants = restaurants;
      } else {
        let indexes = [];
        indexes.push(getRandomInt(9));

        const getIndexes = () => {
          if (indexes.length === 9) return;
          let randomIndex = getRandomInt(9);
          if (indexes.indexOf(randomIndex) < 0) {
            return indexes.push(randomIndex);
          } else {
            getIndexes();
          }
        };

        for (let i = 1; i <= 9; i++) {
          await getIndexes();
        }
        randomRestaurants = indexes.map((index) => {
          return restaurants[index];
        });
      }

      setSelectedRestaurants(randomRestaurants);
    };
    setup();
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
      resetImageHeight();
      setImageHeights();
    });

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  const resetImageHeight = () => {
    const image = document.querySelector(".teaser__img--restaurant");
    if (!image) return;

    image.style.paddingTop = "";

    const images = document.querySelectorAll(".teaser__img--restaurant");
    images.forEach((image) => {
      image.style.paddingTop = "";
    });
  };

  const setImageHeights = () => {
    const images = document.querySelectorAll(".teaser__img--restaurant");
    if (images.length > 0 && windowSize >= 1366) {
      const height = document.querySelector(".teaser__img--restaurant img")
        .offsetHeight;

      images.forEach((image) => {
        image.style.paddingTop = height + "px";
      });
    }
  };

  useEffect(() => {
    setImageHeights();
    // Position the pagination arrows halway down the teaser image.
    if (!sliderRef || !sliderRef.current) return;
    const paginationPrev = sliderRef.current.querySelector(
      ".swiper-button-prev"
    );
    const paginationNext = sliderRef.current.querySelector(
      ".swiper-button-next"
    );
    const slide = sliderRef.current.querySelector(".swiper-slide");
    const padding = parseInt(
      window
        .getComputedStyle(slide, null)
        .getPropertyValue("padding-top")
        .replace("px", "")
    );
    const teaserImg = slide.querySelector(".teaser__img");
    const teaserImgHeight = teaserImg.getBoundingClientRect().height + padding;
    const topPos = teaserImgHeight + "px";
    const marginTop = "-" + teaserImgHeight / 2 + "px";
    paginationPrev.style.top = topPos;
    paginationNext.style.top = topPos;
    paginationPrev.style.marginTop = marginTop;
    paginationNext.style.marginTop = marginTop;
  }, [sliderRef]);

  useEffect(() => {
    if (!swiper) return;
    swiper.on("transitionEnd", () => {
      setSlidePos(swiper.activeIndex);
    });

    // swiper.on("transitionStart", () => {
    //   const swipers = document.querySelectorAll(".teaser-content--restaurant");
    //   if (swipers[swipers.length - 3])
    //     swipers[swipers.length - 3].classList.remove("active-slide");
    // });

    // swiper.on("reachEnd", () => {
    //   setTimeout(() => {
    //     const activeSwiper = document.querySelector(
    //       ".restaurant-slider .swiper-slide-active"
    //     );
    //     const nextSwiper = document.querySelector(
    //       ".restaurant-slider .swiper-slide-next"
    //     );
    //     const thirdSwiper = document.querySelector(
    //       ".restaurant-slider .swiper-slide-next + .swiper-slide"
    //     );
    //     if (activeSwiper && nextSwiper && thirdSwiper) {
    //       activeSwiper.classList.remove("swiper-slide-active");
    //       activeSwiper.classList.remove("active-slide");
    //       nextSwiper.classList.remove("swiper-slide-next");
    //       nextSwiper.classList.add("swiper-slide-active");
    //       nextSwiper.classList.add("active-slide");
    //       thirdSwiper.classList.add("swiper-slide-next");
    //     }
    //   }, 100);
    // });
  }, [swiper]);

  if (!selectedRestaurants) return <></>;

  return (
    <section className="restaurant-slider__container container">
      <div
        className="teaser-container restaurant-slider"
        ref={(el) => {
          slider.current = el;
          if (slider.current) {
            setSliderRef(slider);
          }
        }}
      >
        <div className="teaser-container__top">
          <div className="teaser__title">
            <h2>Food & Drink</h2>
          </div>

          {props.componentData.hasOwnProperty("link") &&
            props.componentData.link.hasOwnProperty("url") &&
            props.componentData.link.hasOwnProperty("label") &&
            props.componentData.link.url !== null &&
            props.componentData.link.label !== "" && (
              <div className="teaser__view-more">
                {/* <a
                  className="link link--uppercase link--arrow"
                  href={getSlug(props.componentData.link.url)}
                >
                  {props.componentData.link.label}
                  <img src={blackArrow} alt="" />
                </a> */}

                <a
                  className="link link--uppercase link--arrow"
                  href={getSlug(props.componentData.link.url)}
                >
                  <span style={{ display: "inline-block" }}>
                    {props.componentData.link.label}
                  </span>{" "}
                  <span className="arrow">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="29.789"
                      height="8.21"
                    >
                      <g data-name="Group 1359">
                        <path
                          data-name="Path 759"
                          d="M0 4.105h20.186"
                          stroke="#000"
                          strokeWidth=".5"
                        />
                        <path
                          data-name="Polygon 1"
                          d="M29.789 4.1L18.842 8.2V0z"
                        />
                      </g>
                    </svg>
                  </span>
                </a>
              </div>
            )}
        </div>
        <div className="teaser-content__container">
          <Swiper getSwiper={setSwiper} {...params}>
            {selectedRestaurants.map((restaurant, index) => {
              let floor = null;
              if (
                restaurant &&
                restaurant.hasOwnProperty("floor") &&
                restaurant.floor.length > 0 &&
                props.floors
              ) {
                floor = props.floors.filter((floor) => {
                  if (!restaurant.floor) return false;
                  return restaurant.floor.includes(floor.id);
                });
              }
              let categories = null;
              if (
                restaurant &&
                restaurant["restaurant-category"]?.length > 0 &&
                props.restaurantCats
              ) {
                categories = props.restaurantCats.filter((cat) => {
                  if (!restaurant["restaurant-category"]) return false;
                  return restaurant["restaurant-category"].includes(cat.id);
                });
              }

              return (
                <div
                  className={
                    "teaser-content teaser-content--restaurant" +
                    (index === selectedRestaurants.length - 2
                      ? " second-to-last"
                      : "") +
                    (index === slidePos ? " active-slide" : "")
                  }
                  key={`restaurant-${index}`}
                  onMouseOver={(e) => {}}
                >
                  <Link
                    as={`${getSlug(restaurant.link)}`}
                    href={`${getSlug(restaurant.link)}`}
                  >
                    <a
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        height: "100%",
                        width: "100%",
                      }}
                    >
                      <RestaurantSlideItem data={restaurant} />

                      <OpenStatus openingHours={restaurant.acf.opening_hours} />

                      <div
                        className="teaser-content__inside"
                        style={{ flexGrow: "1" }}
                      >
                        <h2 className="teaser__subject">
                          {parse(restaurant.title.rendered)}
                        </h2>

                        <div className="teaser__description">
                          {truncate.apply(
                            decodeString(
                              he.decode(restaurant.content.rendered)
                            ),
                            [200, true]
                          )}
                        </div>
                      </div>

                      <div className="teaser__tag-container">
                        {floor &&
                          floor.map((item) => (
                            <Tag
                              key={`floor-${item.id}`}
                              colour={item.acf.colour}
                              title={item && item.name ? item.name : ""}
                            />
                          ))}
                        {categories &&
                          categories.map((category) => (
                            <Tag
                              key={`category-${category.id}`}
                              colour={category.acf.colour}
                              title={
                                category && category.name ? category.name : ""
                              }
                            />
                          ))}
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default RestaurantSlider;
