import React, { useState, useEffect, useRef } from "react";
import Swiper from "react-id-swiper";
import blackArrow from "../static/images/general/arrow-black.svg";
import parse from "html-react-parser";
import Link from "next/link";
import { getSlug } from "../utils/getSlug";
import "moment-timezone";
import moment from "moment";
import Tag from "./Tag";
import Image from "./Image";

const WhatsOn = (props) => {
  const [windowSize, setWindowSize] = useState(0);
  const events = props.events;
  const eventCats = props.eventCats;
  const slider = useRef(null);
  const [sliderRef, setSliderRef] = useState(null);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  useEffect(() => {
    // Position the pagination arrows halway down the teaser image.
    if (!sliderRef || !sliderRef.current) return;

    const paginationPrev = sliderRef.current.querySelector(
      ".swiper-button-prev"
    );
    const paginationNext = sliderRef.current.querySelector(
      ".swiper-button-next"
    );
    const slide = sliderRef.current.querySelector(".swiper-slide");
    const padding = parseInt(
      window
        .getComputedStyle(slide, null)
        .getPropertyValue("padding-top")
        .replace("px", "")
    );

    const teaserImg = slide.querySelector(".teaser__img");
    const teaserImgHeight = teaserImg.getBoundingClientRect().height + padding;
    const topPos = teaserImgHeight + "px";
    const marginTop = "-" + teaserImgHeight / 2 + "px";
    paginationPrev.style.top = topPos;
    paginationNext.style.top = topPos;
    paginationPrev.style.marginTop = marginTop;
    paginationNext.style.marginTop = marginTop;
  }, [sliderRef]);

  if (!events || events.length < 1 || !eventCats || eventCats.length < 1)
    return <></>;

  const params = {
    slidesPerView: "auto",
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  const eventCategoryIds = props.data.topic;

  const matchedEvents = events.filter((item) => {
    if (!item["event-category"] || !eventCategoryIds) return false;
    return item["event-category"].some((r) => eventCategoryIds.indexOf(r) >= 0);
  });

  if (matchedEvents.length === 0) return <></>;

  return (
    <section className="container whatson">
      <div className="teaser-container">
        <div className="teaser-container__top">
          <div className="teaser__title">
            <h2>What's On</h2>
          </div>

          {props.data.hasOwnProperty("link") &&
            props.data.link.hasOwnProperty("url") &&
            props.data.link.hasOwnProperty("label") &&
            props.data.link.url !== null &&
            props.data.link.label !== "" && (
              <div className="teaser__view-more">
                {/* <a
                  className="link link--uppercase link--arrow"
                  href={getSlug(props.data.link.url)}
                >
                  {props.data.link.label}
                  <img src={blackArrow} alt="" />
                </a> */}

                <a
                  className="link link--uppercase link--arrow"
                  href={getSlug(props.data.link.url)}
                >
                  <span style={{ display: "inline-block" }}>
                    {props.data.link.label}
                  </span>{" "}
                  <span className="arrow">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="29.789"
                      height="8.21"
                    >
                      <g data-name="Group 1359">
                        <path
                          data-name="Path 759"
                          d="M0 4.105h20.186"
                          stroke="#000"
                          strokeWidth=".5"
                        />
                        <path
                          data-name="Polygon 1"
                          d="M29.789 4.1L18.842 8.2V0z"
                        />
                      </g>
                    </svg>
                  </span>
                </a>
              </div>
            )}
        </div>
        <div
          className="teaser-content__container teaser-content__container--whats-on"
          ref={(el) => {
            slider.current = el;
            if (slider.current) {
              setSliderRef(slider);
            }
          }}
        >
          <Swiper {...params}>
            {matchedEvents.map((event, index) => {
              const tags = props.eventCats.filter((eventCat) => {
                if (!event["event-category"]) return false;
                return event["event-category"].includes(eventCat.id);
              });
              const splitDate = (str) => str.split("/");
              const formattedDate = new Date(
                `${
                  splitDate(event.acf.dates.start_date)[1] +
                  "/" +
                  splitDate(event.acf.dates.start_date)[0] +
                  "/" +
                  splitDate(event.acf.dates.start_date)[2]
                }`
              );
              const formattedEndDate = new Date(
                `${
                  splitDate(event.acf.dates.end_date)[1] +
                  "/" +
                  splitDate(event.acf.dates.end_date)[0] +
                  "/" +
                  splitDate(event.acf.dates.end_date)[2]
                }`
              );
              var today = new Date();
              var dd = String(today.getDate()).padStart(2, "0");
              var mm = String(today.getMonth() + 1).padStart(2, "0");
              var yyyy = today.getFullYear();

              today = mm + "/" + dd + "/" + yyyy;
              moment().tz("Europe/London");
              if (!formattedEndDate) return <></>;
              const dateHasPassed = moment(formattedEndDate).isBefore(today);

              if (dateHasPassed) return <></>;

              const startDateHasPassed = moment(formattedDate).isBefore(today);

              function getdateFormated(date) {
                var a = moment(today);
                var b = moment(date);

                var days = b.diff(a, "days");
                var weeks = Math.round(b.diff(a, "week", true));

                var calback = function () {
                  if (weeks === 0) {
                    return "[In " + days + " days]";
                  } else if (weeks === 1) {
                    return "[Next Week]";
                  } else {
                    return `[In ${weeks} weeks]`;
                  }
                };
                return moment(date).calendar(null, {
                  sameDay: "[Today]",
                  nextDay: "[Tomorrow]",
                  nextWeek: calback,
                  sameElse: calback,
                });
              }

              const dateOverview = startDateHasPassed
                ? "today"
                : getdateFormated(formattedDate);

              let endsOnSameDay = false;

              let startDayArray = event.acf.dates.start_date.split("/");
              let endDayArray = event.acf.dates.end_date.split("/");
              if (startDayArray[1] === endDayArray[1]) {
                if (startDayArray[0] === endDayArray[0]) {
                  endsOnSameDay = true;
                }
              }

              return (
                <div
                  className={`teaser-content teaser-content--portrait`}
                  key={`whats-on-teaser-content-${index}`}
                  style={{
                    borderColor: index === 0 ? "transparent" : "",
                  }}
                >
                  <Link
                    as={`${getSlug(event.link)}`}
                    href={`${getSlug(event.link)}`}
                  >
                    <a
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        height: "100%",
                        width: "100%",
                      }}
                    >
                      <div className="teaser__img">
                        <Image data={event.acf.image} imageSize="33vw" />
                      </div>

                      <div
                        className="teaser-content__inside"
                        style={{ flexGrow: "1" }}
                      >
                        <div className="teaser__span-container teaser__span-container--date">
                          <span className="teaser__span">{dateOverview}</span>
                        </div>

                        <h2 className="teaser__subject">
                          {parse(event.title.rendered)}
                        </h2>
                        <h3 className="teaser__date">
                          {event.acf.dates.start_date}
                          {!endsOnSameDay && <> - {event.acf.dates.end_date}</>}
                        </h3>
                      </div>
                      <div className="teaser__tag-container">
                        {tags.map((tagItem, index) => {
                          if (index > 3 || !tagItem || !tagItem.name)
                            return <></>;
                          return (
                            <Tag
                              key={tagItem + tagItem.name}
                              colour={tagItem.acf.colour}
                              title={tagItem.name}
                            />
                          );
                        })}
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default WhatsOn;
