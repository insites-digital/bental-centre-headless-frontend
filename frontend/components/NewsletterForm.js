import React from "react";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import { createCookie } from "../utils/createCookie";
import Link from "next/link";

// const MAILCHIMP_POST_URL = `https://insitesdigital.us18.list-manage.com/subscribe/post?
//   u=bdfd90868a8757db8c2d4483d&amp;id=ea98ba2f7a`;

const MAILCHIMP_POST_URL = `https://bentallcentre.us1.list-manage.com/subscribe/post?u=a8ce5f4075483f90ece44a3d3&amp;id=29080c8de0`;

const EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
const NAME_REGEX = /^[A-Za-z]+$/i;

class NewsletterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {
        firstName: false,
        lastName: false,
        email: false,
        terms: false,
      },
      terms: false,
      showTerms: false,
      raiseSubmitToParent: this.props.hasOwnProperty("data")
        ? this.props.data
        : null,
    };

    // create references to grab values from each field
    this.emailRef = React.createRef(undefined);
    this.fNameRef = React.createRef(undefined);
    this.lNameRef = React.createRef(undefined);
  }

  onType = () => {
    if (
      this.fNameRef.current.value !== "" &&
      this.lNameRef.current.value !== "" &&
      this.emailRef.current.value !== ""
    ) {
      this.setState({ showTerms: true });
    } else {
      this.setState({ showTerms: false });
    }
  };

  onSubmit = (e, subscribe) => {
    e.preventDefault();

    this.setState(
      {
        errors: {
          terms: this.state.terms ? false : true,
        },
      },
      () => {
        if (this.emailRef !== undefined) {
          this.setState({
            errors: {
              firstName: !NAME_REGEX.test(this.fNameRef.current.value),
              lastName: !NAME_REGEX.test(this.lNameRef.current.value),
              email: !EMAIL_REGEX.test(this.emailRef.current.value),
              terms: this.state.terms ? false : true,
            },
          });
        }
        const values = Object.values(this.state.errors);

        // if there aren't any errors, go ahead and subscribe
        if (
          Object.values(this.state.errors) &&
          !Object.values(this.state.errors).includes(true)
        ) {
          createCookie("newsletterSignedUp", "true", 60);

          // sends submit message to parent to remove popup
          if (this.state.raiseSubmitToParent) this.state.raiseSubmitToParent();

          subscribe({
            EMAIL: this.emailRef.current.value,
            FNAME: this.fNameRef.current.value,
            LNAME: this.lNameRef.current.value,
          });
        }
      }
    );
  };

  render() {
    return (
      <MailchimpSubscribe
        url={MAILCHIMP_POST_URL}
        render={({ subscribe, status, message }) => {
          switch (status) {
            case "success":
              return (
                <div className="newsletter-form__success">
                  <p>Success! You subscribed to the mailing list.</p>
                </div>
              );
            default:
              return (
                <form
                  className="newsletter-form"
                  onSubmit={(e) => {
                    this.onSubmit(e, subscribe);
                  }}
                >
                  <div
                    className={
                      "newsletter-form__field " +
                      (this.state.errors.firstName ? "error-found" : "")
                    }
                  >
                    <input
                      name="firstName"
                      placeholder="Forename"
                      onKeyUp={() => {
                        this.onType();
                      }}
                      className={
                        this.state.errors.firstName ? "error-found" : ""
                      }
                      ref={this.fNameRef}
                    />
                  </div>
                  <div
                    className={
                      "newsletter-form__field " +
                      (this.state.errors.lastName ? "error-found" : "")
                    }
                  >
                    <input
                      name="lastName"
                      onKeyUp={() => {
                        this.onType();
                      }}
                      placeholder="Surname"
                      className={
                        this.state.errors.lastName ? "error-found" : ""
                      }
                      ref={this.lNameRef}
                    />
                  </div>
                  <div
                    className={
                      "newsletter-form__field " +
                      (this.state.errors.email ? "error-found" : "")
                    }
                  >
                    <input
                      name="email"
                      onKeyUp={() => {
                        this.onType();
                      }}
                      placeholder="Email"
                      ref={this.emailRef}
                      className={this.state.errors.email ? "error-found" : ""}
                    />
                  </div>

                  {this.state.showTerms && status !== "error" && (
                    <div
                      className={
                        "opt-in " +
                        (this.state.errors.terms ? "opt-in--error" : "")
                      }
                    >
                      <input
                        checked={this.state.terms}
                        type="checkbox"
                        id="terms"
                      />
                      <label
                        htmlFor="terms"
                        onClick={() => {
                          this.setState({ terms: !this.state.terms });
                        }}
                      >
                        <span>
                          I’d like to receive emails from the Bentall Centre.
                          For details on how we will use your information please
                          read our{" "}
                          <a href="/privacy-policy" target="_blank">
                            Privacy Notice
                          </a>
                        </span>
                      </label>
                    </div>
                  )}

                  <button
                    className="submit-btn"
                    type="submit"
                    style={{
                      marginTop:
                        !this.state.showTerms || status === "error"
                          ? "25px"
                          : "",
                    }}
                  >
                    Sign up
                  </button>
                  {status === "error" &&
                    message.replace("0 - ", "") !== "Please enter a value" && (
                      <div
                        className="newsletter-form__error"
                        dangerouslySetInnerHTML={{
                          __html:
                            "This email has already signed up to our newsletter",
                        }}
                      />
                    )}
                </form>
              );
          }
        }}
      />
    );
  }
}

export default NewsletterForm;
