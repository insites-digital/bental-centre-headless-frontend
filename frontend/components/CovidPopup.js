import React, { useEffect, useState, useRef } from "react";
import NewsletterForm from "./NewsletterForm";
import closeIcon from "../static/images/site/close-icon.svg";
import { createCookie } from "../utils/createCookie";
import Link from "next/link";

const CovidPop = (props) => {
  const [headerHeight, setHeaderHeight] = useState(0);
  const [showPopup, setShowPopup] = useState(false);
  const popup = useRef(null);

  const closePopup = () => {
    setShowPopup(false);
    createCookie("covidPopup", "false", 1);
    setTimeout(() => {
      popup.current.classList.add("popup--pushed-back");
    }, 600);
  };

  // Click link, set cookie then goto Target page
  class LinkExtended extends React.Component {
    render() {
      const { onCustomClick, ...props } = this.props;
      return <a {...props} onClick={this.handleClick} />;
    }

    handleClick = (event) => {
      if (this.props.onClick) {
        this.props.onClick(event);
      }

      if (this.props.onCustomClick) {
        this.props.onCustomClick(createCookie("covidPopup", "false", 1));
      }
    };
  }
  //

  useEffect(() => {
    const header = document.querySelector("header");
    const cookieExists = document.cookie.match(
      /^(.*;)?\s*covidPopup\s*=\s*[^;]+(.*)?$/
    );

    if (cookieExists === null) {
      setTimeout(() => {
        if (header) {
          setHeaderHeight(header.clientHeight);
        }
        setShowPopup(true);
      }, 500);

      window.addEventListener("resize", () =>
        setHeaderHeight(header.clientHeight)
      );
    }

    return () => {
      window.removeEventListener("resize", () =>
        setHeaderHeight(header.clientHeight)
      );
    };
  }, []);
  return (
    <div
      className={`popup popup--hide popup--pushed-back ${
        showPopup ? "popup--show" : "popup--hide popup--pushed-back"
      }`}
      ref={popup}
      style={{
        top: headerHeight + "px",
        height: `calc(100vh - ${headerHeight}px)`,
        opacity: "0",
      }}
    >
      <div className="popup__close" onClick={closePopup}>
        <a role="button">
          <img src={closeIcon} alt="" />
        </a>
      </div>
      <div className="popup__message">
        <h2>Bentall Centre Update</h2>
        <p>Covid-19 latest centre information</p>
        <Link href="/whats-on/covid-19-info/">
          <LinkExtended
            onCustomClick={() => createCookie("covidPopup", "false", 1)}
          >
            Find out more
          </LinkExtended>
        </Link>
      </div>
    </div>
  );
};

export default CovidPop;
