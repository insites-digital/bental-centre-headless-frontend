import React, { useState, useEffect } from "react";
import Swiper from "react-id-swiper";
import parse from "html-react-parser";
import blackArrow from "../static/images/general/arrow-black.svg";
import { getSlug } from "../utils/getSlug";
import Link from "next/link";
import ProductImageHoverItem from "./ProductImageHoverItem";

const FeaturedProductsSlider = (props) => {
  const [swiper, setSwiper] = useState(null);
  const [windowSize, setWindowSize] = useState(0);
  const [loaded, setLoaded] = useState(false);
  const [params, setParams] = useState(null);

  // console.log("product slider :: ", props);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => {
      setWindowSize(window.innerWidth);
      setParams(null);
      if (window.innerWidth > 700) {
        setParams({
          slidesPerView: "auto",
          loop: true,
          cssMode: false,
          // noSwiping: true,
          speed: transitionSpeed,
          waitForTransition: false,
          freeMode: true,
          autoplay: {
            delay: 0,
            disableOnInteraction: true,
          },
        });
      } else {
        setParams({
          slidesPerView: "auto",
          loop: true,
        });
      }
    });

    if (window.innerWidth > 700) {
      setParams({
        slidesPerView: "auto",
        loop: true,
        cssMode: false,
        // noSwiping: true,
        speed: transitionSpeed,
        waitForTransition: false,
        freeMode: true,
        autoplay: {
          delay: 0,
          disableOnInteraction: true,
        },
      });
    } else {
      setParams({
        slidesPerView: "auto",
        loop: true,
        noSwiping: false,
        // speed: transitionSpeed,
        // waitForTransition: false,
      });
    }
    setLoaded(true);

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  useEffect(() => {
    if (!swiper || windowSize === 0) return;
    swiper.on("slideChangeTransitionStart", function () {
      setTranslate(swiper.getTranslate());
    });
  }, [swiper]);

  if (props.allProducts.length === 0) return <></>;

  const { heading, products } = props.data;
  const [translate, setTranslate] = useState(null);
  const [transformProperties, setTransformProperties] = useState(null);
  const transitionSpeed = 8000;

  const selectedProducts = props.allProducts.filter((item) => {
    return products.indexOf(item.id) !== -1;
  });

  // console.log(selectedProducts.length);

  if (selectedProducts.length < 1 || windowSize === 0) return <></>;

  if (!loaded || !params) return <></>;

  return (
    <section className="container">
      <div className="teaser-container">
        <div className="teaser-container__top">
          {heading && (
            <div className="teaser__title">
              <h2>{heading}</h2>
            </div>
          )}
          {props.data.hasOwnProperty("link") &&
            props.data.link.hasOwnProperty("url") &&
            props.data.link.hasOwnProperty("label") &&
            props.data.link.url !== null &&
            props.data.link.label !== "" && (
              <div className="teaser__view-more">
                <a
                  className="link link--uppercase link--arrow"
                  href={getSlug(props.data.link.url)}
                >
                  <span style={{ display: "inline-block" }}>
                    {props.data.link.label}
                  </span>{" "}
                  <span className="arrow">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="29.789"
                      height="8.21"
                    >
                      <g data-name="Group 1359">
                        <path
                          data-name="Path 759"
                          d="M0 4.105h20.186"
                          stroke="#000"
                          strokeWidth=".5"
                        />
                        <path
                          data-name="Polygon 1"
                          d="M29.789 4.1L18.842 8.2V0z"
                        />
                      </g>
                    </svg>
                  </span>
                </a>
              </div>
            )}
        </div>
        <div
          className="teaser-content__container"
          onMouseEnter={() => {
            if (windowSize <= 700) return;
            swiper.autoplay.stop();
            const swiperContainer = document.querySelector(
              ".swiper-container-free-mode > .swiper-wrapper"
            );
            if (!swiperContainer) return;
            setTransformProperties(swiperContainer.style.transform);
            var computedStyle = window.getComputedStyle(swiperContainer),
              somePause = computedStyle.getPropertyValue("transform");
            swiperContainer.style.transform = somePause;
          }}
          onMouseLeave={() => {
            if (windowSize <= 700) return;
            swiper.autoplay.start();
            const swiperContainer = document.querySelector(
              ".swiper-container-free-mode > .swiper-wrapper"
            );
            if (!swiperContainer) return;
            const transitionNumber = transitionSpeed / 1000;
            const a = (swiper.translate - translate) / transitionNumber;
            let b = (swiper.translate - swiper.getTranslate()) / a;
            b = b * 1000;
            b = Math.round(b);
            b = String(b) + "ms";
            swiperContainer.style.transform = transformProperties;
            swiperContainer.style.transitionDuration = b;
          }}
        >
          <Swiper getSwiper={setSwiper} {...params}>
            {props.stores.length > 0 &&
              selectedProducts.map((productItem) => {
                const store = props.stores.filter((item) => {
                  if (!productItem.acf.store) return false;
                  return productItem.acf.store.includes(item.id);
                });
                return (
                  <div
                    key={productItem.id}
                    className="teaser-content teaser-content--medium teaser-content--featured-product"
                  >
                    <Link
                      as={`${getSlug(productItem.link)}`}
                      href={`${getSlug(productItem.link)}`}
                    >
                      <a>
                        <ProductImageHoverItem
                          data={productItem}
                          size={"50vw"}
                          className=""
                        />

                        <div className="teaser-content__inside teaser-content__inside--featured-products">
                          {store.length > 0 && (
                            <h2 className="teaser__subject teaser__subject--no-underline">
                              {parse(store[0].title.rendered)}
                            </h2>
                          )}

                          <span className="teaser__subject teaser__subject--small teaser__subject--no-underline">
                            {parse(productItem.title.rendered)}
                          </span>
                          <span className="teaser__subject teaser__subject--small teaser__subject--no-underline">
                            £{parse(productItem.acf.price)}
                          </span>
                        </div>
                      </a>
                    </Link>
                  </div>
                );
              })}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default FeaturedProductsSlider;
