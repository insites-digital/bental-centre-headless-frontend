import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import moment from "moment";
import instagramIcon from "../static/images/site/instagram.svg";
import twitterIcon from "../static/images/site/twitter.svg";
import FooterSection from "./FooterSection";
import Link from "next/link";
import NewsletterForm from "./NewsletterForm";

const Footer = (props) => {
  if (!props.data) return <></>;
  const footerData = props.data;
  const [windowSize, setWindowSize] = useState(961);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  const getSlug = (url) => {
    if (!url) return url;
    let formatUrl;
    if (url === "") return "/";
    if (url.includes(".co.uk")) {
      formatUrl = url.split(".co.uk")[1];
      return formatUrl === "/home/" ? "/" : formatUrl;
    }
    formatUrl = url.split(".com")[1];
    return formatUrl === "/home/" ? "/" : formatUrl;
  };

  const {
    copyright,
    social,
    directions,
    address,
    navigation,
    newsletter,
  } = footerData.footer;

  const { opening_hours } = footerData.centre_details;

  const formattedAddress = address.replace(/\r\n/g, "<br/>");

  const data = [
    {
      title: "Getting here",
      classMod: "footer__section--direction",
      innerContent: (
        <>
          <p>{parse(directions)}</p>
          <address>{parse(formattedAddress)}</address>
        </>
      ),
      footContent: (
        <>
          <div className="social">
            <ul>
              {social &&
                social.map((item, index) => {
                  return (
                    <li
                      key={index}
                      className={"social__icon social__icon--" + item.account}
                    >
                      <a href={item.url}>
                        {item.account === "facebook" && (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="7.606"
                            height="15.764"
                          >
                            <path
                              d="M0 5.213h1.629V3.63a4.231 4.231 0 01.525-2.443A2.9 2.9 0 014.684 0a10.234 10.234 0 012.922.293l-.407 2.415a5.5 5.5 0 00-1.313-.2c-.634 0-1.2.227-1.2.86v1.841h2.6l-.181 2.359H4.687v8.193H1.632V7.568H.003z"
                              data-name="Path 730"
                            ></path>
                          </svg>
                        )}
                        {item.account === "twitter" && (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="15.765"
                            height="12.812"
                          >
                            <path
                              d="M15.765 1.517a6.493 6.493 0 01-1.858.509A3.231 3.231 0 0015.329.237a6.441 6.441 0 01-2.055.784 3.235 3.235 0 00-5.593 2.213 3.188 3.188 0 00.083.737A9.185 9.185 0 011.097.591a3.242 3.242 0 001 4.32 3.243 3.243 0 01-1.466-.4v.04a3.235 3.235 0 002.6 3.171 3.143 3.143 0 01-.853.114 3.2 3.2 0 01-.608-.057 3.233 3.233 0 003.021 2.245 6.541 6.541 0 01-4.788 1.34 9.2 9.2 0 0014.159-7.75c0-.141 0-.281-.009-.418a6.557 6.557 0 001.614-1.675"
                              data-name="Path 732"
                            ></path>
                          </svg>
                        )}
                        {item.account === "instagram" && (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="15.764"
                            height="15.753"
                          >
                            <path
                              d="M12.718 0H3.047A3.049 3.049 0 000 3.044v9.665a3.049 3.049 0 003.047 3.044h9.671a3.049 3.049 0 003.047-3.044V3.044A3.049 3.049 0 0012.718 0zm.874 1.816h.348v2.67l-2.663.01-.009-2.67zM5.634 6.259a2.766 2.766 0 11-.526 1.617 2.769 2.769 0 01.528-1.617zm8.6 6.449a1.511 1.511 0 01-1.511 1.509H3.052a1.51 1.51 0 01-1.51-1.509V6.259h2.353a4.31 4.31 0 107.986 0h2.353z"
                              data-name="Path 734"
                            ></path>
                          </svg>
                        )}
                      </a>
                    </li>
                  );
                })}
            </ul>
          </div>
        </>
      ),
    },
    {
      title: "Opening hours",
      classMod: "footer__section--opening",
      innerContent: (
        <>
          <div
            className="table-container"
            aria-labelledby="opening-hours"
            tabIndex="0"
          >
            {opening_hours &&
              opening_hours.map((date, index) => {
                const openTimes = !date.isClosed
                  ? moment(new Date("1970-01-01T" + date.open)).format(
                      "hh:mma"
                    ) +
                    " - " +
                    moment(new Date("1970-01-01T" + date.close)).format(
                      "hh:mma"
                    )
                  : "CLOSED";

                return (
                  <div
                    key={`date-${index}`}
                    className="table-row"
                    style={{ display: "flex" }}
                  >
                    <div>
                      <span>{date.day}</span>
                    </div>

                    <div>
                      <span>{openTimes}</span>
                    </div>
                  </div>
                );
              })}
          </div>
        </>
      ),
      footContent: <></>,
    },
    {
      title: "Info",
      classMod: "footer__section--navigation",
      innerContent: (
        <>
          <nav className="footer__section-links">
            <ul>
              {navigation &&
                navigation.map((navItem, index) => {
                  return (
                    <li key={`nav-${index}`}>
                      <Link href={getSlug(navItem.url)}>
                        <a href={getSlug(navItem.url)}>{navItem.label}</a>
                      </Link>
                    </li>
                  );
                })}
            </ul>
          </nav>
        </>
      ),
      footContent: <></>,
    },
    {
      title: "Newsletter",
      classMod: "footer__section--newsletter",
      innerContent: (
        <>
          <p className="bold">{newsletter.text}</p>
          <NewsletterForm />
        </>
      ),
      footContent: (
        <>
          <p>{copyright}</p>
        </>
      ),
    },
  ];

  return (
    <footer className="footer">
      {data &&
        data.map((item, index) => (
          <FooterSection data={item} key={`footer-section-${index}`} />
        ))}
      {windowSize < 961 && (
        <>
          <div className="social--mobile">
            <ul>
              {social &&
                social.map((item, index) => {
                  return (
                    <li
                      key={index}
                      className={"social__icon social__icon--" + item.account}
                    >
                      <a href={item.url} target="_blank">
                        {item.account === "facebook" && (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="35.908"
                            height="35.907"
                          >
                            <path
                              d="M17.953 35.907a17.953 17.953 0 1117.954-17.953 17.974 17.974 0 01-17.954 17.953zm0-35.031a17.078 17.078 0 1017.078 17.078A17.1 17.1 0 0017.953.876z"
                              data-name="Path 729"
                            ></path>
                            <path
                              d="M14.151 15.284h1.629v-1.583a4.231 4.231 0 01.525-2.443 2.9 2.9 0 012.53-1.187 10.234 10.234 0 012.922.293l-.407 2.415a5.5 5.5 0 00-1.313-.2c-.634 0-1.2.227-1.2.86v1.841h2.6l-.181 2.359h-2.418v8.193h-3.055v-8.193h-1.629z"
                              data-name="Path 730"
                            ></path>
                          </svg>
                        )}
                        {item.account === "twitter" && (
                          <img src={twitterIcon} />
                        )}
                        {item.account === "instagram" && (
                          <img src={instagramIcon} />
                        )}
                      </a>
                    </li>
                  );
                })}
            </ul>
          </div>
          <p className="copyright">{copyright}</p>
        </>
      )}
    </footer>
  );
};

export default Footer;
