import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import moment from "moment";

const SubHeader = (props) => {
  const { data } = props;
  const [showBanner, setShowBanner] = useState(true);
  const marquee = useRef(null);

  const getSlug = (url) => {
    if (!url) return url;
    let formatUrl;
    if (url.includes(".co.uk")) {
      formatUrl = url.split(".co.uk")[1];
      return formatUrl === "/home/" ? "/" : formatUrl;
    }
    formatUrl = url.split(".com")[1];
    return formatUrl === "/home/" ? "/" : formatUrl;
  };

  useEffect(() => {
    if (localStorage.getItem("hideMarquee")) {
      var object = JSON.parse(localStorage.getItem("hideMarquee"));
      if (!object || !object.timestamp) return;
      var dateString = moment(object.timestamp),
        expiry = moment(dateString).add(30, "days"),
        now = moment();
      if (expiry.diff(now, "days") > 0) {
        hideBanner();
      } else {
        setShowBanner(true);
      }
    }

    setTimeout(() => {
      const subHeader = document.getElementById("sub-header");
      if (subHeader) {
        subHeader.style.display = "flex";
      }
    }, 300);
  }, []);

  const hideBanner = () => {
    const mainHeader = document.querySelector("header").clientHeight;
    const subHeader = document.getElementById("sub-header").clientHeight;
    const newHeight = mainHeader - subHeader + "px";
    document.querySelector(".page-wrapper").style.marginTop = newHeight;
    setShowBanner(false);

    let object = { timestamp: new Date() };
    localStorage.setItem("hideMarquee", JSON.stringify(object));
  };

  const Ticker = data.map((item, index) => (
    <Link key={`item-${index}`} href={getSlug(item.link.url)}>
      <a href={item.link.url} className="marquee_item alt">
        <span
          style={{ display: "flex", alignItems: "center" }}
          className={index === 0 ? "marquee--arrow" : ""}
        >
          <span className={index === 1 ? "underline" : ""}>
            {item.link.text}
          </span>
          {index === 0 && (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="22.463"
              height="6.191"
            >
              <g data-name="Group 1335">
                <path
                  fill="none"
                  stroke="#fff"
                  d="M0 3.095h15.222"
                  data-name="Path 759"
                ></path>
                <path
                  fill="#fff"
                  d="M22.463 3.1l-8.255 3.1V0z"
                  data-name="Polygon 1"
                ></path>
              </g>
            </svg>
          )}
        </span>
      </a>
    </Link>
  ));

  if (!showBanner) return <></>;

  return (
    <div id="sub-header" style={{ display: "none" }}>
      <div id="sub-header-close" onClick={hideBanner}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="11.188"
          height="11.188"
          fill="none"
          stroke="#fff"
        >
          <path d="M.354.354l10.48 10.48" />
          <path d="M10.834.354L.354 10.834" />
        </svg>
      </div>

      <div className="marquee-header-wrap">
        <div className="marquee" ref={marquee}>
          {Ticker}
          {Ticker}
          {Ticker}
          {Ticker}
          {Ticker}
          {Ticker}
        </div>
      </div>
    </div>
  );
};

export default SubHeader;
