import React from "react";
import TabbedRestaurants from "../../components/tabbedContent/TabbedRestaurants";
import CampaignFeature from "../../components/CampaignFeature";
import WhatsOn from "../../components/WhatsOn";
import TeaserFeature from "../../components/TeaserFeature";
import FeaturedProductsSlider from "../../components/FeaturedProductsSlider";
import RestaurantSlider from "../../components/RestaurantSlider";

const EatDrink = (props) => {
  const title = props.data.title.rendered;
  let Components = <></>;

  if (
    props.data.acf &&
    props.data.acf.hasOwnProperty("components") &&
    props.data.acf.components.length > 0
  ) {
    Components = () =>
      props.data.acf.components.map((item, index) => {
        switch (item.acf_fc_layout) {
          case "eventSlider":
            return (
              <WhatsOn
                key={`whatson=${index}`}
                data={item}
                events={props.events}
                eventCats={props.eventCats}
              />
            );
          case "dotd":
            return (
              <TeaserFeature
                key={`teaser=${index}`}
                data={props.globalContent.daily_deals}
                products={props.products}
              />
            );
          case "fullwidthTeaser":
            return <CampaignFeature key={`campaign=${index}`} data={item} />;
          case "restaurantSlider":
            return (
              <RestaurantSlider
                key={`restaurant=${index}`}
                data={props.restaurants}
                componentData={item}
                restaurantCats={props.restaurantCats}
                floors={props.floors}
              />
            );
          case "productSlider":
            return (
              <FeaturedProductsSlider
                key={`products=${index}`}
                data={item}
                allProducts={props.products}
                stores={props.stores}
              />
            );
          default:
            break;
        }
      });
  }
  return (
    <>
      <div
        className="container services-page"
        style={{ marginBottom: "100px", marginTop: "40px" }}
      >
        <TabbedRestaurants
          title={title}
          restaurants={props.restaurants}
          categories={props.categories}
          floors={props.floors}
        />
      </div>
      <Components />
    </>
  );
};

export default EatDrink;
