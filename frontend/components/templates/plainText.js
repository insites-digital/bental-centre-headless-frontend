import React from "react";
import parse from "html-react-parser";
import { getSlug } from "../../utils/getSlug";
import Error from "../Error";

const PlainText = (props) => {
  const { data } = props;
  if (
    !data.acf ||
    data.acf.components.filter((e) => {
      return e.acf_fc_layout === "text_area";
    }).length < 0
  ) {
    return <Error />;
  }

  const formatLinks = (text) => {
    let string = text;
    let originalLink = text.match(/href="([^"]*)/g);
    if (originalLink && originalLink.length > 0) {
      originalLink = originalLink.map((item) => item.replace("href=", ""));
      originalLink = originalLink.map((item) => item.replace(/"/g, ""));
    }
    if (
      originalLink &&
      !originalLink.includes("mailto:") &&
      !originalLink.includes("tel:")
    ) {
      originalLink = originalLink[1];
    } else {
      return text;
    }
    const formattedLink = getSlug(originalLink);
    string = string.replace(originalLink, formattedLink);
    return string;
  };

  let pageContent = data.acf.components.filter((e) => {
    return e.acf_fc_layout === "text_area";
  })[0];

  if (pageContent && pageContent.content) {
    pageContent = formatLinks(pageContent.content);
  }

  if (!pageContent) return <></>;
  return (
    <>
      <div className="hero-text" style={{ borderBottom: "1px black solid" }}>
        <nav className="container" style={{ marginTop: "36px" }}>
          <ul>
            <li>
              <a href="/the-centre/" style={{ textDecoration: "none" }}>
                The Centre
              </a>
            </li>
            <li>
              <a
                href=""
                className="disabled-link"
                style={{ textDecoration: "none" }}
              >
                {parse(props.data.title.rendered)}
              </a>
            </li>
          </ul>
        </nav>
      </div>
      <div className="container">
        <section className="plain-text">{parse(pageContent)}</section>
      </div>
    </>
  );
};

export default PlainText;
