import React from "react";
import parse from "html-react-parser";
import ProductGrid from "../../components/ProductGrid";
import ImageText5050 from "../../components/ImageText5050";
import VideoPromotion from "../../components/video/VideoPromotion";
import ContentPromotion from "../../components/ContentPromotion";
import CampaignFeature from "../../components/CampaignFeature";
import Hero from "../../components/Hero";
import EventGrid from "../../components/EventGrid";
import RestaurantSlider from "../../components/RestaurantSlider";
import WhatsOn from "../../components/WhatsOn";
import HeroText from "../../components/HeroText";
import FeaturedProductsSlider from "../../components/FeaturedProductsSlider";
import TeaserFeature from "../../components/TeaserFeature";

const Campaign = (props) => {
  const GetComponents = () => {
    return props.data.acf.components ? (
      props.data.acf.components.map((component) => {
        switch (component.acf_fc_layout) {
          case "hero":
            return <Hero data={component.slides} />;
          case "fiftyFifty":
            return <ImageText5050 data={component} />;
          case "eventSlider":
            return (
              <WhatsOn
                data={component}
                events={props.events}
                eventCats={props.eventCats}
              />
            );
          case "productGrid":
            return (
              <ProductGrid
                data={component}
                products={props.products}
                stores={props.stores}
              />
            );
          case "eventGrid":
            return (
              <EventGrid
                data={component}
                events={props.events}
                eventCats={props.eventCats}
              />
            );
          case "videoPromotion":
            return <VideoPromotion data={component} />;
          case "restaurantSlider":
            return (
              <RestaurantSlider
                data={props.restaurants}
                componentData={component}
                restaurantCats={props.restaurantCats}
                floors={props.floors}
              />
            );
          case "contentPromotion":
            return <ContentPromotion data={component} />;
          case "fullwidthTeaser":
            return <CampaignFeature data={component} />;
          case "dotd":
            return (
              <TeaserFeature
                data={props.globalContent.daily_deals}
                products={props.products}
              />
            );
          case "productSlider":
            return (
              <FeaturedProductsSlider
                data={component}
                allProducts={props.products}
                stores={props.stores}
              />
            );
          case "text_area":
            return (
              <HeroText
                data={{
                  description: component.content,
                }}
              />
            );
          default:
            return <></>;
        }
      })
    ) : (
      <></>
    );
  };

  return (
    <>
      <div className="container hero-text">
        <nav>
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li style={{ textTransform: "uppercase" }}>
              {parse(props.data.title.rendered)}
            </li>
          </ul>
        </nav>

        <div className="hero-text__content hero-text__content--full">
          <h2>{parse(props.data.acf.campaign_name)}</h2>
          <h1>{parse(props.data.title.rendered)}</h1>
          {parse(props.data.acf.content)}
        </div>
      </div>
      <GetComponents />
    </>
  );
};

export default Campaign;
