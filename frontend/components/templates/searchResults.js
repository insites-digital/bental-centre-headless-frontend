import React, { useState, useEffect } from "react";
import sw from "stopword";
import parse from "html-react-parser";
import { getSlug } from "../../utils/getSlug";
import Link from "next/link";
import _ from "lodash";
import { useRouter } from "next/router";
import blackArrow from "../../static/images/general/arrow-black.svg";
import he from "he";

const resultsInitial = [];
const valueInitial = "";

const SearchResults = (props) => {
  const [results, setResults] = useState(resultsInitial);
  const [value, setValue] = useState(valueInitial);
  const router = useRouter();
  const searchData = {
    stores: {
      name: "Stores & Brands",
      results: props.stores,
    },
    products: {
      name: "Products",
      results: props.products,
    },
    restaurants: {
      name: "Restaurants",
      results: props.restaurants,
    },
    amenities: {
      name: "Services & Facilities",
      results: props.amenities,
    },
    events: {
      name: "Events",
      results: props.events,
    },
    campaigns: {
      name: "Campaigns",
      results: props.campaigns,
    },
  };

  const decodeString = (encodedStr) => {
    let parser = new DOMParser();
    let dom = parser.parseFromString(
      "<!doctype html><body>" + encodedStr,
      "text/html"
    );
    return dom.body.textContent;
  };

  const handleSearchChange = ({ value }) => {
    setValue({ value });

    setTimeout(() => {
      if (value.length < 1) {
        setResults(resultsInitial);
        setValue(valueInitial);
        return;
      }

      let isMatch = (result) => {
        let searchTerms = sw.removeStopwords(value.split(" "));
        searchTerms = searchTerms.filter((el) => el != "");

        let match = false;
        let matches = 0;
        searchTerms.forEach((term) => {
          const termRegex = new RegExp(_.escapeRegExp(term), "i");
          if (termRegex.test(decodeString(he.decode(result.title.rendered)))) {
            match = true;
            matches = matches + 1;
          }
          if (
            termRegex.test(result.content.rendered.replace(/(<([^>]+)>)/gi, ""))
          ) {
            match = true;
            matches = matches + 1;
          }
          if (matches === 0 && term.substr(term.length - 1) === "s") {
            let nonPluralRegex = new RegExp(
              _.escapeRegExp(term.substr(0, term.length - 1)),
              "i"
            );
            if (
              nonPluralRegex.test(
                decodeString(he.decode(result.title.rendered))
              )
            ) {
              match = true;
              matches = matches + 1;
            }

            if (
              nonPluralRegex.test(
                decodeString(
                  he.decode(
                    result.content.rendered.replace(/(<([^>]+)>)/gi, "")
                  )
                )
              )
            ) {
              match = true;
              matches = matches + 1;
            }
          }
          if (matches > 0) {
            result.matches = matches;
          }
        });
        return match;
      };

      const filteredResults = _.reduce(
        searchData,
        (memo, data, name) => {
          let results = _.filter(data.results, isMatch);
          results = results.sort((a, b) => {
            return b.matches - a.matches;
          });
          if (results.length && data && data.name) {
            let dataName = data.name;
            memo[name] = { name: dataName, results };
          } // eslint-disable-line no-param-reassign

          return memo;
        },
        {}
      );
      setResults(filteredResults);
    }, 300);
  };

  useEffect(() => {
    let keyword = router.asPath;
    keyword = keyword.substr(keyword.indexOf("?keyword=") + 9, keyword.length);
    handleSearchChange({ value: decodeURIComponent(keyword) });
  }, []);

  // Maybe do a loading of resultsh here
  if (Object.keys(results).length === 0 && results.constructor === Object)
    return (
      <>
        <section className="search-results search-result--empty">
          <div className="search-results__categories">
            <div className="search-results__header">
              <h2>Search results for your search: {value["value"]}</h2>
            </div>
          </div>
          <div className="container search-results__no-results-msg">
            <div className="search-results__msg-container">
              <h1>There are no matches for your search</h1>
              <p>Try less specific search terms or checking the spelling.</p>
              <p>
                <Link as={`/`} href={`/`}>
                  <a className="link link--uppercase link--arrow">
                    Back to homepage <img src={blackArrow} />
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </section>
      </>
    );

  return (
    <section className="search-results">
      <div className="search-results__categories">
        <div className="search-results__header">
          <h2>Search results for your search: {value["value"]}</h2>
        </div>
        {results.hasOwnProperty("events") && (
          <dl className="search-results__category-list category">
            <dt className="search-results__category-header category-title">
              {results &&
                results.events &&
                results.events.name &&
                parse(results.events.name)}
            </dt>
            {results.events.results &&
              results.events.results.length > 0 &&
              results.events.results.map((item) => (
                <dd
                  key={item.id}
                  className="search-results__category-result category-results"
                >
                  <Link as={getSlug(item.link)} href={getSlug(item.link)}>
                    <a>{parse(item.title.rendered)}</a>
                  </Link>
                </dd>
              ))}
          </dl>
        )}

        {results.hasOwnProperty("stores") && (
          <dl className="search-results__category-list category">
            <dt className="search-results__category-header category-title">
              {results &&
                results.stores &&
                results.stores.name &&
                parse(results.stores.name)}
            </dt>
            {results.stores.results &&
              results.stores.results.length > 0 &&
              results.stores.results.map((item) => (
                <dd
                  key={item.id}
                  className="search-results__category-result category-results"
                >
                  <Link as={getSlug(item.link)} href={getSlug(item.link)}>
                    <a>{parse(item.title.rendered)}</a>
                  </Link>
                </dd>
              ))}
          </dl>
        )}

        {results.hasOwnProperty("amenities") && (
          <dl className="search-results__category-list category">
            <dt className="search-results__category-header category-title">
              {results &&
                results.amenities &&
                results.amenities.name &&
                parse(results.amenities.name)}
            </dt>
            {results.amenities.results &&
              results.amenities.results.length > 0 &&
              results.amenities.results.map((item) => (
                <dd
                  key={item.id}
                  className="search-results__category-result category-results"
                >
                  <Link as={getSlug(item.link)} href={getSlug(item.link)}>
                    <a>{parse(item.title.rendered)}</a>
                  </Link>
                </dd>
              ))}
          </dl>
        )}

        {results.hasOwnProperty("restaurants") && (
          <dl className="search-results__category-list category">
            <dt className="search-results__category-header category-title">
              {results &&
                results.restaurants &&
                results.restaurants.name &&
                parse(results.restaurants.name)}
            </dt>
            {results.restaurants.results &&
              results.restaurants.results.length > 0 &&
              results.restaurants.results.map((item) => (
                <dd
                  key={item.id}
                  className="search-results__category-result category-results"
                >
                  <Link as={getSlug(item.link)} href={getSlug(item.link)}>
                    <a>{parse(item.title.rendered)}</a>
                  </Link>
                </dd>
              ))}
          </dl>
        )}

        {results.hasOwnProperty("campaigns") && (
          <dl className="search-results__category-list category">
            <dt className="search-results__category-header category-title">
              {results && results.campaigns && parse(results.campaigns.name)}
            </dt>
            {results.campaigns.results &&
              results.campaigns.results.length > 0 &&
              results.campaigns.results.map((item) => (
                <dd
                  key={item.id}
                  className="search-results__category-result category-results"
                >
                  <Link as={getSlug(item.link)} href={getSlug(item.link)}>
                    <a>{parse(item.title.rendered)}</a>
                  </Link>
                </dd>
              ))}
          </dl>
        )}

        {results.hasOwnProperty("products") && (
          <div className="search-results__category-list">
            <h2 className="search-results__category-header category-title search-results__category-header--selected">
              Products
            </h2>
          </div>
        )}
      </div>
      {results.hasOwnProperty("products") && (
        <div className="search-results__items container">
          <div className="product-grid__products teaser-content__container">
            {results.products.results.map((product) => {
              const store = props.stores.filter(
                (store) => store.id === product.acf.store[0]
              );
              return (
                <div
                  key={`${product.id}`}
                  className="teaser-content teaser-content--third"
                >
                  <Link as={getSlug(product.link)} href={getSlug(product.link)}>
                    <a>
                      <div className="teaser__img">
                        <img
                          src={product.acf.image.url.url}
                          alt={product.title.rendered}
                        />
                      </div>
                      <div className="teaser-content__inside teaser-content__inside--featured-products">
                        <h2 className="teaser__subject teaser__subject--no-underline">
                          {store &&
                            store.hasOwnProperty("title") &&
                            parse(store.title.rendered)}
                        </h2>
                        <span className="teaser__subject teaser__subject--no-underline teaser__subject--not-bold">
                          {parse(product.title.rendered)}
                        </span>
                        <span className="teaser__subject teaser__subject--no-underline teaser__subject--not-bold">
                          £{product.acf.price}
                        </span>
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </section>
  );
};

export default SearchResults;
