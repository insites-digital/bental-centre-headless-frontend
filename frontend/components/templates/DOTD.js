import React, { useState, useEffect } from "react";
import { getSlug } from "../../utils/getSlug";
import parse from "html-react-parser";
import twitter from "../../static/images/social/twitter.svg";
import blackArrow from "../../static/images/general/arrow-black.svg";
import facebook from "../../static/images/social/facebook.svg";
import instagram from "../../static/images/social/instagram.svg";

import moment from "moment";
import "moment-timezone";

const DOTD = (props) => {
  const [dealTimer, setDealTimer] = useState(null);
  const [cta, setCta] = useState(null);
  const [description, setDescription] = useState(null);
  const [deal_ends, setDealEnds] = useState(null);
  const [showDealOfDay, setShowDealOfDay] = useState(null);
  const [product, setProduct] = useState(null);
  const [storeData, setStoreData] = useState(null);
  const [dealEnds, setDealEndsMessage] = useState("");

  const setTimer = () => {
    setDealEndsMessage("Ends in ");
    const now = moment();
    const expiration = moment(deal_ends);
    const diff = expiration.diff(now);
    const diffDuration = moment.duration(diff);
    const days = diffDuration.days();
    const hours = diffDuration.hours();
    const minutes = diffDuration.minutes();
    const seconds = diffDuration.seconds();
    let dealEndsStr = "";
    if (days > 0) {
      dealEndsStr += days === 1 ? "1 day " : days + " days ";
    }
    if (hours >= 1) {
      dealEndsStr += hours > 1 ? hours + "hours " : "1hour ";
    }
    if (minutes >= 1) {
      dealEndsStr += minutes + "min ";
    }
    dealEndsStr += seconds + "sec";

    setDealEndsMessage(dealEnds + dealEndsStr);

    if (days <= 0 && hours <= 0 && minutes <= 0 && seconds < 1) {
      setDealTimer("Deal ended");
      return;
    }
    setDealTimer(dealEnds + dealEndsStr);
  };

  useEffect(() => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0");
    var yyyy = today.getFullYear();
    today = mm + "/" + dd + "/" + yyyy;
    props.data.deals.length > 0 &&
      props.data.deals.forEach((teaser) => {
        moment.tz.setDefault("Europe/London");
        const splitDate = teaser.display_date.split("/");
        const day = splitDate[0];
        const month = splitDate[1];
        const year = splitDate[2];
        const difference = moment(`${month}/${day}/${year}`).diff(
          today,
          "days"
        );

        if (difference === 0) {
          setShowDealOfDay(true);
          setProduct(
            props.products.filter((item) => item.id === teaser.product[0])
          );
          setCta(teaser.cta);
          setDealEnds(teaser.deal_ends);
          setDescription(teaser.description);
        }
      });
  }, []);

  useEffect(() => {
    let startTimer;

    if (deal_ends !== null && !dealTimer)
      startTimer = setInterval(() => {
        setTimer();
      }, 1000);
    return () => {
      clearInterval(startTimer);
    };
  }, [deal_ends]);

  useEffect(() => {
    if (product !== null) {
      setStoreData(
        props.stores.filter((store) => {
          return store.id === product[0].acf.store[0];
        })[0]
      );
    }
  }, [product]);

  {
    showDealOfDay === null && <></>;
  }
  return (
    <>
      {showDealOfDay && dealTimer && (
        <section className="single-page">
          <div className="container hero-text">
            <nav>
              <ul>
                <li>
                  <a href="/the-centre/">The Centre</a>
                </li>
                <li>
                  <a href="/deal-of-the-day/" style={{ pointerEvents: "none" }}>
                    Deal of the Day
                  </a>
                </li>
              </ul>
            </nav>
          </div>
          <div className="container">
            <div className="teaser-container teaser-container--single-page">
              <div
                className="teaser-content__container"
                style={{ flexWrap: "wrap" }}
              >
                <div className="teaser-content teaser-content--half">
                  <div className="teaser__img">
                    <img
                      src={product[0].acf.image.url.url}
                      alt={product[0].title.rendered}
                    />
                  </div>
                </div>
                <div className="teaser-content teaser-content--half teaser-content--details">
                  <div className="teaser__span-container">
                    <div className="teaser__tag-container">
                      <span className="teaser__tag">{dealTimer}</span>
                    </div>
                  </div>
                  <div className="teaser-content__inside">
                    <h2 className="teaser__subject">Deal of the Day</h2>
                    <div className="teaser__description">
                      {description && parse(description)}
                      {cta && cta !== "" && (
                        <a
                          className="link link--uppercase link--arrow"
                          href={getSlug(product[0].link)}
                        >
                          {cta} <img src={blackArrow} />
                        </a>
                      )}
                    </div>
                  </div>
                </div>
                {storeData.acf.opening_hours &&
                  storeData.acf.opening_hours.length > 0 && (
                    <div className="teaser-content--fourth">
                      <h2 className="teaser__subject">Opening hours</h2>
                      <div className="teaser-content__inside">
                        <div className="teaser__span-container">
                          <table className="opening-times">
                            <tbody>
                              {storeData.acf.opening_hours.map(
                                (opening, index) => (
                                  <tr key={`openinghours-${index}`}>
                                    <td className="capitalize">
                                      {opening.day}
                                    </td>
                                    <td>
                                      {moment(
                                        new Date(
                                          "1970-01-01T" + opening.start_time
                                        )
                                      )
                                        .format("h:mma")
                                        .replace(":00", "")}{" "}
                                      <span className="dash">-</span>{" "}
                                      {moment(
                                        new Date(
                                          "1970-01-01T" + opening.end_time
                                        )
                                      )
                                        .format("h:mma")
                                        .replace(":00", "")}
                                    </td>
                                  </tr>
                                )
                              )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}
                {storeData.acf.contact_details && (
                  <div className="teaser-content--fourth teaser-content--fourth--contact">
                    {storeData.acf.contact_details.email !== "" ||
                      (storeData.acf.contact_details.phone_number !== "" && (
                        <h2 className="teaser__subject">Contact</h2>
                      ))}

                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        <p>
                          {storeData.acf.contact_details.email !== "" && (
                            <>
                              <a
                                href={
                                  "mailto:" +
                                  storeData.acf.contact_details.email
                                }
                              >
                                {storeData.acf.contact_details.email}
                              </a>
                              <br />
                            </>
                          )}
                          {storeData.acf.contact_details.phone_number !==
                            "" && (
                            <>
                              <a
                                href={
                                  "tel:" +
                                  storeData.acf.contact_details.phone_number
                                }
                              >
                                {storeData.acf.contact_details.phone_number}
                              </a>
                            </>
                          )}
                        </p>
                        {storeData.acf.contact_details.social_media.length >
                          0 && (
                          <div className="social">
                            {storeData.acf.contact_details.social_media.map(
                              (social) => {
                                let socialAccount;
                                if (social.account === "facebook") {
                                  socialAccount = facebook;
                                } else if (social.account === "twitter") {
                                  socialAccount = twitter;
                                } else {
                                  socialAccount = instagram;
                                }
                                return (
                                  <p
                                    key={social.account}
                                    className={`social--${social.account}`}
                                  >
                                    <a href={social.link} target="_blank">
                                      <div className="social__container">
                                        <div className="social__icon">
                                          <img src={socialAccount} alt="" />
                                        </div>
                                        {social.handle !== "" && (
                                          <span className="social__handle">
                                            {social.handle}
                                          </span>
                                        )}
                                      </div>
                                    </a>
                                  </p>
                                );
                              }
                            )}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                )}

                {storeData.acf.contact_details &&
                  storeData.acf.contact_details.address !== "" && (
                    <div className="teaser-content--fourth">
                      <h2 className="teaser__subject">Location</h2>
                      <div className="teaser-content__inside">
                        <div className="teaser__description">
                          <address>
                            {parse(storeData.acf.contact_details.address)}
                          </address>
                        </div>
                      </div>
                    </div>
                  )}

                {storeData.acf.facilities &&
                  storeData.acf.facilities.length > 0 && (
                    <div className="teaser-content--fourth">
                      <h2 className="teaser__subject">Facilities</h2>
                      <div className="teaser-content__inside">
                        <div className="teaser__description">
                          <ul>
                            {storeData.acf.facilities.map((facility, index) => (
                              <li key={`facility-${index}`}>{facility}</li>
                            ))}
                          </ul>
                        </div>
                      </div>
                    </div>
                  )}
              </div>
            </div>
          </div>
        </section>
      )}
      {!showDealOfDay && (
        <section className="error container">
          <div className="error__container">
            <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2MS44NzYiIGhlaWdodD0iNTIuNzY3Ij48Zz48Y2lyY2xlIGN4PSIxLjYyNyIgY3k9IjEuNjI3IiByPSIxLjYyNyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi4xODEgNS45ODYpIi8+PGNpcmNsZSBjeD0iMS42MjciIGN5PSIxLjYyNyIgcj0iMS42MjciIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwLjkzMSA1Ljk4NikiLz48Y2lyY2xlIGN4PSIxLjYyNyIgY3k9IjEuNjI3IiByPSIxLjYyNyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTUuNjE1IDUuOTg2KSIvPjxwYXRoIGQ9Ik01Ny41MTYgMEg0LjI5NEE0LjI5IDQuMjkgMCAwMDAgNC4yOTR2NDQuMTc5YTQuMjkgNC4yOSAwIDAwNC4yOTQgNC4yOTRoNTMuMjg3YTQuMjkgNC4yOSAwIDAwNC4yOTQtNC4yOTRWNC4yOTRBNC4zODkgNC4zODkgMCAwMDU3LjUxNiAwek00LjI5NCAzLjMxOGg1My4yODdhMSAxIDAgMDEuOTc2Ljk3NnY3LjYxMkgzLjMxOFY0LjI5NGExIDEgMCAwMS45NzYtLjk3NnptNTMuMjIyIDQ2LjA2NUg0LjI5NGExIDEgMCAwMS0uOTc2LS45NzZWMTUuMTZoNTUuMTc0djMzLjI0N2ExIDEgMCAwMS0uOTc2Ljk3NnoiLz48cGF0aCBkPSJNMTkuNzE0IDI5LjQ3NGExLjczMyAxLjczMyAwIDAwMi4zNDIgMGwxLjgyLTEuODIyIDEuODIyIDEuODIyYTEuNzMzIDEuNzMzIDAgMDAyLjM0MiAwIDEuNjQ5IDEuNjQ5IDAgMDAwLTIuMzQybC0xLjgyLTEuODI0IDEuODIyLTEuODIyYTEuNjU2IDEuNjU2IDAgMTAtMi4zNDItMi4zNDJsLTEuODI0IDEuODI0LTEuODItMS44MjJhMS42NTYgMS42NTYgMCAwMC0yLjM0MiAyLjM0MmwxLjgyMiAxLjgyLTEuODIyIDEuODIyYTEuNjQ5IDEuNjQ5IDAgMDAwIDIuMzQ0ek00Mi4wOTYgMjEuMjExYTEuNjQ5IDEuNjQ5IDAgMDAtMi4zNDIgMGwtMS44MjQgMS44MjItMS44Mi0xLjgyMmExLjY1NiAxLjY1NiAwIDEwLTIuMzQyIDIuMzQybDEuODIyIDEuODItMS44MjIgMS44MjJhMS42NDkgMS42NDkgMCAwMDAgMi4zNDIgMS43MzMgMS43MzMgMCAwMDIuMzQyIDBsMS44Mi0xLjgyIDEuODIyIDEuODIyYTEuNzMzIDEuNzMzIDAgMDAyLjM0MiAwIDEuNjQ5IDEuNjQ5IDAgMDAwLTIuMzQybC0xLjgyLTEuODI0IDEuODIyLTEuODIyYTEuNjQ5IDEuNjQ5IDAgMDAwLTIuMzR6TTQwLjYgMzQuNDE5SDIxLjIxMWExLjYyNyAxLjYyNyAwIDAwMCAzLjI1M2g5LjE3M3YzLjMxOGE0LjI5NCA0LjI5NCAwIDEwOC41ODggMHYtMy4zMThoMS41NjJhMS42MTEgMS42MTEgMCAwMDEuNjI3LTEuNjI3IDEuNTIgMS41MiAwIDAwLTEuNTYxLTEuNjI2em0tNC44OCA2LjYzN2ExLjA0MSAxLjA0MSAwIDAxLTIuMDgyIDB2LTMuMzE5aDIuMDE3djMuMzE4eiIvPjwvZz48L3N2Zz4=" />
            <h1>Sorry, there is no deal of the day today.</h1>
            <p>Try again tomorrow or take a look at other products.</p>
            <p>
              <a className="link link--uppercase link--arrow" href="/">
                Back to homepage{" "}
                <img src="data:image/svg+xml;base64,CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjkuNzg5IiBoZWlnaHQ9IjguMjEiPjxnIGRhdGEtbmFtZT0iR3JvdXAgMTMxMiI+PHBhdGggZGF0YS1uYW1lPSJQYXRoIDc1OSIgZD0iTTAgNC4xMDVoMjAuMTg2IiBmaWxsPSJub25lIiBzdHJva2U9IiMwMDAiLz48cGF0aCBkYXRhLW5hbWU9IlBvbHlnb24gMSIgZD0iTTI5Ljc4OSA0LjFMMTguODQyIDguMlYweiIvPjwvZz48L3N2Zz4=" />
              </a>
            </p>
          </div>
        </section>
      )}
    </>
  );
};

export default DOTD;
