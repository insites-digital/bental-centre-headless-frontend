import React, { useState, useEffect } from "react";
import parse from "html-react-parser";

const OpeningHours = (data) => {
    if (
        !data.acf.contact_details &&
        !data.acf.facilities &&
        data.acf.location &&
        data.acf.location.length > 0
    ) {
        storeData = props.stores.filter(
        (elem) => elem.id === data.acf.location[0]["ID"]
        )[0];
        if (!storeData) {
        storeData = props.restaurants.filter(
            (elem) => elem.id === data.acf.location[0]["ID"]
        )[0];
        }
    }
    if (!data.acf.contact_details && !data.acf.facilities && data.acf.store) {
        storeData = props.stores.filter((elem) => elem.id === data.acf.store[0])[0];
        if (!openingHours && storeData.acf.opening_hours)
        setOpeningHours([...storeData.acf.opening_hours]);
    }

    const [openingHours, setOpeningHours] = useState(
        storeData &&
        storeData.hasOwnProperty("acf") &&
        storeData.acf.hasOwnProperty("opening_hours")
        ? [...storeData.acf.opening_hours]
        : null
    );

    if (!data.acf.contact_details && !data.acf.facilities && data.acf.store) {
        if (!openingHours && storeData.acf.opening_hours)
        setOpeningHours([...storeData.acf.opening_hours]);
    }

    useEffect(() => {
        if (!openingHours) return;
        const now = moment();
        let dayNumber = now.day();
        let day;

        switch (parseInt(dayNumber)) {
        case 0:
            day = "sunday";
            break;
        case 1:
            day = "monday";
            break;
        case 2:
            day = "tuesday";
            break;
        case 3:
            day = "wednesday";
            break;
        case 4:
            day = "thursday";
            break;
        case 5:
            day = "friday";
            break;
        case 6:
            day = "saturday";
            break;
        default:
            return;
        }

        let index = openingHours
        .map((e) => {
            return e.day;
        })
        .indexOf(day);
        let formattedOpeningHours = [];
        let today = [...openingHours];

        today = today[index];
        if (today) {
        //today.day = "Today";
        today.isToday = true;

        if (index > 0 && index !== openingHours.length - 1) {
            formattedOpeningHours = [
            today,
            ...openingHours.slice(index + 1, openingHours.length),
            ...openingHours.slice(0, index),
            ];
        } else if (index === 0) {
            formattedOpeningHours = [
            today,
            ...openingHours.slice(index + 1, openingHours.length),
            ];
        } else if (index === openingHours.length - 1) {
            formattedOpeningHours = [
            today,
            ...openingHours.slice(0, openingHours.length - 1),
            ];
        }

        setOpeningHours(formattedOpeningHours);
        }
    }, []);

    // return (

    // )
}


