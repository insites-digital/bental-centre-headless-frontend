import React, { useEffect, useState } from "react";
import HeroText from "../HeroText";
import parse from "html-react-parser";

const Careers = (props) => {
  const [windowSize, setWindowSize] = useState(0);

  useEffect(() => {
    setWindowSize(window.innerWidth);

    window.addEventListener("resize", () => setWindowSize(window.innerWidth));

    return () => {
      window.removeEventListener("resize", () =>
        setWindowSize(window.innerWidth)
      );
    };
  }, []);

  const roundUp = (number, roundTo) => Math.ceil(number / roundTo) * roundTo;
  let emptyGridItems = [];
  let emptyGridAmounts = 0;
  if (windowSize >= 700) {
    if (props.careers.length % 3 !== 0) {
      emptyGridAmounts =
        roundUp(props.careers.length, 3) - props.careers.length;
      for (let i = 1; i <= emptyGridAmounts; i++) {
        emptyGridItems.push(
          '<div class="teaser-content teaser-content--third"></div>'
        );
      }
    }
  }

  return (
    <>
      <HeroText
        data={{
          breadcrumbs: [
            { name: "The Centre", link: "/the-centre/" },
            { name: "Careers with us", link: "" },
          ],
          description: props.data.acf.components[0].content,
        }}
      ></HeroText>
      <div className="container">
        <div className="teaser-container teaser-container--careers">
          <div className="teaser-content__container">
            {props.careers.map((career) => {
              const store = props.stores.filter(
                (store) => store.id === career.store["ID"]
              );
              return (
                <div className="teaser-content teaser-content--third">
                  <div className="teaser-content__inside">
                    <h3 className="teaser--careers__subheading">
                      {store[0] && parse(store[0].title.rendered)}
                    </h3>
                    <div className="teaser--careers__heading-container">
                      {career &&
                        career.positions &&
                        career.positions.map((position) => (
                          <h2 className="teaser--careers__heading">
                            {position && position.title}{" "}
                            {position.contract.length > 0 && (
                              <span className="small">
                                (
                                {position.contract.map((contract, index) => {
                                  if (index === 0) {
                                    return contract.replace("-", " ");
                                  }
                                  return "," + contract.replace("-", " ");
                                })}
                                )
                              </span>
                            )}
                          </h2>
                        ))}
                    </div>

                    <div className="teaser__description">
                      {parse(career.description)}
                    </div>
                  </div>
                </div>
              );
            })}
            {emptyGridItems.map((item) => parse(item))}
          </div>
        </div>
      </div>
    </>
  );
};

export default Careers;
