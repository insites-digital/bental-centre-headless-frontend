import React from "react";
import HeroText from "../../components/HeroText";
import TabbedServicesFacilities from "../../components/tabbedContent/TabbedServicesFacilities";

const Services = (props) => {
  return (
    <>
      <HeroText
        data={{
          breadcrumbs: [
            { name: "The Centre", link: "/the-centre/" },
            { name: "Services & Facilities", link: "" },
          ],
        }}
      ></HeroText>
      <div
        className="services-page"
        style={{ marginBottom: "100px", marginTop: "40px" }}
      >
        <TabbedServicesFacilities
          amenities={props.data}
          amenityCats={props.category}
        />
      </div>
    </>
  );
};

export default Services;
