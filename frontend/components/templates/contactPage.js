import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import twitter from "../../static/images/social/twitter.svg";
import facebook from "../../static/images/social/facebook.svg";
import instagram from "../../static/images/social/instagram.svg";
import moment from "moment";
import OpenStatus from "../../components/OpenStatus";

const ContactPage = (props) => {
  const [content, setContent] = useState("");
  const [storeData, setStoreData] = useState("");
  useEffect(() => {
    if (
      props.data.hasOwnProperty("acf") &&
      props.data.acf.hasOwnProperty("components")
    ) {
      if (props.data.acf.components[0].acf_fc_layout === "text_area") {
        setContent(props.data.acf.components[0].content);
      }
    }
    setStoreData(
      props.stores.filter((item) => item.title.rendered.includes("Bentall"))[0]
    );
  }, []);
  if (!props || !props.hasOwnProperty("data")) return <></>;

  return (
    <>
      <section className="single-page contact">
        <div className="hero-text container" style={{ marginTop: "55px" }}>
          <nav>
            <ul>
              <li>
                <a href="/contact-us/" style={{ textDecoration: "none" }}>
                  Contact Us
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div className="container">
          <div
            className="teaser-container teaser-container--single-page"
            style={{ marginTop: "0" }}
          >
            <div
              className="teaser-content__container"
              style={{ flexWrap: "wrap" }}
            >
              <div className="teaser-content teaser-content--half">
                {storeData && (
                  <div className="teaser__img">
                    <img
                      src={storeData.acf.image.url.original_image.url}
                      alt={parse(storeData.title.rendered)}
                    />
                  </div>
                )}
              </div>
              <div className="teaser-content teaser-content--half teaser-content--details">
                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(props.data.title.rendered)}
                  </h2>
                  <div className="teaser__description">{parse(content)}</div>
                </div>
              </div>
              {/* {storeData &&
                storeData.acf.opening_hours &&
                storeData.acf.opening_hours.length > 0 && (
                  <div className="teaser-content--fourth">
                    <h2 className="teaser__subject">Opening hours</h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__span-container">
                        <table className="opening-times">
                          <tbody>
                            {storeData.acf.opening_hours.map(
                              (opening, index) => (
                                <tr key={`openinghours-${index}`}>
                                  <td className="capitalize">{opening.day}</td>
                                  <td>
                                    {moment(
                                      new Date(
                                        "1970-01-01T" + opening.start_time
                                      )
                                    )
                                      .format("h:mma")
                                      .replace(":00", "")}{" "}
                                    <span className="dash">-</span>{" "}
                                    {moment(
                                      new Date("1970-01-01T" + opening.end_time)
                                    )
                                      .format("h:mma")
                                      .replace(":00", "")}
                                  </td>
                                </tr>
                              )
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                )}
              {storeData && storeData.acf.contact_details && (
                <div className="teaser-content--fourth teaser-content--fourth--contact">
                  {storeData.acf.contact_details.email !== "" ||
                    (storeData.acf.contact_details.phone_number !== "" && (
                      <h2 className="teaser__subject">Contact</h2>
                    ))}

                  <div className="teaser-content__inside">
                    <div className="teaser__description">
                      <p>
                        {storeData.acf.contact_details.email !== "" && (
                          <>
                            <a
                              href={
                                "mailto:" + storeData.acf.contact_details.email
                              }
                            >
                              {storeData.acf.contact_details.email}
                            </a>
                            <br />
                          </>
                        )}
                        {storeData.acf.contact_details.phone_number !== "" && (
                          <>
                            <a
                              href={
                                "tel:" +
                                storeData.acf.contact_details.phone_number
                              }
                            >
                              {storeData.acf.contact_details.phone_number}
                            </a>
                          </>
                        )}
                      </p>
                      {storeData.acf.contact_details.social_media.length >
                        0 && (
                        <div className="social">
                          {storeData.acf.contact_details.social_media.map(
                            (social) => {
                              let socialAccount;
                              if (social.account === "facebook") {
                                socialAccount = facebook;
                              } else if (social.account === "twitter") {
                                socialAccount = twitter;
                              } else {
                                socialAccount = instagram;
                              }
                              return (
                                <p
                                  key={social.account}
                                  className={`social--${social.account}`}
                                >
                                  <a href={social.link} target="_blank">
                                    <div className="social__container">
                                      <div className="social__icon">
                                        <img src={socialAccount} alt="" />
                                      </div>
                                      {social.handle !== "" && (
                                        <span className="social__handle">
                                          {social.handle}
                                        </span>
                                      )}
                                    </div>
                                  </a>
                                </p>
                              );
                            }
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}

              {storeData &&
                storeData.acf.contact_details &&
                storeData.acf.contact_details.address !== "" && (
                  <div className="teaser-content--fourth">
                    <h2 className="teaser__subject">Location</h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        <address>
                          {parse(storeData.acf.contact_details.address)}
                        </address>
                      </div>
                    </div>
                  </div>
                )}

              {storeData &&
                storeData.acf.facilities &&
                storeData.acf.facilities.length > 0 && (
                  <div className="teaser-content--fourth">
                    <h2 className="teaser__subject">Facilities</h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        <ul>
                          {storeData.acf.facilities.map((facility, index) => (
                            <li key={`facility-${index}`}>{facility}</li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                )} */}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ContactPage;
