import React, { useState, useEffect } from "react";
import parse from "html-react-parser";
import twitter from "../../static/images/social/twitter.svg";
import facebook from "../../static/images/social/facebook.svg";
import instagram from "../../static/images/social/instagram.svg";
import moment from "moment";
import { isBST } from "../../utils/isBST";
import OpenStatus from "../../components/OpenStatus";
import ImageHoverItem from "../ImageHoverItem";

const SinglePage = (props) => {
  // console.log("props ::", props);

  const { data } = props;
  let storeData = data;

  if (
    !data.acf.contact_details &&
    !data.acf.facilities &&
    data.acf.location &&
    data.acf.location.length > 0
  ) {
    storeData = props.stores.filter(
      (elem) => elem.id === data.acf.location[0]["ID"]
    )[0];
    if (!storeData) {
      storeData = props.restaurants.filter(
        (elem) => elem.id === data.acf.location[0]["ID"]
      )[0];
    }
  }
  if (!data.acf.contact_details && !data.acf.facilities && data.acf.store) {
    storeData = props.stores.filter((elem) => elem.id === data.acf.store[0])[0];
    // if (!openingHours && storeData.acf.opening_hours)
    //   setOpeningHours([...storeData.acf.opening_hours]);
  }

  const [openingHours, setOpeningHours] = useState(
    storeData &&
      storeData.hasOwnProperty("acf") &&
      storeData.acf.hasOwnProperty("opening_hours")
      ? [...storeData.acf.opening_hours]
      : null
  );

  if (!data.acf.contact_details && !data.acf.facilities && data.acf.store) {
    if (!openingHours && storeData?.acf?.opening_hours)
      setOpeningHours([...storeData.acf.opening_hours]);
  }

  useEffect(() => {
    if (!openingHours) return;
    const now = moment();
    let dayNumber = now.day();
    let day;

    switch (parseInt(dayNumber)) {
      case 0:
        day = "sunday";
        break;
      case 1:
        day = "monday";
        break;
      case 2:
        day = "tuesday";
        break;
      case 3:
        day = "wednesday";
        break;
      case 4:
        day = "thursday";
        break;
      case 5:
        day = "friday";
        break;
      case 6:
        day = "saturday";
        break;
      default:
        return;
    }

    let index = openingHours
      .map((e) => {
        return e.day;
      })
      .indexOf(day);
    let formattedOpeningHours = [];
    let today = [...openingHours];

    today = today[index];
    if (today) {
      //today.day = "Today";
      today.isToday = true;

      if (index > 0 && index !== openingHours.length - 1) {
        formattedOpeningHours = [
          today,
          ...openingHours.slice(index + 1, openingHours.length),
          ...openingHours.slice(0, index),
        ];
      } else if (index === 0) {
        formattedOpeningHours = [
          today,
          ...openingHours.slice(index + 1, openingHours.length),
        ];
      } else if (index === openingHours.length - 1) {
        formattedOpeningHours = [
          today,
          ...openingHours.slice(0, openingHours.length - 1),
        ];
      }

      setOpeningHours(formattedOpeningHours);
    }
  }, []);

  const britishSummerTime = isBST(new Date());

  return (
    <>
      <section className="single-page">
        <div className="container hero-text">
          <nav>
            {data.hasOwnProperty("breadcrumbs") && data.breadcrumbs.length > 0 && (
              <ul>
                {data.breadcrumbs.map((breadcrumb, index) => {
                  if (index + 1 === data.breadcrumbs.length) {
                    return (
                      <li style={{ textTransform: "uppercase" }}>
                        {breadcrumb && breadcrumb.name && breadcrumb.name}
                      </li>
                    );
                  }
                  return (
                    <li>
                      <a href={breadcrumb.link}>
                        {breadcrumb && breadcrumb.name && breadcrumb.name}
                      </a>
                    </li>
                  );
                })}
              </ul>
            )}
          </nav>
        </div>
        <div className="container">
          <div className="teaser-container teaser-container--single-page">
            <div
              className="teaser-content__container"
              style={{ flexWrap: "wrap" }}
            >
              <div className="teaser-content teaser-content--half">
                {data.acf.image.svg && data.acf.image.hasOwnProperty("svg") && (
                  <div
                    className={
                      "teaser__img" +
                      (data.acf.image.hasOwnProperty("svg")
                        ? " teaser__img--svg"
                        : "")
                    }
                  >
                    <div>{parse(data.acf.image.svg)}</div>
                  </div>
                )}
                {!data.acf.image.hasOwnProperty("svg") &&
                  (data?.acf?.image?.url?.sizes ? (
                    <div
                      style={{
                        paddingTop: `calc(${data?.acf?.image?.url?.sizes["xsmall_50vw-height"]} / ${data?.acf?.image?.url?.sizes["xsmall_50vw-width"]} * 100%)`,
                      }}
                      className={
                        "teaser__img" +
                        (data.acf.image.hasOwnProperty("svg")
                          ? " teaser__img--svg"
                          : "")
                      }
                    >
                      <ImageHoverItem data={data} size={"100vw"} className="" />
                    </div>
                  ) : null)}
              </div>
              <div className="teaser-content teaser-content--half teaser-content--details">
                {data.hasOwnProperty("show_opening") &&
                  openingHours &&
                  data.show_opening &&
                  storeData &&
                  storeData.acf &&
                  storeData.acf.opening_hours.length > 0 && (
                    <OpenStatus openingHours={storeData.acf.opening_hours} />
                  )}
                <div className="teaser-content__inside">
                  <h2 className="teaser__subject">
                    {parse(data.title.rendered)}
                  </h2>
                  <div className="teaser__description">
                    {parse(data.content.rendered)}
                  </div>
                  {props.data.acf.downloadable_pdf &&
                  props.data.acf.downloadable_pdf.pdf_value ? (
                    <div className="teaser__pdf">
                      <a
                        href={props.data.acf.downloadable_pdf.pdf.url}
                        download
                      >
                        {props.data.acf.downloadable_pdf.message}
                      </a>
                    </div>
                  ) : null}
                </div>
              </div>
              {openingHours && openingHours.length > 0 && (
                <div className="teaser-content--fourth">
                  <h2 className="teaser__subject">Opening hours</h2>
                  <div className="teaser-content__inside">
                    <div className="teaser__span-container">
                      <table className="opening-times">
                        <tbody>
                          {openingHours.map((opening, index) => {
                            const endTime =
                              opening.end_time === "23:59:00"
                                ? "00:00:00"
                                : opening.end_time;

                            return (
                              <tr key={`openinghours-${index}`}>
                                <td className="capitalize">
                                  {opening.isToday ? "Today" : opening.day}
                                </td>
                                {opening.start_time !== "" &&
                                  opening.end_time !== "" && (
                                    // Hello, if you have come here to fix this again
                                    // I have no clue why, but safari really wants to mess around with dates here
                                    // it's putting all times as 1 hour ahead
                                    // the getTimezoneOffset nonsense is to normalise times to a certain timezone
                                    // but again for some reason this sets the time as 2 hours ahead so I have subtracted this
                                    // wish I could explain why but it's a complete mystery to me

                                    <td>
                                      {moment(
                                        new Date(
                                          "1970-01-01T" + opening.start_time
                                        ).setTime(
                                          new Date(
                                            "1970-01-01T" +
                                              opening.start_time +
                                              "Z"
                                          ) -
                                            new Date().getTimezoneOffset() *
                                              60 *
                                              1000
                                        )
                                      )
                                        .subtract(
                                          britishSummerTime ? 2 : 1,
                                          "hour"
                                        )
                                        .format("h:mma")
                                        .replace(":00", "")}{" "}
                                      <span className="dash">-</span>{" "}
                                      {moment(
                                        new Date(
                                          "1970-01-01T" + endTime
                                        ).setTime(
                                          new Date(
                                            "1970-01-01T" + endTime + "Z"
                                          ) -
                                            new Date().getTimezoneOffset() *
                                              60 *
                                              1000
                                        )
                                      )
                                        .subtract(
                                          britishSummerTime ? 2 : 1,
                                          "hour"
                                        )
                                        .format("h:mma")
                                        .replace(":00", "")}
                                    </td>
                                  )}
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              )}
              {storeData && storeData.acf.contact_details && (
                <div className="teaser-content--fourth teaser-content--fourth--contact">
                  <h2 className="teaser__subject">Contact</h2>

                  <div className="teaser-content__inside">
                    <div className="teaser__description">
                      <p>
                        {storeData.acf.contact_details.email !== "" && (
                          <>
                            <a
                              href={
                                "mailto:" + storeData.acf.contact_details.email
                              }
                            >
                              {storeData.acf.contact_details.email}
                            </a>
                            <br />
                          </>
                        )}
                        {storeData.acf.contact_details.phone_number !== "" && (
                          <>
                            <a
                              href={
                                "tel:" +
                                storeData.acf.contact_details.phone_number
                              }
                            >
                              {storeData.acf.contact_details.phone_number}
                            </a>
                          </>
                        )}
                      </p>
                      {storeData.acf.contact_details.social_media.length >
                        0 && (
                        <div className="social">
                          {storeData.acf.contact_details.social_media.map(
                            (social) => {
                              let socialAccount;
                              if (social.account === "facebook") {
                                socialAccount = facebook;
                              } else if (social.account === "twitter") {
                                socialAccount = twitter;
                              } else {
                                socialAccount = instagram;
                              }
                              return (
                                <p
                                  key={social.account}
                                  className={`social--${social.account}`}
                                >
                                  <a href={social.link} target="_blank">
                                    <div className="social__container">
                                      <div className="social__icon">
                                        <img src={socialAccount} alt="" />
                                      </div>
                                      {social.handle !== "" && (
                                        <span className="social__handle">
                                          {social.handle}
                                        </span>
                                      )}
                                    </div>
                                  </a>
                                </p>
                              );
                            }
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}

              {storeData &&
                storeData.acf.contact_details &&
                storeData.acf.contact_details.address !== "" && (
                  <div className="teaser-content--fourth">
                    <h2 className="teaser__subject">Location</h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        <address>
                          {parse(
                            storeData.acf.contact_details.address.replace(
                              /\r\n/g,
                              "<br/>"
                            )
                          )}
                        </address>
                      </div>
                    </div>
                  </div>
                )}

              {storeData &&
                storeData.acf.facilities &&
                storeData.acf.facilities.length > 0 && (
                  <div className="teaser-content--fourth">
                    <h2 className="teaser__subject">Facilities</h2>
                    <div className="teaser-content__inside">
                      <div className="teaser__description">
                        <ul>
                          {storeData.acf.facilities.map((facility, index) => (
                            <li key={`facility-${index}`}>{facility}</li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SinglePage;
