import React from "react";
import parse from "html-react-parser";
import { getSlug } from "../../utils/getSlug";
import Error from "../Error";
import Hero from "../Hero";
import ImageText5050 from "../ImageText5050";
import WhatsOn from "../WhatsOn";
import ProductGrid from "../ProductGrid";
import EventGrid from "../EventGrid";
import VideoPromotion from "../video/VideoPromotion";
import RestaurantSlider from "../RestaurantSlider";
import ContentPromotion from "../ContentPromotion";
import CampaignFeature from "../CampaignFeature";
import TeaserFeature from "../TeaserFeature";
import FeaturedProductsSlider from "../FeaturedProductsSlider";
import HeroText from "../HeroText";

const Standard = (props) => {
  const GetComponents = () => {
    return props.data.acf.components ? (
      props.data.acf.components.map((component) => {
        switch (component.acf_fc_layout) {
          case "hero":
            return <Hero data={component.slides} />;
          case "fiftyFifty":
            return <ImageText5050 data={component} />;
          case "eventSlider":
            return (
              <WhatsOn
                data={component}
                events={props.events}
                eventCats={props.eventCats}
              />
            );
          case "productGrid":
            return (
              <ProductGrid
                data={component}
                products={props.products}
                stores={props.stores}
              />
            );
          case "eventGrid":
            return (
              <EventGrid
                data={component}
                events={props.events}
                eventCats={props.eventCats}
              />
            );
          case "videoPromotion":
            return <VideoPromotion data={component} />;
          case "restaurantSlider":
            return (
              <RestaurantSlider
                data={props.restaurants}
                componentData={component}
                restaurantCats={props.restaurantCats}
                floors={props.floors}
              />
            );
          case "contentPromotion":
            return <ContentPromotion data={component} />;

          case "fullwidthTeaser":
            return <CampaignFeature data={component} />;

          case "dotd":
            return (
              <TeaserFeature
                data={props.globalContent.daily_deals}
                products={props.products}
              />
            );
          case "productSlider":
            return (
              <FeaturedProductsSlider
                data={component}
                allProducts={props.products}
                stores={props.stores}
              />
            );
          case "text_area":
            return (
              <HeroText
                data={{
                  description: component.content,
                }}
              />
            );
          default:
            return <></>;
        }
      })
    ) : (
      <></>
    );
  };

  return (
    <>
      <div className="standard">
        <GetComponents />
      </div>
    </>
  );
};

export default Standard;
