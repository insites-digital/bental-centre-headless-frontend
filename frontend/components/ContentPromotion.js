import React from "react";
import parse from "html-react-parser";

const ContentPromotion = props => {
  return (
    <section className="content-promotion container">
      <div className="content-promotion__image">
        <img src={props.data.image.url} alt={props.data.image.alt} />
      </div>
      <div className="content-promotion__text">
        <div>{parse(props.data.content)}</div>
      </div>
    </section>
  );
};

export default ContentPromotion;
