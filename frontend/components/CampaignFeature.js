import React from "react";
import blackArrow from "../static/images/general/arrow-black.svg";
import Link from "next/link";
import Image from "./Image";

const CampaignFeature = (props) => {
  const { heading, sub_heading, description, link, image } = props.data;

  const getSlug = (url) => {
    if (!url) return url;
    let formatUrl;
    if (url.includes(".co.uk")) {
      formatUrl = url.split(".co.uk")[1];
      return formatUrl === "/home/" ? "/" : formatUrl;
    }
    formatUrl = url.split(".com")[1];
    return formatUrl === "/home/" ? "/" : formatUrl;
  };

  return (
    <div className="campaign-feature campaign-feature--full-width">
      <div className="campaign-feature__full-link">
        <div className="campaign-feature__img">
          <Image data={image} imageSize="100vw" />
        </div>
        <Link href={`${getSlug(link.url)}`}>
          <div className="campaign-feature__content">
            {heading && <h2>{heading}</h2>}​
            {sub_heading && <h4>{sub_heading}</h4>}
            {description && <p>{description}</p>}
            <Link href={`${getSlug(link.url)}`}>
              <a className="link link--uppercase link--arrow">
                <span style={{ display: "inline-block" }}>{link.label}</span>{" "}
                <span className="arrow">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="29.789"
                    height="8.21"
                  >
                    <g data-name="Group 1359">
                      <path
                        data-name="Path 759"
                        d="M0 4.105h20.186"
                        stroke="#000"
                        strokeWidth=".5"
                      />
                      <path
                        data-name="Polygon 1"
                        d="M29.789 4.1L18.842 8.2V0z"
                      />
                    </g>
                  </svg>
                </span>
              </a>
            </Link>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default CampaignFeature;
