import React from "react";

const ProductImageHoverItem = (props) => {
  if (!props.hasOwnProperty("data")) return;
  let hoverSource = "";
  let imageSource = "";

  if (props.data.acf.image.hover) {
    hoverSource =
      props.data.acf.image.hover.sizes[`xsmall_${props.size}`] +
      " 640w, " +
      props.data.acf.image.hover.sizes[`xsmall_${props.size}`] +
      " 768w, " +
      props.data.acf.image.hover.sizes[`small_${props.size}`] +
      " 1024w, " +
      props.data.acf.image.hover.sizes[`medium_${props.size}`] +
      " 1366w, " +
      props.data.acf.image.hover.sizes[`large_${props.size}`] +
      " 1600w, " +
      props.data.acf.image.hover.sizes[`xlarge_${props.size}`] +
      " 1920w ";
  }

  if (props.size === "100vw") {
    imageSource =
      props.data.acf.image.url.sizes.medium +
      " 640w, " +
      props.data.acf.image.url.sizes.medium_large +
      " 768w, " +
      props.data.acf.image.url.sizes.large +
      " 1024w, " +
      props.data.acf.image.url.sizes.large_large +
      " 1366w, " +
      props.data.acf.image.url.sizes.xlarge +
      " 1600w, " +
      props.data.acf.image.url.sizes.xlarge_large +
      " 1920w ";
  } else if (props.size === "33vw") {
    imageSource =
      props.data.acf.image.url.sizes.xsmall_33vw +
      " 640w, " +
      props.data.acf.image.url.sizes.xsmall_33vw +
      " 768w, " +
      props.data.acf.image.url.sizes.small_33vw +
      " 1024w, " +
      props.data.acf.image.url.sizes.medium_33vw +
      " 1366w, " +
      props.data.acf.image.url.sizes.large_33vw +
      " 1600w, " +
      props.data.acf.image.url.sizes.large_large +
      " 1920w ";
  } else if (props.size === "50vw") {
    imageSource =
      props.data.acf.image.url.sizes.xsmall_50vw +
      " 640w, " +
      props.data.acf.image.url.sizes.xsmall_50vw +
      " 768w, " +
      props.data.acf.image.url.sizes.small_50vw +
      " 1024w, " +
      props.data.acf.image.url.sizes.medium_50vw +
      " 1366w, " +
      props.data.acf.image.url.sizes.large_50vw +
      " 1600w, " +
      props.data.acf.image.url.sizes.large_large +
      " 1920w ";
  }
  return (
    <div className={`teaser__img ${props.className}`}>
      <img
        src={props.data.acf.image.url.sizes.medium}
        srcSet={imageSource}
        alt=""
      />

      {props.data.acf.image.hover && (
        <img
          className="hover"
          src={props.data.acf.image.hover.sizes[`medium_${props.size}`]}
          srcSet={hoverSource}
          alt=""
        />
      )}
    </div>
  );
};

export default ProductImageHoverItem;
