import axios from "axios";

// may want to include an error handler in here
const wordpress = axios.create({
  baseURL: process.env.WORDPRESS_REST_API,
  // auth: {
  //   username: "username",
  //   password: "password",
  // },
  headers: {
    "Content-Type": "application/json",
  },
});

export const formatAxiosError = (err) => {
  console.error(
    "\nA FETCH REQUEST HAS FAILED\n",
    "Route: " + err?.config?.url,
    "\nError: \n",
    err
  );
  return err;
};

export default wordpress;
