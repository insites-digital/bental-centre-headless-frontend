import wordpress from "../wordpress";

const getContentNumPages = async (contentType) => {
  try {
    const { headers } = await wordpress.get(`${contentType}`, {
      params: {
        _fields: `id`,
        per_page: 100,
      },
    });

    return headers["x-wp-totalpages"];
  } catch (err) {
    console.error(`Failed to count content for ${contentType}: `, err);
  }
};

export default getContentNumPages;
