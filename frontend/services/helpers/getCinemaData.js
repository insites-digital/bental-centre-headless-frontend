const baseUrl = "https://api-gate2.movieglu.com";
// const cinemaDetailsUrl = `${baseUrl}/cinemaDetails/`;
// const filmDetailsUrl = `${baseUrl}/filmDetails/`;
// const filmTrailerUrl = `${baseUrl}/trailers/`;
// const filmImageUrl = `${baseUrl}/images/`;
// const cinemaShowTimesUrl = `${baseUrl}/cinemaShowTimes/`;
// const filmsNowShowingUrl = `${baseUrl}/filmsNowShowing/`;

const movieGluHeader = {
  "api-version": "v200",
  "x-api-key": "zAonHtF0f3aiAhlslyAMR20sREfgdDiK4cHd5Bu7",
  Authorization: "Basic SU5TSV8yOm95cVM2RnJSQThiVg==",
  client: "INSI_2",
  territory: "UK",
  // Bentall Centre Latitude and Longitude to 6 decimal places
  geolocation: "51.411714; -0.304334",
  "device-datetime": new Date().toISOString(),
};

const baseRequest = async (path) => {
  const data = await fetch(`${baseUrl}/${path}`, {
    method: "GET",
    headers: movieGluHeader,
  })
    .then((res) => res.json())
    .catch((err) => {
      console.error(err);
    });

  return data;
};

// export const getCinemaDetails = async (cinemaId) => {
//     return baseRequest(`${cinemaDetailsUrl}?cinema_id=${cinemaId}`)
// }

// export const getFilmsNowShowing = async () => {
//     return baseRequest(`${filmsNowShowingUrl}`)
// }

// export const getFilmDetails = async (filmId) => {
//     return baseRequest(`${filmDetailsUrl}?film_id=${filmId}`)
// }

// export const getFilmImage = async (filmId, size) => {
//     return baseRequest(`${filmImageUrl}?film_id=${filmId}&size_category=${size}`)
// }

// export const getFilmTrailer = async (filmId) => {
//     return baseRequest(`${filmTrailerUrl}?film_id=${filmId}`)
// }

// export const getAllFilms = async () => {
//     return getFilmsNowShowing()
// }

export const getCinemaShowTimes = async (cinema_id, date) => {
  const cinemaShowTimes = await baseRequest(
    `cinemaShowTimes/?cinema_id=${cinema_id}&date=${date}`
  );
  return cinemaShowTimes;
};
export const getCinemaShowTimesForAFilm = async (cinema_id, film_id, date) => {
  const cinemaShowTimes = await baseRequest(
    `cinemaShowTimes/?film_id=${film_id}&cinema_id=${cinema_id}&date=${date}`
  );
  return cinemaShowTimes;
};

export const getFilmById = async (id) => {
  const film = await baseRequest(`filmDetails?film_id=${id}`);
  return film;
};

export const getPurchaseConfirmation = async (
  cinema_id,
  film_id,
  date,
  time
) => {
  const purchaseConfirmation = await baseRequest(
    `purchaseConfirmation/?cinema_id=${cinema_id}&film_id=${film_id}&date=${date}&time=${time}`
  );
  return purchaseConfirmation;
};
