import axios from "axios";
import wordpress, { formatAxiosError } from "../wordpress";
import getContentNumPages from "../helpers/getContentNumPages";
import { getFilmById } from "../helpers/getCinemaData";

export const getAllContent = async (contentType) => {
  try {
    let content = [];

    const numPages = await getContentNumPages(contentType);

    for (let page = 1; page <= numPages; page += 1) {
      const { data } = await wordpress.get(`${contentType}`, {
        params: {
          per_page: 50,
          page: page,
        },
      });
      content.push(data);
    }

    await axios
      .all(content)
      .catch((e) =>
        console.log(`error fetching content for ${contentType}: `, e)
      );

    return content.flat();
  } catch (err) {
    console.error(`Failed to get ${contentType}: `, err);
  }
};

export const getContent = async (contentType, fields = "") => {
  const numPages = await getContentNumPages(contentType);
  try {
    const { data } = await wordpress
      .get(contentType, {
        params: {
          _fields: fields,
          per_page: 100,
        },
      })
      .catch((err) => formatAxiosError(err));
    return data;
  } catch {
    console.log("Error with fetching data for ", contentType);
  }
};

export const getContentFromPages = async (contentType) => {
  const numPages = await getContentNumPages(contentType);
  let content = [];

  for (let page = 1; page <= numPages; page += 1) {
    const { data } = await wordpress.get(`${contentType}`, {
      params: {
        per_page: 100,
        page: page,
      },
    });
    content.push(data);
  }

  await axios
    .all(content)
    .catch((e) =>
      console.log(`error fetching content for ${contentType}: `, e)
    );

  return content.flat();
};

export const getPageBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`pages`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getEventBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`events`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getStoreBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`stores`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getRestaurantBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`restaurants`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getFilmBySlug = async (slug) => {
  const { data } = await getFilmById(slug);

  return data;
};

export const getAmenityBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`amenities`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getProductBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`products`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getCampaignBySlug = async (slug) => {
  const { data } = await wordpress
    .get(`campaigns`, {
      params: {
        slug: slug,
      },
    })
    .catch((err) => formatAxiosError(err));
  return data[0];
};

export const getOptions = async () => {
  const { data } = await wordpress
    .get("acf/options")
    .catch((err) => formatAxiosError(err));

  return data;
};
