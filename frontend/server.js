const express = require("express");
const next = require("next");
const port = parseInt(process.env.PORT, 10) || 3020;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    /* 301 redirects */
    server.get("/brands", (req, res) => {
      res.redirect(301, "/stores");
    });

    server.get("/sustainability", (req, res) => {
      res.redirect(301, "/the-centre/sustainability");
    });

    server.get("/bentall_1_1", (req, res) => {
      res.redirect(301, "/");
    });

    server.get("/kingston", (req, res) => {
      res.redirect(301, "/the-centre");
    });

    server.get("/terms-conditions", (req, res) => {
      res.redirect(301, "/legal-information");
    });

    server.get("/visiting-us", (req, res) => {
      res.redirect(301, "/the-centre/directions");
    });

    server.get("/the-edit", (req, res) => {
      res.redirect(301, "/");
    });

    server.get("/the-edit/:slug", (req, res) => {
      res.redirect(301, "/");
    });

    server.get("/opening-hours", (req, res) => {
      res.redirect(301, "/the-centre/opening-hours");
    });

    server.get("/brand/:slug", (req, res) => {
      res.redirect(301, "/stores");
    });

    server.get("/brand/:slug", (req, res) => {
      res.redirect(301, "/stores");
    });

    server.get("/food-drink", (req, res) => {
      res.redirect(301, "/eat-drink");
    });

    server.get("/car-parking-2", (req, res) => {
      res.redirect(301, "/the-centre/parking");
    });

    server.get("/car-parking", (req, res) => {
      res.redirect(301, "/the-centre/parking");
    });

    server.get("/whats-on/redevelopment", (req, res) => {
      res.redirect(301, "/redevelopment");
    });

    server.get("/whats-on/podcasts", (req, res) => {
      res.redirect(301, "/whats-on");
    });

    server.get("/covid19-info-for-customers", (req, res) => {
      res.redirect(301, "/whats-on/covid-19-info");
    });

    server.get("/gift-card", (req, res) => {
      res.redirect(301, "/amenity/gift-card");
    });

    server.get("/careers", (req, res) => {
      res.redirect(301, "/the-centre/careers-with-us");
    });

    server.get("/stores/#/collect", (req, res) => {
      res.redirect(301, "/the-centre/click-and-collect");
    });

    server.get("/whats-on/boss-x-aj-bxng-collection", (req, res) => {
      res.redirect(301, "/whats-on/boss-collab-anthony-joshua");
    });

    server.get("/whats-on/face-coverings", (req, res) => {
      res.redirect(301, "/whats-on/covid-19-info");
    });
    server.get("/how-to-get-here", (req, res) => {
      res.redirect(301, "/the-centre/directions");
    });
    /* End of 301 redirects */

    server.get("/deal-of-the-day", (req, res) => {
      const actualPage = "/deal-of-the-day";
      app.render(req, res, actualPage);
    });

    server.get("/search", (req, res) => {
      const actualPage = "/search";
      app.render(req, res, actualPage);
    });

    server.get("/stores", (req, res) => {
      const actualPage = "/stores";
      app.render(req, res, actualPage);
    });

    server.get("/eat-drink", (req, res) => {
      const actualPage = "/eat-drink";
      app.render(req, res, actualPage);
    });

    server.get("/cinema", (req, res) => {
      const actualPage = "/cinema";
      app.render(req, res, actualPage);
    });

    server.get("/contact-us", (req, res) => {
      const actualPage = "/contact-us";
      app.render(req, res, actualPage);
    });

    server.get("/services-facilities", (req, res) => {
      const actualPage = "/services-facilities";
      app.render(req, res, actualPage);
    });

    server.get("/whats-on", (req, res) => {
      const actualPage = "/whats-on";
      app.render(req, res, actualPage);
    });

    server.get("/the-centre/careers-with-us", (req, res) => {
      const actualPage = "/careers-with-us";
      app.render(req, res, actualPage);
    });

    server.get("/the-centre/opening-hours", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "opening-hours" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/the-centre/about-us", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "about-us" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/the-centre/directions", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "how-to-get-here" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/the-centre/parking", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "parking" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/the-centre/sustainability", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "sustainability" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/the-centre/click-and-collect", (req, res) => {
      const actualPage = "/the-centre";
      const queryParams = { q: "click-collect" };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/sitemap.xml", (req, res) => {
      const actualPage = "/sitemap.xml";
      app.render(req, res, actualPage);
    });

    server.get("/the-centre", (req, res) => {
      const actualPage = "/the-centre";
      app.render(req, res, actualPage);
    });

    server.get("/:slug", (req, res) => {
      const actualPage = "/page";
      const queryParams = { slug: req.params.slug };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
